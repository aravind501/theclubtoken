//
//  UpgradePackageViewController.swift
//  TheClubToken
//
//  Created by trioangle on 30/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CDAlertView
import SwiftyJSON

class PackageUpgradeViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var epinAddressBaseView: UIView!
    @IBOutlet weak var packageInfoBaseView: UIView!
    @IBOutlet weak var epinAddressTxtFld: HoshiTextField!
    @IBOutlet weak var qrBtn: UIButton!
    @IBOutlet weak var nameValLbl: UILabel!
    @IBOutlet weak var packageTypeValLbl: UILabel!
    @IBOutlet weak var amountValLbl: UILabel!
    @IBOutlet weak var upgradePackBaseView: UIView!
    @IBOutlet weak var upgradePackageBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var epinAddTitleLbl: UILabel!
    @IBOutlet weak var packageInfo: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var packageTypetitLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    
    var packageNeme = String()
    lazy var lang = Language.default.object
    override func viewDidLoad() {
        super.viewDidLoad()

        epinAddressTxtFld.delegate = self
        epinAddressBaseView.layer.cornerRadius = 10
        packageInfoBaseView.layer.cornerRadius = 10
        upgradePackBaseView.layer.cornerRadius = upgradePackBaseView.frame.height / 2
        NotificationCenter.default.addObserver(
        self,
        selector: #selector(self.handelQrCodeValue),
        name: Notification.Name("qrCodePasser"),
        object: nil)
        
        nameValLbl.text = self.packageNeme
        packageTypeValLbl.text = ""
        amountValLbl.text = ""
        
        titleLbl.text = lang.upgradePackage
        epinAddTitleLbl.text = lang.EPINAddress
        packageInfo.text = lang.PackageInformation
        nameLbl.text = lang.name
        packageTypetitLbl.text = lang.PackageType
        amountLbl.text = lang.Amount
        
        upgradePackageBtn.setTitle(lang.upgradePackage, for: .normal)
        
        // Do any additional setup after loading the view.
    }
    
    @objc func handelQrCodeValue(notification: NSNotification){
        
        print(notification.object!)
        self.epinAddressTxtFld.text = String(describing: notification.object!)
        self.callEpinUpgradePackageShowApi(epincode: self.epinAddressTxtFld.text!)
        //do stuff using the userInfo property of the notification object
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func qrBtnAction(_ sender: Any) {
        
        let qrVc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "QRScanViewController") as? QRScanViewController
         
        self.navigationController?.present(qrVc!, animated: false, completion: nil)
        
    }
    @IBAction func upgradePackageBtnAction(_ sender: Any) {
        
        if self.epinAddressTxtFld.text!.length > 0 {
            self.callEpinUpgradePackageApi(epincode: self.epinAddressTxtFld.text!)
        }
        
    }
    
    func callEpinUpgradePackageApi(epincode:String) {
        let params = ["userid":customer_id,"epincode":epincode]
                                print(params)
        
                                APIManager.shared.EpinUpgradePackage(params: params as [String : AnyObject]) { (response) in
                                                            print("response params1 \(response)")
                                                            print("CHeckparams")
                                                            
                                                            
                                                            if let dict = response["upgrade_package_epin"].dictionary {

                                                                if dict["status"]?.stringValue == "success" {
                                                                 
                                                                    let msg = dict["message"]?.stringValue
                                                                    
                                                                    Common.checkRemoveCdAlert()
                                                                            let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                                                    alert.circleFillColor = UIColor.buttonprimary
                                                                    let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                                            alert.add(action: doneAction)
                                                                            alert.show() { (alert) in

                                                                                NotificationCenter.default.post(name: Notification.Name("topup"), object: nil)

                                                                                self.packageTypeValLbl.text = ""
                                                                                self.amountValLbl.text = ""

                                                                            }
                                                                }
                                                                else
                                                                {
                                                                    Common.checkRemoveCdAlert()

                                                                    let alert = CDAlertView(title: AppName, message: dict["message"]?.stringValue, type: .error)
                                                                    alert.circleFillColor = UIColor.buttonprimary

                                                                    alert.autoHideTime = 2.0
                                                                    alert.show()
                                                                }
                                                                
                                                            }
                                                                    else {
                                                                        
                                                                Common.checkRemoveCdAlert()

                                                                        let alert = CDAlertView(title: AppName, message: "EpinUpgradePackage", type: .error)
                                                                        alert.circleFillColor = UIColor.buttonprimary

                                                                        alert.autoHideTime = 2.0
                                                                        alert.show()

                                                                        
                                                            }
                                }
    }
    
    func callEpinUpgradePackageShowApi(epincode:String) {
        let params = ["userid":customer_id,"epincode":epincode]
                                print(params)
        
                                APIManager.shared.EpinUpgradePackageShow(params: params as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                                    
                                    if let dict = response["upgrade_package_epinshow"].dictionary {

                                        if dict["status"]?.stringValue == "success" {
                                        
                                            self.nameValLbl.text = self.packageNeme
                                            self.packageTypeValLbl.text = dict["package_type"]?.stringValue
                                            self.amountValLbl.text = dict["package_amount"]?.stringValue
                                            
                                        }
                                        else
                                        {
                                            let alert = CDAlertView(title: AppName, message: dict["message"]?.stringValue, type: .error)
                                            alert.circleFillColor = UIColor.buttonprimary

                                            alert.autoHideTime = 2.0
                                            alert.show()
                                        }
                                        
                                    }
                                            else {
                                                
                                                let alert = CDAlertView(title: AppName, message: "EpinUpgradePackageShow", type: .error)
                                                alert.circleFillColor = UIColor.buttonprimary

                                                alert.autoHideTime = 2.0
                                                alert.show()

                                                
                                    }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method

    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()

        if textField.text != ""
        {
            self.callEpinUpgradePackageShowApi(epincode: self.epinAddressTxtFld.text!)

        }
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
                textField.resignFirstResponder()

        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
