//
//  HomeViewController.swift
//  TheClubToken
//
//  Created by trioangle on 05/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import ExpandableCell
import CDAlertView
import SwiftyJSON
import MarqueeLabel
import DropDown
class TabHomeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var DoneBtn: UIButton!
    @IBOutlet var tableView: ExpandableTableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var currentPakangeLbl: UILabel!
    @IBOutlet weak var sliderCountBtn: UIButton!
    @IBOutlet weak var currentPackageBtn: UIButton!
    @IBOutlet weak var sendPaymentBtn: UIButton!
    @IBOutlet weak var flagBaseView: UIView!
    @IBOutlet weak var flagBtn: UIButton!
    
        @IBOutlet weak var sendPaymentTitleLbl: UILabel!
        @IBOutlet weak var myEpinTitleLbl: UILabel!
        @IBOutlet weak var transactionHistoryTitleLbl: UILabel!
        @IBOutlet weak var upgradePakangeTitleLbl: UILabel!
        @IBOutlet weak var referTitleLbl: UILabel!
    
    var homeDataDict = JSON()
    var currentPackage = String()
    var kycStatus = String()
    let dropDown = DropDown()
    var langArr = ["English","Chinese"]
    var packageDatas = [String : JSON]()
    lazy var lang = Language.default.object
    @IBOutlet weak var tctnoticeLbl: MarqueeLabel!
    
    var announcementsArray = [String]()
   
    var parentCells: [[String]] = [
            [ExpandableCell1.ID,
             ExpandableCell1.ID,
             ExpandableCell1.ID,
             ExpandableCell1.ID
            ]
        ]
    
        var rewardCell: UITableViewCell {
            return tableView.dequeueReusableCell(withIdentifier: RewardCell.ID)!
        }
    var registrationCodeCell: UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: RegistrationCodeCell.ID)!
    }
    var assetsCell: UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: AssetsCell.ID)!
    }
    var clubBalanceCell: UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: ClubBalanceCell.ID)!
    }
    var sliderArray = [JSON]()
    var referalCode = String()
        
        override func viewDidLoad() {
            super.viewDidLoad()

            
            sendPaymentTitleLbl.text = lang.sendPayment
            myEpinTitleLbl.text = lang.myEpin
            transactionHistoryTitleLbl.text = lang.transactionHistory
            upgradePakangeTitleLbl.text = lang.upgradePackage
            referTitleLbl.text = lang.refer

            collectionView.delegate = self
            collectionView.dataSource = self
            
            let cellWidth : CGFloat = collectionView.frame.size.width
            let cellheight : CGFloat = collectionView.frame.size.height
            let cellSize = CGSize(width: cellWidth , height:cellheight)

            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.itemSize = cellSize
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            collectionView.setCollectionViewLayout(layout, animated: true)
            
            collectionView.reloadData()
            NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.currencyTabbedObserver),
            name: NSNotification.Name(rawValue: "CurrencyTapped"),
            object: nil)

            
            NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("upgradepackagepush"), object: nil)
            
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification1(notification:)), name: Notification.Name("payment"), object: nil)

            
            NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedPackageUpgradeNotification(notification:)), name: Notification.Name("packageupgradepush"), object: nil)

                NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationchip(notification:)), name: Notification.Name("chipexchange"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationchip(notification:)), name: Notification.Name("topup"), object: nil)

            
            let lang = UserDefaults.standard.string(forKey: "lang")
            
            if lang == "zh" {
                self.flagBtn.setImage(UIImage(named: "chineseFlag"), for: .normal)
                
            }
            else{
                UserDefaults.standard.set("en", forKey:  "lang")
               self.flagBtn.setImage(UIImage(named: "india"), for: .normal)
            }
            }
    
    @objc func methodOfReceivedNotification(notification: Notification) {

        self.push(id: "PackageViewController", animation: true, fromSB: Profile)

    }
    
    
    @objc func methodOfReceivedNotification1(notification: Notification) {

        self.push(id: "PaymentViewController", animation: true, fromSB: Profile)

    }

    
    @objc func methodOfReceivedNotificationchip(notification: Notification) {

//        callCurrencyListApi()
//        callPromotionsApi()
//        viewprofile()
//        callHomeApi()
//        callHomePackageDetailApi()
//        callHomeGetReferCodeApi()
        
        let vc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainTabbarViewController") as? MainTabbarViewController
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as? AppDelegate)!
        appDelegate.window?.rootViewController = vc


    }
    
    @objc func methodOfReceivedPackageUpgradeNotification(notification: Notification) {

       // self.push(id: "PackageUpgradeViewController", animation: true, fromSB: Tabbar)
        
        let packageUpgradeVc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "PackageUpgradeViewController") as? PackageUpgradeViewController
        packageUpgradeVc?.packageNeme = self.currentPackage
        self.navigationController?.pushViewController(packageUpgradeVc!, animated: true)

    }
    
    @IBAction func langChangeBtnaction(_ sender: Any) {
        

        
        self.dropDown.dataSource = self.langArr
            self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                
                if item == "English" {
                    
                    UserDefaults.standard.set("en", forKey:  "lang")
                    
                    UIView.appearance().semanticContentAttribute = Language.default.getSemantic
                    self.flagBtn.setImage(UIImage(named: "india"), for: .normal)
                    
                    self.changeToLanguage("en")
                }
                else
                {
                    UserDefaults.standard.set("zh", forKey:  "lang")
                    UIView.appearance().semanticContentAttribute = Language.default.getSemantic
                    self.flagBtn.setImage(UIImage(named: "chineseFlag"), for: .normal)
                    self.changeToLanguage("zh")
                }
            
                self.dropDown.hide()

            }
            
           dropDown.anchorView = self.flagBaseView
            dropDown.borderColor = UIColor.black
            dropDown.backgroundColor = UIColor.white
            dropDown.borderLineWidth = 1.0
            
            dropDown.show()
    }
    
    
    private func changeToLanguage(_ langCode: String) {
        
        
        UserDefaults.standard.set([langCode], forKey: "AppleLanguages")
        UserDefaults.standard.set(langCode, forKey: "lang")
        UserDefaults.standard.synchronize()

        let vc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainTabbarViewController") as? MainTabbarViewController
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as? AppDelegate)!
        appDelegate.window?.rootViewController = vc

        
         /*   let message = "In order to change the language, the App must be closed and reopened by you."
            let confirmAlertCtrl = UIAlertController(title: "App restart required", message: message, preferredStyle: .alert)

            let confirmAction = UIAlertAction(title: "Close now", style: .destructive) { _ in
                UserDefaults.standard.set(langCode, forKey: "lang")
                UserDefaults.standard.synchronize()
                //self.viewDidLoad()
                exit(0)
            }
            confirmAlertCtrl.addAction(confirmAction)

            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            confirmAlertCtrl.addAction(cancelAction)

            present(confirmAlertCtrl, animated: true, completion: nil)*/
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.expandableDelegate = self
        tableView.animation = .automatic
        callCurrencyListApi()
        callPromotionsApi()
        viewprofile()
        callHomeApi()
        announcements()
        callHomePackageDetailApi()
        callHomeGetReferCodeApi()
        
            tableView.register(UINib(nibName: "RewardCell", bundle: nil), forCellReuseIdentifier: RewardCell.ID)
        tableView.register(UINib(nibName: "RegistrationCodeCell", bundle: nil), forCellReuseIdentifier: RegistrationCodeCell.ID)
        tableView.register(UINib(nibName: "AssetsCell", bundle: nil), forCellReuseIdentifier: AssetsCell.ID)
        tableView.register(UINib(nibName: "ClubBalanceCell", bundle: nil), forCellReuseIdentifier: ClubBalanceCell.ID)
            
            tableView.register(UINib(nibName: "ExpandableCell", bundle: nil), forCellReuseIdentifier: ExpandableCell1.ID)
    }
    @IBAction func currentPackageBtnAction(_ sender: Any) {
        
        let currVc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "CurrentPackageViewController") as? CurrentPackageViewController
        currVc?.currentPackage = self.currentPackage
        currVc?.datas = packageDatas
        currVc?.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(currVc!, animated: false, completion: nil)
    }
    
    @IBAction func sendPaymentBtnAction(_ sender: Any) {
        
        
        let currVc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "CurrencyViewController") as? CurrencyViewController
         currVc?.modalPresentationStyle = .overCurrentContext
         self.navigationController?.present(currVc!, animated: false, completion: nil)

//        if self.kycStatus == "KYC not submitted"{
//
//            Common.checkRemoveCdAlert()
//
//            let alert = CDAlertView(title: AppName, message: "Please submit your KYC", type: .error)
//            alert.circleFillColor = UIColor.buttonprimary
//
//            alert.autoHideTime = 2.0
//            alert.show()
//        }
//        else
//        {
//       let currVc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "CurrencyViewController") as? CurrencyViewController
//        currVc?.modalPresentationStyle = .overCurrentContext
//        self.navigationController?.present(currVc!, animated: false, completion: nil)
//        }
        
    }
    
    @objc  func currencyTabbedObserver(notification: NSNotification){
        
        print(notification.object!)
        let scanPayVc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "ScanAndPayViewController") as? ScanAndPayViewController
        scanPayVc!.selectedCurr = notification.object! as! JSON
        self.navigationController?.pushViewController(scanPayVc!, animated: true)
        //referVc?.referalCode = self.referalCode
        //self.navigationController?.present(referVc!, animated: false, completion: nil)
        //do stuff using the userInfo property of the notification object
    }
    
    @IBAction func referBtnAction(_ sender: Any) {
        
        
        let referVc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "ReferViewController") as? ReferViewController
        referVc?.referalCode = self.referalCode
        referVc?.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(referVc!, animated: false, completion: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
                super.viewDidAppear(animated)
                
        //        tableView.openAll()
            }
    
    
    @IBAction func transactionact(_ sender: Any) {
        
        fromtranscation = "home"
        self.push(id: "TransactionsViewController", animation: true, fromSB: Tabbar)

    }
    @IBAction func upgradepackageact(_ sender: Any) {
        
        
       // let referVc = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "UpgradePackageViewController") as? UpgradePackageViewController
       // referVc?.modalPresentationStyle = .overCurrentContext
       // self.navigationController?.present(referVc!, animated: false, completion: nil)

       if current_package == "FREE"
        {
             let referVc = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "UpgradePackageViewController") as? UpgradePackageViewController
             referVc?.modalPresentationStyle = .overCurrentContext
             self.navigationController?.present(referVc!, animated: false, completion: nil)

         }

        else
        {
           self.push(id: "PackageViewController", animation: true, fromSB: Profile)

        }
    }
    
    @IBAction func myepinsact(_ sender: Any) {
        
        self.push(id: "EpinViewController", animation: true, fromSB: Profile)

    }
    func callCurrencyListApi() {

        APIManager.shared.currency_list() { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
           
            if let _ = response["currency_list"].array {

                APPDELEGATE.currenctListArry = response["currency_list"].arrayValue

            }
                    else {
                        Common.checkRemoveCdAlert()

                        let alert = CDAlertView(title: AppName, message: "Currency list", type: .error)
                        alert.circleFillColor = UIColor.buttonprimary

                        alert.autoHideTime = 2.0
                        alert.show()

                        
            }
             
                                            
        }
    }
    
    func callPromotionsApi() {

        APIManager.shared.Promotions() { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
           
            if let _ = response["promotions"].array {

                let promotionArray = response["promotions"].arrayValue
                
                self.sliderArray = promotionArray
                
                //self.sliderCountBtn.setTitle("\(self.sliderArray.count)", for: .normal)
                self.sliderCountBtn.setTitle("\(1)", for: .normal)
                
                self.collectionView.reloadData()

            }
                    else {
                       
                Common.checkRemoveCdAlert()

                        let alert = CDAlertView(title: AppName, message: "PROMOTIONS", type: .error)
                        alert.circleFillColor = UIColor.buttonprimary

                        alert.autoHideTime = 2.0
                        alert.show()

                        
            }
             
                                            
        }
    }
    
    func viewprofile()
    {
                    let params1 = ["userid":customer_id]
                               
        
                    print(params1)
        
                                APIManager.shared.view_profile(params: params1 as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                                    if response.count > 0 {
                                        self.kycStatus = response["kyc_status"].stringValue

                                                         let name = response["firstname"].stringValue

                                                         UserDefaults.standard.set(name, forKey: "username")
                                                         

                                                                     }
                                                                             else {
                                        Common.checkRemoveCdAlert()

                                                                                 
                                                                                 let alert = CDAlertView(title: AppName, message: "View Profile", type: .error)
                                                                                 alert.circleFillColor = UIColor.buttonprimary

                                                                                 alert.autoHideTime = 2.0
                                                                                 alert.show()

                                                                                 
                                                                     }
                                    
                          
                                    
        }
    }
    
    func announcements()
    {
                    let params1 = [":":":"]
                               
        
                    print(params1)
        
                                APIManager.shared.announcements(params: params1 as [String : AnyObject]) { (response) in
                                    print("announcements params1 \(response)")
                                    
                                    
                                    let dict = response["announcements"].arrayValue

                                 //   self.announcementsArray.removeAll()
                               
                                    if dict.count > 0
                                    {
                                        
                                        for i in 0...dict.count - 1
                                        {
                                            let dicts = dict[i].dictionaryValue
                                            
                                            if let id = dicts["date"]
                                            {
                                            let date = dicts["date"]!.stringValue
                                            }
                                            
                                            if let id = dicts["announcement"]
                                            {
                                            let announce = dicts["announcement"]!.stringValue
                                            self.announcementsArray.append(announce)
                                            }
                                            
                                            if i == dict.count - 1

                                            {
                                                   self.tctnoticeLbl.text = self.announcementsArray.joined(separator: ",")
                                                self.tctnoticeLbl.type = .continuous
                                                self.tctnoticeLbl.animationCurve = .linear

                                            }
                                        }
                                        

                                    }
                                    
                                    else
                                    {
                                       self.tctnoticeLbl.text = "TCT Announcements"
                                        self.tctnoticeLbl.type = .continuous
                                        self.tctnoticeLbl.animationCurve = .linear

                                    }
                                    
                                    
                                    
                          
                                    
        }
    }
    
    func callHomeApi() {
        let params = ["userid":customer_id]
                                print(params)
        
                                APIManager.shared.Home(params: params as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                                    if let _ = response["home"].dictionary {

                                        current_package = response["home"]["current_package"].stringValue
                                        print("current_package \(current_package)")
                                        self.homeDataDict = response["home"]
                                        self.tableView.reloadData()

                                    }
                                            else {
                                                Common.checkRemoveCdAlert()

                                                let alert = CDAlertView(title: AppName, message: "HOME", type: .error)
                                                alert.circleFillColor = UIColor.buttonprimary

                                                alert.autoHideTime = 2.0
                                                alert.show()

                                                
                                    }

        }
    }
    func callHomePackageDetailApi() {
        let params = ["userid":customer_id]
                                print(params)
        
                                APIManager.shared.Home_package_info(params: params as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                                    
                                    
                                    
                                    if let _ = response["package_info"].dictionary {

                                        self.packageDatas = response["package_info"].dictionaryValue

                                        let package_info = response["package_info"].dictionary
                                        
                                        self.currentPackage = String(describing: package_info!["package_name"]!)
                                        self.currentPakangeLbl.text = "\(self.lang.currentPackage) : \(String(describing: package_info!["package_name"]!))"
                                    }
                                            else {
                                                Common.checkRemoveCdAlert()

                                                let alert = CDAlertView(title: AppName, message: "PACKAGE", type: .error)
                                                alert.circleFillColor = UIColor.buttonprimary

                                                alert.autoHideTime = 2.0
                                                alert.show()

                                                
                                    }
                                
        }
    }
    
    func callHomeGetReferCodeApi() {
        let params = ["userid":customer_id]
                                print(params)
        
                                APIManager.shared.Home_get_referralcode(params: params as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                                    
                                    if let _ = response["referral_code"].dictionary {

                                        let referral_code = response["referral_code"].dictionary
                                        self.referalCode = referral_code!["referral_code"]!.stringValue
                        
                                    }
                                            else {
                                                
                                        Common.checkRemoveCdAlert()

                                                let alert = CDAlertView(title: AppName, message: "PACKAGE", type: .error)
                                                alert.circleFillColor = UIColor.buttonprimary

                                                alert.autoHideTime = 2.0
                                                alert.show()

                                                
                                    }
        }
    }
    
    func callTopUpshowApi() {
        
        var currId = String()
        
        for dict in APPDELEGATE.currenctListArry {
            
            if "\(dict["currency_symbol"].stringValue)" == "TCT"
            {
               currId = "\(dict["id"].stringValue)"
            }
        }
        
        let params = ["userid":customer_id,"currency":currId]
                                print(params)
        
                                APIManager.shared.TopUpShow(params: params as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                                    
                                    if response.count > 0 {

                                                let topUpVc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "TopUpViewController") as? TopUpViewController
                                                 topUpVc!.topUpDict = response
                                                 topUpVc!.currId = currId
                                                topUpVc?.modalPresentationStyle = .overCurrentContext
                                                self.navigationController?.present(topUpVc!, animated: false, completion: nil)
                        
                                    }
                                            else {
                                                Common.checkRemoveCdAlert()

                                                let alert = CDAlertView(title: AppName, message: "TOPUOSHOW", type: .error)
                                                alert.circleFillColor = UIColor.buttonprimary

                                                alert.autoHideTime = 2.0
                                                alert.show()

                                                
                                    }
        }
    }
    
    func callExchangeChipshowApi() {
        
        let dict = APPDELEGATE.currenctListArry.first
        print("\(dict!["id"].stringValue)")
        let params = ["userid":customer_id,"currency":"\(dict!["id"].stringValue)"]
                                print(params)
        
                                APIManager.shared.get_currency_balance(params: params as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")

                                    if response.count > 0 {

                                                let exchangeVc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "ExchangeViewController") as? ExchangeViewController
                                                 exchangeVc!.exchangeChipDict = response
                                                exchangeVc?.modalPresentationStyle = .overCurrentContext
                                                self.navigationController?.present(exchangeVc!, animated: false, completion: nil)
                        
                                    }
                                            else {
                                        Common.checkRemoveCdAlert()

                                                
                                                let alert = CDAlertView(title: AppName, message: "EXCHANGECHIPS", type: .error)
                                                alert.circleFillColor = UIColor.buttonprimary

                                                alert.autoHideTime = 2.0
                                                alert.show()

                                                
                                    }
        }
    }

    @IBAction func profileact(_ sender: Any) {
        
        self.push(id: "UserProfileListViewController", animation: true, fromSB: Profile)
    }
    
    @IBAction func notificationact(_ sender: Any) {
        self.push(id: "NotificationViewController", animation: true, fromSB: Profile)

    }
    
    override func didReceiveMemoryWarning() {
                super.didReceiveMemoryWarning()
                // Dispose of any resources that can be recreated.
            }
            
            @IBAction func openAllButtonClicked() {
                tableView.openAll()
            }
            
            @IBAction func expandMultiButtonClicked(_ sender: Any) {
                tableView.expansionStyle = .multi
            }
            
            @IBAction func expandSingleButtonClicked(_ sender: Any) {
                tableView.expansionStyle = .single
                tableView.closeAll()
            }
            
            @IBAction func closeAllButtonClicked() {
                tableView.closeAll()
            }
            
            @IBAction func SelectionDisplayOn(_ sender: UIButton) {
                tableView.autoRemoveSelection = !tableView.autoRemoveSelection
                let isOn = tableView.autoRemoveSelection ? "Off" : "On"
                sender.setTitle("Selection Stays \(isOn)", for: .normal)
            }
    
    @objc func topUpPressed(sender: UIButton!) {
    
        self.callTopUpshowApi()

    }
    @objc func exchangeChipPressed(sender: UIButton!) {
    
        self.callExchangeChipshowApi()

    }
    
    @objc func withDrawPressed(sender: UIButton!) {
    
        let referVc = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "WithdrawViewController") as? WithdrawViewController
        referVc?.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(referVc!, animated: false, completion: nil)

    }
            
            //scroll view methods are being forwarded correctly
            func scrollViewDidScroll(_ scrollView: UIScrollView) {
                print("scrollViewDidScroll")
            }
            
            func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
                print("scrollViewDidScroll, decelerate:\(decelerate)")
            }
            func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
                print("scrollViewDidEndScrollingAnimation")
            }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let indexPath = collectionView.indexPathsForVisibleItems.first {
            self.sliderCountBtn.setTitle("\(indexPath.row + 1)", for: .normal)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sliderArray.count
      }

      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
          //cell.backgroundColor = .red
          let sliderImgView = cell.viewWithTag(1) as!UIImageView
          let dict = self.sliderArray[indexPath.row]
          let imgUrl = URL(string: dict["promotion_image"].stringValue)!
          sliderImgView.af_setImage(withURL: imgUrl)
          return cell
      }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let dict = self.sliderArray[indexPath.row]

        let id = dict["promotion_url"].stringValue
        
        
        if id != "" && !id.isEmpty
        {
            if #available(iOS 10.0, *) {
                guard let url = URL(string: id) else { return }
                UIApplication.shared.open(url)
                
            } else {
                
                guard let url = URL(string: id) else { return }
                UIApplication.shared.openURL(url)
                
            }

        }

        
    }
        }

        extension TabHomeViewController: ExpandableDelegate {
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCellsForRowAt indexPath: IndexPath) -> [UITableViewCell]? {
                switch indexPath.section {
                case 0:
                    switch indexPath.row {
                    case 0:
                        print(indexPath.row)
                        let rewardCell = tableView.dequeueReusableCell(withIdentifier: RewardCell.ID) as! RewardCell
                       
                        rewardCell.personalTitleLabel.text = lang.rewards
                        rewardCell.commTitleLabel.text = lang.rewards
                        
                        
                        rewardCell.personalValLabel.text = String(describing: Double(self.homeDataDict["personal_rewards_tct"].stringValue)!)
                        rewardCell.commValLabel.text = String(describing: Double(self.homeDataDict["community_rewards_tct"].stringValue)!)
//                        rewardCell.baseView.roundCorners(corners: [.bottomLeft, .
//                        bottomRight], radius: 10.0)
                       // cell3.titleLabel.text = "Third Expanded Cell"
                        return [rewardCell]
                        
                     //   case 1:
//                         let registrationCodeCell = tableView.dequeueReusableCell(withIdentifier: RegistrationCodeCell.ID) as! RegistrationCodeCell
                        
//                         registrationCodeCell.baseView.roundCorners(corners: [.bottomLeft, .
//                         bottomRight], radius: 10.0)
                       //  return [registrationCodeCell]
                        
                    case 2:
                        let assetsCell = tableView.dequeueReusableCell(withIdentifier: AssetsCell.ID) as! AssetsCell
                        
                        assetsCell.tctValLabel.text = String(describing: Double(self.homeDataDict["TCT"].stringValue)!)
                        assetsCell.theClubTokenValLabel.text = String(describing: Double(self.homeDataDict["The Club Token"].stringValue)!)
                        assetsCell.ethValLabel.text = String(describing: Double(self.homeDataDict["ETH"].stringValue)!)
                        assetsCell.ethereumValLabel.text = String(describing: Double(self.homeDataDict["Ethereum"].stringValue)!)
                        assetsCell.btcValLabel.text = String(describing: Double(self.homeDataDict["BTC"].stringValue)!)
                        assetsCell.bitcoinValLabel.text = String(describing: Double(self.homeDataDict["Bitcoin"].stringValue)!)
                        assetsCell.usdtValLabel.text = String(describing: Double(self.homeDataDict["USDT"].stringValue)!)
                        assetsCell.tetherValLabel.text = String(describing: Double(self.homeDataDict["Tether"].stringValue)!)
                        
                        assetsCell.topupBtn.layer.cornerRadius = 10
                        assetsCell.topupBtn.layer.borderWidth = 1
                        assetsCell.topupBtn.layer.borderColor = UIColor.borderGold.cgColor
                        assetsCell.topupBtn.setTitle(lang.topup, for: .normal)
                        
                        assetsCell.topupBtn.addTarget(self, action: #selector(topUpPressed(sender:)), for: .touchUpInside)
                        
//                         assetsCell.baseView.roundCorners(corners: [.bottomLeft, .
//                         bottomRight], radius: 10.0)
                         return [assetsCell]
                    case 3:
                        let clubBalanceCell = tableView.dequeueReusableCell(withIdentifier: ClubBalanceCell.ID) as! ClubBalanceCell
                        
                        clubBalanceCell.exchangeBtn.layer.cornerRadius = 10
                        clubBalanceCell.exchangeBtn.layer.borderWidth = 1
                        clubBalanceCell.exchangeBtn.layer.borderColor = UIColor.borderGold.cgColor
                        
                        clubBalanceCell.withDrawBtn.layer.cornerRadius = 10
                        clubBalanceCell.withDrawBtn.layer.borderWidth = 1
                        clubBalanceCell.withDrawBtn.layer.borderColor = UIColor.borderGold.cgColor
                        
                        clubBalanceCell.digiChipTitleLabel.text = lang.digitalChips

                        clubBalanceCell.exchangeBtn.setTitle(lang.exchange, for: .normal)
                        
                        clubBalanceCell.withDrawBtn.setTitle(lang.withdraw, for: .normal)
                        
                        
                        clubBalanceCell.digiChipValLabel.text = String(describing: Double(self.homeDataDict["digital_chips"].stringValue)!)

                        clubBalanceCell.exchangeBtn.addTarget(self, action: #selector(exchangeChipPressed(sender:)), for: .touchUpInside)
                        
                        clubBalanceCell.withDrawBtn.addTarget(self, action: #selector(withDrawPressed(sender:)), for: .touchUpInside)
                        
//                         clubBalanceCell.baseView.roundCorners(corners: [.bottomLeft, .
//                         bottomRight], radius: 10.0)
                         return [clubBalanceCell]
                    
                    default:
                        break
                    }
                default:
                    break
                }
                return nil
            }
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, heightsForExpandedRowAt indexPath: IndexPath) -> [CGFloat]? {
                switch indexPath.section {
                case 0:
                    switch indexPath.row {
                    case 0:
                        return [60]
                        
                    case 1:
                        return [60]
                        
                    case 2:
                        return [255]
                        
                    case 3:
                        return [80]
                    
                    default:
                        break
                    }

                default:
                    break
                }
                return nil
                
            }
            
            func numberOfSections(in tableView: ExpandableTableView) -> Int {
                return parentCells.count
            }

            func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int {
                return parentCells[section].count
            }

            func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectRowAt indexPath: IndexPath) {
              //  let cell = expandableTableView.dequeueReusableCell(withIdentifier: parentCells[indexPath.section][indexPath.row]) as! ExpandableCell1

              ///  cell.baseView.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
                print("didSelectRow:\(indexPath)")
            }
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectExpandedRowAt indexPath: IndexPath) {
                print("didSelectExpandedRowAt:\(indexPath)")
            }
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCell: UITableViewCell, didSelectExpandedRowAt indexPath: IndexPath) {
//                if let cell = expandedCell as? ExpandedCell {
//                    print("\(cell.titleLabel.text ?? "")")
//                }
            }
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, titleForHeaderInSection section: Int) -> String? {
                return "Section:\(section)"
            }
            func expandableTableView(_ expandableTableView: ExpandableTableView, heightForHeaderInSection section: Int) -> CGFloat {
                return 0
            }
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let cell = expandableTableView.dequeueReusableCell(withIdentifier: parentCells[indexPath.section][indexPath.row]) as! ExpandableCell1

//                cell.baseView.roundCorners(corners: [.topLeft, .topRight, .bottomLeft
//                , .bottomRight], radius: 10.0)
//                cell.baseView.roundCorners(corners: [.bottomLeft
//                    , .bottomRight], radius: 10.0)
                
               if self.homeDataDict.count > 0
               {
                
                if indexPath.row == 0 {
                    
                    cell.titleLbl.text = lang.rewards
                    cell.firstHeadingLbl.text = "TCT"
                    cell.secondHeadingLbl.text = "USD"
                    
                    cell.firstValLbl.text = String(describing: Double(self.homeDataDict["rewards_tct"].stringValue)!)
                    cell.secondValLbl.text = String(describing: Double(self.homeDataDict["rewards__usd"].stringValue)!)
                }
                else if indexPath.row == 1 {
                    
                    cell.secondHeadingLbl.isHidden = false
                    cell.secondValLbl.isHidden = false
                    cell.firstHeadingLbl.text = "TCT"
                    cell.secondHeadingLbl.text = "USD"
                    
                    cell.titleLbl.text = lang.registrationCode
                    cell.firstValLbl.text = String(describing: Double(self.homeDataDict["reg_codes_tct"].stringValue)!)
                    cell.secondValLbl.text = String(describing: Double(self.homeDataDict["reg_codes_usd"].stringValue)!)
                }
                else if indexPath.row == 2 {
                    cell.titleLbl.text = lang.assets
                    
                    cell.firstHeadingLbl.text = "USD"
                    cell.firstValLbl.text = String(describing: Double(self.homeDataDict["assets"].stringValue)!)
                    
                    cell.secondHeadingLbl.isHidden = true
                    cell.secondValLbl.isHidden = true
                }
                else if indexPath.row == 3 {
                    
                    cell.secondHeadingLbl.isHidden = false
                    cell.secondValLbl.isHidden = false
                    
                    cell.titleLbl.text = lang.clubBalance
                    cell.firstHeadingLbl.text = "TCT"
                    cell.secondHeadingLbl.text = "USD"
                    
                    cell.firstValLbl.text = String(describing: Double(self.homeDataDict["club_balance_tct"].stringValue)!)
                    cell.secondValLbl.text = String(describing: Double(self.homeDataDict["club_balance_usd"].stringValue)!)
                }
                }
                //cell.
                return cell
            }
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                return 120
            }
            
            @objc(expandableTableView:didCloseRowAt:) func expandableTableView(_ expandableTableView: UITableView, didCloseRowAt indexPath: IndexPath) {
                let cell = expandableTableView.cellForRow(at: indexPath)
                cell?.contentView.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
                cell?.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
            }
            
            func expandableTableView(_ expandableTableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
                return true
            }
            
            func expandableTableView(_ expandableTableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        //        let cell = expandableTableView.cellForRow(at: indexPath)
        //        cell?.contentView.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        //        cell?.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            }
            
        //    func expandableTableView(_ expandableTableView: ExpandableTableView, titleForHeaderInSection section: Int) -> String? {
        //        return "Section \(section)"
        //    }
        //
        //    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForHeaderInSection section: Int) -> CGFloat {
        //        return 33
        //    }
        }





