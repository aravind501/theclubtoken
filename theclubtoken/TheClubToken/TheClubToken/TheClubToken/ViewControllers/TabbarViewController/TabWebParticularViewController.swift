//
//  TabWebParticularViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 18/07/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD
class TabWebParticularViewController: UIViewController,WKNavigationDelegate {

    @IBOutlet weak var casinowebview: WKWebView!
    var urlvalue = ""
    var titleValue = ""
    
    @IBOutlet weak var titleLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if titleValue != ""
        {
            titleLbl.text = titleValue

        }
        
        
        casinowebview.navigationDelegate = self
        casinowebview.isOpaque = false
        casinowebview.backgroundColor = .clear

        
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
        SVProgressHUD.setContainerView(self.topMostViewController().view)
        SVProgressHUD.show()

        if urlvalue != ""
        {
            let url1 = URL(string: urlvalue)

        self.casinowebview.load(URLRequest(url: url1!))

        }
        // Do any additional setup after loading the view.
    }
    @IBAction func fundHereBtnAction(_ sender: Any) {
        
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
        SVProgressHUD.setContainerView(self.topMostViewController().view)
        SVProgressHUD.show()

        var fundUrlvalue = ""
          
        if titleValue == "A11 CASINO" {
            fundUrlvalue = "https://ourtct.com/casino"
        }
        else if titleValue == "A11 BETS" {
            fundUrlvalue = "https://ourtct.com/a11bets"
        }
        else if titleValue == "A11 GAME" {
            fundUrlvalue = "https://ourtct.com/a11game"
        }
        
        let url1 = URL(string: fundUrlvalue)
        self.casinowebview.load(URLRequest(url: url1!))

    }
    
    @IBAction func backact(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

        
    }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        SVProgressHUD.dismiss()
        print("webview load")
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        SVProgressHUD.dismiss()
        print("webview error")
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
