//
//  ProfileHelpViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 05/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class ProfileHelpViewController: UIViewController {

    @IBOutlet weak var accountline: UIView!
    @IBOutlet weak var accountview: UIView!
    @IBOutlet weak var authenticationview: UIView!
    @IBOutlet weak var authenticationline: UIView!
    @IBOutlet weak var helpview: UIView!
    @IBOutlet weak var helpline: UIView!

    @IBOutlet weak var accountbtn: UIButton!
    
    @IBOutlet weak var authbtn: UIButton!
    @IBOutlet weak var helpbtn: UIButton!
    lazy var lang = Language.default.object
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var accountTiLbl: UILabel!
    @IBOutlet weak var authedicationTiLbl: UILabel!
    @IBOutlet weak var helpTiLbl: UILabel!
    @IBOutlet weak var supportTiLbl: UILabel!
    @IBOutlet weak var feedbackTiLbl: UILabel!
    @IBOutlet weak var faqTiLbl: UILabel!
    @IBOutlet weak var termsAndConditionTiLbl: UILabel!
    @IBOutlet weak var privacyPoliTiLbl: UILabel!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        
        titleLbl.text = lang.Help
        accountTiLbl.text = lang.Account
        authedicationTiLbl.text = lang.Authentication
        helpTiLbl.text = lang.Help
        
        supportTiLbl.text = lang.Support
        feedbackTiLbl.text = lang.Feedback
        faqTiLbl.text = lang.FAQtext
        termsAndConditionTiLbl.text = lang.TermsandConditions
        privacyPoliTiLbl.text = lang.PrivacyPolicy
        // Do any additional setup after loading the view.
    }
    @IBAction func backbtn(_ sender: Any) {
        
        
        let mainViewControllerVC = self.navigationController?.viewControllers.first(where: { (viewcontroller) -> Bool in
            return viewcontroller is UserProfileListViewController
        })
        if let mainViewControllerVC = mainViewControllerVC {
            navigationController?.popToViewController(mainViewControllerVC, animated: true)
        }

    }

    
    @IBAction func accountact(_ sender: Any) {
        
        self.push(id: "EditProfileViewController", animation: false, fromSB: Profile)
    }
    @IBAction func authenticationact(_ sender: Any) {
        self.push(id: "EditProfileAuthenticationViewController", animation: false, fromSB: Profile)
    }
    
    @IBAction func helpact(_ sender: Any) {
        
        
        
    }


    @IBAction func supportact(_ sender: Any) {
        
        
        self.push(id: "HelpdeskViewController", animation: true, fromSB: Profile)

    }
    @IBAction func feedbackact(_ sender: Any) {
        
        self.push(id: "FeedbackViewController", animation: true, fromSB: Profile)

    }
   
    @IBAction func faqsact(_ sender: Any) {
        
        
        self.push(id: "FAQViewController", animation: true, fromSB: Profile)

    }
    
    @IBAction func termsandconditions(_ sender: Any) {
        
        
        self.push(id: "TermsandConditionsViewController", animation: true, fromSB: Profile)

    }
    
    
    @IBAction func privacypolicy(_ sender: Any) {
        
        
        self.push(id: "PrivacyPolicyViewController", animation: true, fromSB: Profile)

    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
