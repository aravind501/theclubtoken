//
//  CurrencyViewController.swift
//  TheClubToken
//
//  Created by trioangle on 28/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class CurrencyViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return APPDELEGATE.currenctListArry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")!
        
        let currLbl = cell.viewWithTag(1) as! UILabel
        
        let dict = APPDELEGATE.currenctListArry[indexPath.row]
        currLbl.text = "\(dict["currency_symbol"].stringValue) (\(dict["currency_name"].stringValue))"
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = APPDELEGATE.currenctListArry[indexPath.row]
        print(dict)
        self.dismiss(animated: false, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CurrencyTapped"), object: dict)
    }
    

    @IBOutlet weak var currencyTitleLbl: UILabel!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var tableViewOutlet: UITableView!
    lazy var lang = Language.default.object
    override func viewDidLoad() {
        super.viewDidLoad()

        self.currencyTitleLbl.text = lang.Currency
        self.closeBtn.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysTemplate),
                                      for: .normal)
        self.closeBtn.tintColor = UIColor.white
        
        tableViewOutlet.delegate = self
        tableViewOutlet.dataSource = self
        tableViewOutlet.reloadData()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
