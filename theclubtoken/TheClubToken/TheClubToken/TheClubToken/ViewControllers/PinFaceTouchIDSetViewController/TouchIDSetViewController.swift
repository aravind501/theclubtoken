//
//  TouchIDSetViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 01/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import BiometricAuthentication
import CDAlertView
class TouchIDSetViewController: UIViewController {

    @IBOutlet weak var useitBtn: UIButton!
    @IBOutlet weak var skipbtn: UIButton!
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var backbtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    lazy var lang = Language.default.object
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if touchIDfrom == "userlogin"
        {
            self.backImg.isHidden = false
            self.backbtn.isHidden = false
            skipbtn.isHidden = true
        }
        else
        {
            self.backImg.isHidden = true
            self.backbtn.isHidden = true
            skipbtn.isHidden = false

        }
        
        titleLbl.text = lang.EnableBioAuthentication
        useitBtn.setTitle(lang.UseIt, for: .normal)
        skipbtn.setTitle(lang.SKIP, for: .normal)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backact(_ sender: Any) {
        self.popLeft()
    }
    @IBAction func UseritBtnact(_ sender: Any) {
        
        if  BioMetricAuthenticator.shared.touchIDAvailable() {
            checkBiometric()

        }
        
        else
        {
             Common.checkRemoveCdAlert()
                    let alert = CDAlertView(title: AppName, message: lang.sorryunabletoauthenticatetouchid, type: .error)
            alert.circleFillColor = UIColor.buttonprimary
            let doneAction = CDAlertViewAction(title: lang.OK, font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                    alert.add(action: doneAction)
                    alert.show() { (alert) in

                    }
        }
        
    }
    
    @IBAction func skipact(_ sender: Any) {
        
        UserDefaults.standard.set(true, forKey: "user_isLogin")

        self.push(id: "FaceIDSetViewController", animation: true, fromSB: Login)
    }
  
    
    func checkBiometric()
        {
            // start authentication
            BioMetricAuthenticator.authenticateWithBioMetrics(reason: "", success: {
               
                UserDefaults.standard.set(true, forKey: "user_isLogin")
                UserDefaults.standard.set("fingerenabled", forKey: "finger")
 
                
                 Common.checkRemoveCdAlert()
                let alert = CDAlertView(title: AppName, message: self.lang.Your_Authentication_Successfully_Enabled, type: .success)
                alert.circleFillColor = UIColor.buttonprimary
                let doneAction = CDAlertViewAction(title: self.lang.OK, font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                        alert.add(action: doneAction)
                        alert.show() { (alert) in

                            if touchIDfrom == "userlogin"

                            {
                                self.popLeft()
                            }
                            else
                            {
                            
                            self.push(id: "FaceIDSetViewController", animation: true, fromSB: Login)
                            }
                            
                        }
                
                
                
                
//                self.showAlertView(title: appNameShort, message: "Your Authentication Successfully Enabled") { (true) in
//
//                    if touchIDfrom == "userlogin"
//
//                    {
//                        self.popLeft()
//                    }
//                    else
//                    {
//
//                    self.push(id: "FaceIDSetViewController", animation: true, fromSB: Login)
//                    }
//                }
                
                
            }, failure: { [weak self] (error) in
                
                // do nothing on canceled
                if error == .canceledByUser || error == .canceledBySystem {
                   
                    print("cancelled by user")
                    return
                }
                    
                    // device does not support biometric (face id or touch id) authentication
                else if error == .biometryNotAvailable {
                    self?.showErrorAlert(message: error.message())
                }
                    
                    // show alternatives on fallback button clicked
                else if error == .fallback {
                    // here we're entering username and password
                    // self?.txtUsername.becomeFirstResponder()
                }
                    
                    // No biometry enrolled in this device, ask user to register fingerprint or face
                else if error == .biometryNotEnrolled {
                    self?.showGotoSettingsAlert(message: error.message())
                }
                    
                    // Biometry is locked out now, because there were too many failed attempts.
                    // Need to enter device passcode to unlock.
                else if error == .biometryLockedout {
                    self?.showPasscodeAuthentication(message: error.message())
                    
                }
                    
                    // show error on authentication failed
                else {
                    self?.showErrorAlert(message: error.message())
                }
            })
        }

        
        /*
        // MARK: - Navigation

        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            // Get the new view controller using segue.destination.
            // Pass the selected object to the new view controller.
        }
        */

    }


    extension TouchIDSetViewController {
        
        func showAlert(title: String, message: String) {
            
            let okAction = AlertAction(title: OKTitle)
            let alertController = getAlertViewController(type: .alert, with: title, message: message, actions: [okAction], showCancel: false) { (button) in
            }
            present(alertController, animated: true, completion: nil)
        }
        
        func showLoginSucessAlert() {
            
            //APPDELEGATE.updateHomeView()
            //showAlert(title: "Success", message: "Login successful")
        }
        
        func showErrorAlert(message: String) {
            showAlert(title: "Error", message: message)
        }
        
        func showGotoSettingsAlert(message: String) {
            let settingsAction = AlertAction(title: "Go to settings")
            
            let alertController = getAlertViewController(type: .alert, with: "Error", message: message, actions: [settingsAction], showCancel: true, actionHandler: { (buttonText) in
                if buttonText == CancelTitle { return }
                
                // open settings
                // let url = URL(string: "App-Prefs:root=TOUCHID_PASSCODE".localiz())
                let url = URL(string: UIApplication.openSettingsURLString)
                if UIApplication.shared.canOpenURL(url!) {
                    UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                }
                
            })
            present(alertController, animated: true, completion: nil)
        }
        
        func showPasscodeAuthentication(message: String) {
            
            BioMetricAuthenticator.authenticateWithPasscode(reason: message, success: {
                // passcode authentication success
                self.showLoginSucessAlert()
                
            }) { (error) in
                print(error.message())
            }
        }
    }

