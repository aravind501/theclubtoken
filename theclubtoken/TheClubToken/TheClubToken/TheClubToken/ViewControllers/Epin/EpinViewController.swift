//
//  EpinViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 28/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import SwiftyJSON

class EpinViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var backview: UIView!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var totalview: UIView!
    @IBOutlet weak var availableLbl: UILabel!
    @IBOutlet weak var availableview: UIView!
    @IBOutlet weak var usedLbl: UILabel!
    @IBOutlet weak var usedview: UIView!
    
    @IBOutlet weak var headertitleLbl: UILabel!
    @IBOutlet weak var epinTblview: UITableView!
    
    var searchDatas = JSON()
    lazy var lang = Language.default.object
    var epinDatas = [JSON]()
    @IBOutlet weak var norecordLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        epinTblview.delegate = self
        epinTblview.dataSource = self
        self.epinTblview.tableFooterView = UIView()

        headertitleLbl.text = lang.EPINs
        norecordLbl.text = lang.NoRecordsFound
        
        epinTblview.rowHeight = UITableView.automaticDimension
        epinTblview.estimatedRowHeight = 133.0

        
        backview.clipsToBounds = true
        backview.layer.cornerRadius = 50
        if #available(iOS 11.0, *) {
            backview.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("createpin"), object: nil)

      viewepins()
        // Do any additional setup after loading the view.
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        viewepins()

    }
    
    override func viewDidLayoutSubviews() {
        if #available(iOS 11.0, *){
            
        }else{
           let rectShape = CAShapeLayer()
           rectShape.bounds = view.frame
           rectShape.position = view.center
           rectShape.path = UIBezierPath(roundedRect: view.bounds,    byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 50 , height: 50)).cgPath
           backview.layer.mask = rectShape
    }
    
    }
    
    func viewepins()
    {
                    let params1 = ["userid":customer_id]
                               
        
        print(params1)
        
                                APIManager.shared.user_epins(params: params1 as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    
                                
                                    
                                    self.totalLbl.text = "\(self.lang.Total)" + "\n" + response["total_epins"].stringValue
                                    self.availableLbl.text = "\(self.lang.Available)" + "\n" + response["available_epins"].stringValue
                                    self.usedLbl.text = "\(self.lang.Used)" + "\n" + response["used_epins"].stringValue

                                    self.searchDatas = response
                                    
                                    self.epinDatas = self.searchDatas["total_epins_list"].arrayValue
                                    
                                    if self.epinDatas.count > 0
                                    {
                                        self.epinTblview.isHidden = false
                                        self.norecordLbl.isHidden = true
                                        self.epinTblview.reloadData()
                                    }
                                    
                                    else
                                    {
                                        self.epinTblview.isHidden = true
                                        self.norecordLbl.isHidden = false

                                    }
                                    
                                    
        }
    }
    @IBAction func backact(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createepinact(_ sender: Any) {
       
        
        self.push(id: "CreateEpinViewController", animation: true, fromSB: Profile)

    }
    
    @IBAction func totalbtn(_ sender: Any) {
        
        totalview.isHidden = false
        availableview.isHidden = true
        usedview.isHidden = true
        
        epinDatas = searchDatas["total_epins_list"].arrayValue
        
        if self.epinDatas.count > 0
        {
            self.epinTblview.isHidden = false
            self.norecordLbl.isHidden = true
            self.epinTblview.reloadData()
        }
        
        else
        {
            self.epinTblview.isHidden = true
            self.norecordLbl.isHidden = false

        }
    }
    
    @IBAction func availableBtn(_ sender: Any) {
        
        totalview.isHidden = true
        availableview.isHidden = false
        usedview.isHidden = true

        epinDatas = searchDatas["available_epins_list"].arrayValue
        if self.epinDatas.count > 0
        {
            self.epinTblview.isHidden = false
            self.norecordLbl.isHidden = true
            self.epinTblview.reloadData()
        }
        
        else
        {
            self.epinTblview.isHidden = true
            self.norecordLbl.isHidden = false

        }
    }
    
    @IBAction func usedBtn(_ sender: Any) {
        
        totalview.isHidden = true
        availableview.isHidden = true
        usedview.isHidden = false

        epinDatas = searchDatas["used_epins_list"].arrayValue
        if self.epinDatas.count > 0
        {
            self.epinTblview.isHidden = false
            self.norecordLbl.isHidden = true
            self.epinTblview.reloadData()
        }
        
        else
        {
            self.epinTblview.isHidden = true
            self.norecordLbl.isHidden = false

        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return epinDatas.count
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }



    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
               
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! epinCell
        
        
        let dict = epinDatas[indexPath.row].dictionaryValue
        
        cell.packageLbl.text = "\(lang.Package) " + dict["package"]!.stringValue
        
        cell.codeLbl.text = dict["epincode"]?.stringValue

        
        cell.copyBtn.setImage(UIImage(named: "copy")?.withRenderingMode(.alwaysTemplate),
                                      for: .normal)
        cell.copyBtn.tintColor = UIColor.white

        
        let stat = dict["gift_status"]?.stringValue
        
        if stat == "yes"
        {
            cell.giftbtn.isHidden = false
            cell.giftImg.isHidden = false
        }
        else
        {
            cell.giftbtn.isHidden = true
            cell.giftImg.isHidden = true

        }
       // cell.copyImg.image = UIImage(named : "copy")
      //  cell.copyImg.image = cell.copyImg.image?.withRenderingMode(.alwaysTemplate)
      //  cell.copyImg.tintColor = UIColor.white

        
        cell.copyBtn.tag = indexPath.row
        cell.copyBtn.addTarget(self, action: #selector(buttonSelected), for: .touchUpInside)

        
        cell.qrBtn.tag = indexPath.row
        cell.qrBtn.addTarget(self, action: #selector(buttonSelected1), for: .touchUpInside)

        
        cell.giftbtn.tag = indexPath.row
        cell.giftbtn.addTarget(self, action: #selector(buttonSelected2), for: .touchUpInside)

        
        return cell

    }
    
    @objc func buttonSelected(sender: UIButton){
        print(sender.tag)
        
        sender.blink()
        let dict = epinDatas[sender.tag].dictionaryValue

        let code = dict["epincode"]?.stringValue
        UIPasteboard.general.string = code
        self.view.make(toast: "Epin Code Copied")
    }

    @objc func buttonSelected1(sender: UIButton){
        print(sender.tag)
        
        let dict = epinDatas[sender.tag].dictionaryValue
        
        let referVc = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "EpinBarCodeViewController") as? EpinBarCodeViewController
        referVc?.modalPresentationStyle = .overCurrentContext
        referVc?.from = "myepin"
        if let id = dict["package"]
        {
            referVc?.package = dict["package"]!.stringValue
        }
        
        if let id = dict["epincode"]
        {
            referVc?.code = dict["epincode"]!.stringValue
        }

        if let id = dict["barcode"]
        {
            referVc?.barcode = dict["barcode"]!.stringValue
        }


        self.navigationController?.present(referVc!, animated: false, completion: nil)


    }

    
    @objc func buttonSelected2(sender: UIButton){
        print(sender.tag)
        
        let dict = epinDatas[sender.tag].dictionaryValue

        

        
        let referVc = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "EpinShareViewController") as? EpinShareViewController
        referVc?.modalPresentationStyle = .overCurrentContext
      
        if let id = dict["epincode"]
        {
            referVc?.epinid = dict["epincode"]!.stringValue
        }
        self.navigationController?.present(referVc!, animated: false, completion: nil)

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class epinCell : UITableViewCell
{
    @IBOutlet weak var packageLbl: UILabel!
    @IBOutlet weak var codeLbl: UILabel!
    @IBOutlet weak var copyImg: UIImageView!
    @IBOutlet weak var qrImg: UIImageView!
    @IBOutlet weak var giftImg: UIImageView!
    @IBOutlet weak var copyBtn: UIButton!
    @IBOutlet weak var qrBtn: UIButton!
    
    @IBOutlet weak var giftbtn: UIButton!
}

extension UIView{
    
     func blink() {
         self.alpha = 0.2
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [.autoreverse], animations: {self.alpha = 1.0}, completion: nil)
     }
}

