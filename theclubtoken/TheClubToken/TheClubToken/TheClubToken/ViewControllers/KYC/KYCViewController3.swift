//
//  KYCViewController3.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 04/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CDAlertView
import SVProgressHUD
import DropDown
class KYCViewController3: UIViewController {

    @IBOutlet weak var identifiNumberTxtFld: HoshiTextField!
    @IBOutlet weak var identifiTypeTxtFld: HoshiTextField!
    
    @IBOutlet weak var oploadIdentityTwoTiLbl: UILabel!
    @IBOutlet weak var backTiLbl: UILabel!
    @IBOutlet weak var uploadIdentifyTiLbl: UILabel!
    @IBOutlet weak var frontTiLbl: UILabel!
    @IBOutlet weak var prooffront: UIImageView!
    @IBOutlet weak var previousTitLbl: UILabel!
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var proofbackbtn: UIButton!
    @IBOutlet weak var prooffrontbtn: UIButton!
    @IBOutlet weak var proofback: UIImageView!
    @IBOutlet weak var frontview: UIView!
    @IBOutlet weak var backview: UIView!
    
    @IBOutlet weak var proofforntLbl: UILabel!
    @IBOutlet weak var proofbackLbl: UILabel!
    var front = false
    var back = false
    lazy var lang = Language.default.object
    @IBOutlet weak var bottomview: UIView!
    let dropDown = DropDown()
    var dropDownList = ["Driving Licence","Passport"]

    @IBOutlet weak var downarrow: UIImageView!
    @IBOutlet weak var typelbl1: UILabel!
    
    @IBOutlet weak var typelbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        frontview.addDashedBorder()
        backview.addDashedBorder()
        
        setDesigns()
        typelbl.text = ""
        typelbl1.text = ""
        
        downarrow.image = downarrow.image?.withRenderingMode(.alwaysTemplate)
        downarrow.tintColor = UIColor.buttonsecondary

        
        frontTiLbl.text = lang.Front
        backTiLbl.text = lang.Back
        uploadIdentifyTiLbl.text = lang.UploadIdentity
        oploadIdentityTwoTiLbl.text = lang.UploadIdentity
        previousTitLbl.text = lang.Previous
        
        identifiNumberTxtFld.placeholder = lang.IdentificationNumber
        identifiTypeTxtFld.placeholder = lang.IdentificationType
        submitBtn.setTitle(lang.Submit, for: .normal)
        
        // Do any additional setup after loading the view.
    }
    
 
    func setDesigns(){
        
        self.identifiNumberTxtFld.borderActiveColor = .textcolor
        self.identifiNumberTxtFld.borderInactiveColor = .buttonsecondary
        self.identifiNumberTxtFld.placeholderColor = .textcolor
        self.identifiNumberTxtFld.textColor = .textcolor
        self.identifiNumberTxtFld.delegate = self
        
        self.identifiTypeTxtFld.borderActiveColor = .textcolor
        self.identifiTypeTxtFld.borderInactiveColor = .buttonsecondary
        self.identifiTypeTxtFld.placeholderColor = .textcolor
        self.identifiTypeTxtFld.textColor = .textcolor
        self.identifiTypeTxtFld.delegate = self
       

    }
    
    @IBAction func identificationact(_ sender: Any) {
        
        self.dropDown.dataSource = dropDownList
            self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                
                self.identifiTypeTxtFld.text = item
                self.typelbl.text = item
                self.typelbl1.text = item
                self.dropDown.hide()

            }
            
           dropDown.anchorView = self.bottomview
            dropDown.borderColor = UIColor.black
            dropDown.backgroundColor = UIColor.white
            dropDown.borderLineWidth = 1.0
            
            dropDown.show()

    }
    
    @IBAction func backact(_ sender: Any) {
       // self.popLeftWithoutAnimation()
        
        let mainViewControllerVC = self.navigationController?.viewControllers.first(where: { (viewcontroller) -> Bool in
            return viewcontroller is UserProfileListViewController
        })
        if let mainViewControllerVC = mainViewControllerVC {
            navigationController?.popToViewController(mainViewControllerVC, animated: true)
        }
    }
    
    @IBAction func previousact(_ sender: Any) {
        //self.popLeftWithoutAnimation()
        
        let mainViewControllerVC = self.navigationController?.viewControllers.first(where: { (viewcontroller) -> Bool in
            return viewcontroller is KYCViewController2
        })
        if let mainViewControllerVC = mainViewControllerVC {
            navigationController?.popToViewController(mainViewControllerVC, animated: false)
        }

    }
    @IBAction func nextact(_ sender: Any) {
        
        
        
        if identifiTypeTxtFld.text == ""
        {
            self.view.make(toast: lang.PleaseSeletIdentificationType)
        }
        else if identifiNumberTxtFld.text == ""
        {
            self.view.make(toast: lang.IdentificationNumber)
        }
        else if front == false
        {
            self.view.make(toast: lang.PleaseUploadIdentificationProofFront)

        }

        else if back == false
        {
            self.view.make(toast: lang.PleaseUploadIdentificationProofBack)

        }
else
        {
            SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
            SVProgressHUD.setContainerView(self.view)
            SVProgressHUD.show()
//
//            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
//                SVProgressHUD.dismiss()
//                 Common.checkRemoveCdAlert()
//                        let alert = CDAlertView(title: AppName, message: "Your KYC details have been sent to our team for verification", type: .success)
//                alert.circleFillColor = UIColor.buttonprimary
//                let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
//                        alert.add(action: doneAction)
//                        alert.show() { (alert) in
//
//                            let mainViewControllerVC = self.navigationController?.viewControllers.first(where: { (viewcontroller) -> Bool in
//                                return viewcontroller is UserProfileListViewController
//                            })
//                            if let mainViewControllerVC = mainViewControllerVC {
//                                self.navigationController?.popToViewController(mainViewControllerVC, animated: true)
//                            }
//
//                        }
//
//            }

            
            
            
            
            let imgupload = [ImageUpload(uploadimage: self.prooffront.image!, filename: "proof_front")]

                       let imgupload1 = [ImageUpload(uploadimage: self.proofback.image!, filename: "proof_back")]

                       let params = ["userid": customer_id,
                                     "proof_type":identifiTypeTxtFld.text!,
                                     "id_number":identifiNumberTxtFld.text!]



            APIManager.shared.kyc_img_update(params: params as [String : AnyObject], profimage: imgupload, profimage1: imgupload1) { (response) in

                SVProgressHUD.dismiss()

                print("venki\(response)")

                let dict = response["kyc"].dictionaryValue
                let msg = dict["msg"]!.stringValue
                let status = dict["status"]!.stringValue


                        if status == "success"
                        {
                         Common.checkRemoveCdAlert()
                                let alert = CDAlertView(title: AppName, message: msg, type: .success)
                        alert.circleFillColor = UIColor.buttonprimary
                        let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                alert.add(action: doneAction)
                                alert.show() { (alert) in

                                    self.push(id: "UserProfileListViewController", animation: true, fromSB: Profile)

                                    //self.push(id: "EditProfileViewController", animation: true, fromSB: Profile)

                                }
                        }


                        else
                        {
                            let alert = CDAlertView(title: AppName, message: msg, type: .error)
                            alert.circleFillColor = UIColor.buttonprimary

                            alert.autoHideTime = 2.0
                            alert.show()

                        }
                       }
        }

        
        
    }
    
    @IBAction func prooffront(_ sender: UIButton) {
        
        MediaPicker.shared.showMediaPicker(imageView: prooffront!, placeHolder: nil, sender: sender) { (img, check) in
            
            if check == true {
                
                self.front = true
            }
            
        }
    }
    
    @IBAction func proofback(_ sender: UIButton) {
        
        MediaPicker.shared.showMediaPicker(imageView: proofback!, placeHolder: nil, sender: sender) { (img, check) in
            
            if check == true {
                
                self.back = true
            }
            
        }

    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
 extension KYCViewController3 : UITextFieldDelegate {
     
     func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         
         return textField.resignFirstResponder()
     }
     
     func textFieldDidBeginEditing(_ textField: UITextField) {
         
         if textField == identifiNumberTxtFld
         {
         identifiNumberTxtFld.placeholder = lang.IdentificationNumber
         }
         
         else if textField == identifiTypeTxtFld
         {
         identifiTypeTxtFld.placeholder = lang.IdentificationType
         }
                  


     }
    

     
     func textFieldDidEndEditing(_ textField: UITextField) {
        
         if textField.text?.count == 0 {
             
             
             
             if textField == identifiNumberTxtFld
             {
                
             identifiNumberTxtFld.placeholder = lang.IdentificationNumber
                 self.identifiNumberTxtFld.borderInactiveColor = .buttonsecondary

             }
             
             else if textField == identifiTypeTxtFld
             {
                self.proofforntLbl.isHidden = true
                self.proofbackLbl.isHidden = true

             identifiTypeTxtFld.placeholder = lang.IdentificationType
                 self.identifiTypeTxtFld.borderInactiveColor = .buttonsecondary

             }
                          
         }
        
        else
         {
             if textField == identifiTypeTxtFld
            {
            self.proofforntLbl.isHidden = false
            self.proofbackLbl.isHidden = false

            }

        }
     }
     
 }
