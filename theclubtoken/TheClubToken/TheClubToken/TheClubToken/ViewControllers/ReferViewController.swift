//
//  ReferViewController.swift
//  TheClubToken
//
//  Created by trioangle on 26/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CDAlertView
class ReferViewController: UIViewController {

    @IBOutlet weak var referCodeLbl: UILabel!
    @IBOutlet weak var referAndEarnBtn: UIButton!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var copyBtn: UIButton!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var backbtn: UIButton!
    
    @IBOutlet weak var placementView: UIView!
    var referalCode = String()
    
    @IBOutlet weak var leftImg: UIImageView!
    @IBOutlet weak var autoImg: UIImageView!
    @IBOutlet weak var rightImg: UIImageView!
    @IBOutlet weak var referAndEarnLbl: UILabel!
    @IBOutlet weak var clickBelowToShareLbl: UILabel!
    @IBOutlet weak var byInvitingThemLbl: UILabel!
    @IBOutlet weak var placeMentLbl: UILabel!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var leftLbl: UILabel!
    @IBOutlet weak var autoLbl: UILabel!
    @IBOutlet weak var rightLbl: UILabel!
    
    var selectedPlacement = ""
    var get_refer_code = ""
    lazy var lang = Language.default.object
    override func viewDidLoad() {
        super.viewDidLoad()

        self.referCodeLbl.text = self.referalCode
        
        self.closeBtn.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysTemplate),
                                      for: .normal)
        self.closeBtn.tintColor = UIColor.white
        
        self.copyBtn.setImage(UIImage(named: "copy")?.withRenderingMode(.alwaysTemplate),
                                      for: .normal)
        self.copyBtn.tintColor = UIColor.white
        
        referAndEarnLbl.text = lang.ReferandEarnBonusRewards
        clickBelowToShareLbl.text = lang.ClickbelowtoshareyouruniqueReferralCode
        byInvitingThemLbl.text = lang.ByinvitingthemtoenjoyanyTCTpackage
        placeMentLbl.text = lang.Placement
        leftLbl.text = lang.Left
        autoLbl.text = lang.Auto
        rightLbl.text = lang.Right
        
        referAndEarnBtn.setTitle(lang.ReferEarnNow, for: .normal)
        shareBtn.setTitle(lang.Share, for: .normal)

        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        
        self.selectedPlacement = ""
        self.unselecBtn(btn1: self.leftImg, btn2: self.rightImg, btn3: self.autoImg)

        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func referAndEarnBtnAction(_ sender: Any) {
        
        
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {

            self.backbtn.isHidden = false
            self.placementView.isHidden = false
        })

        
    }
    
    
    @IBAction func shareact(_ sender: Any) {
        
            
         if selectedPlacement == ""
        {
            self.view.make(toast: "Please Select Referral Placement")

        }
        else
        {
            getrefercodeApicall(place: selectedPlacement)
                // referApiCall(place: selectedPlacement)

        }
        
        
        
    }
    func selecBtn(btn1:UIImageView,btn2:UIImageView,btn3:UIImageView)
    {
        
        let image = UIImage(named: "radio-on-button")
        let image1 = UIImage(named: "empty")


        btn1.image = image
        btn2.image = image1
        btn3.image = image1

    }
    
    
    func unselecBtn(btn1:UIImageView,btn2:UIImageView,btn3:UIImageView)
    {
        
        let image1 = UIImage(named: "empty")


        btn1.image = image1
        btn2.image = image1
        btn3.image = image1

    }

    
    
    @IBAction func copyBtnAction(_ sender: Any) {
        
        UIPasteboard.general.string = referCodeLbl.text
    }
    @IBAction func closeact(_ sender: Any) {
        
        self.selectedPlacement = ""
        self.unselecBtn(btn1: self.leftImg, btn2: self.rightImg, btn3: self.autoImg)

        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {

            self.backbtn.isHidden = true
            self.placementView.isHidden = true
        })

        
    }
    
    @IBAction func leftBtn(_ sender: UIButton) {
        
        selectedPlacement = "left"
        selecBtn(btn1: leftImg, btn2: autoImg, btn3: rightImg)
        
    }
    
    @IBAction func autoBtn(_ sender: UIButton) {
        selectedPlacement = "auto"

        selecBtn(btn1: autoImg, btn2: leftImg, btn3: rightImg)

    }
    
    
    @IBAction func rightBtn(_ sender: UIButton) {
        selectedPlacement = "right"

        selecBtn(btn1: rightImg, btn2: leftImg, btn3: autoImg)

    }
    
    func getrefercodeApicall(place:String)
    {
                    let params1 = ["refer_code":referCodeLbl.text!,
                                   "refer_place":place]
        
        print(params1)
        
                                APIManager.shared.get_refercode(params: params1 as [String : AnyObject]) { (response) in
                                
                                    print("response params1 \(response)")
                                    
                                    
                                    
                                    
                                    self.get_refer_code = response["refer_code"].stringValue
                                    
                                    
                                    if self.get_refer_code != "" || !self.get_refer_code.isEmpty
                                    {
                                        self.referApiCall()

                                    }
                                    else
                                    {
                                        self.getrefercodeApicall(place: self.selectedPlacement)
                                    }

        }
    }
    func referApiCall()
    {
                    let params1 = ["userid":customer_id,
                                   "refer_code":get_refer_code,
                                   "refer_place":selectedPlacement]
        
        print(params1)
        
                                APIManager.shared.Refer_Code(params: params1 as [String : AnyObject]) { (response) in
                                
                                    print("response params1 \(response)")
                                    
                                    
                                    let dict = response["referral"].dictionaryValue
                                    
                                    let msg = dict["message"]?.stringValue
                                    let status = dict["status"]?.stringValue
                                    
                                    
                                    if status == "failure"
                                    {
                                        Common.checkRemoveCdAlert()

                                        let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                        alert.circleFillColor = UIColor.buttonprimary

                                        alert.autoHideTime = 2.0
                                        alert.show()

                                    }
                                    
                                    else
                                    {

                                        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {

                                            self.backbtn.isHidden = true
                                            self.placementView.isHidden = true
                                        })

                                        
                                        //self.selectedPlacement = ""
                                        //self.unselecBtn(btn1: self.leftImg, btn2: self.rightImg, btn3: self.autoImg)

                                            // text to share
                                    let referText = " Hi, This is My TCT(THE CLUB TOKEN) Referral Code \n\(self.get_refer_code)\n https://testflight.apple.com/join/14wxlHHN"

                                            // set up activity view controller
                                            let textToShare = [ referText ]
                                            let activityViewController = UIActivityViewController(activityItems: textToShare as [Any], applicationActivities: nil)
                                            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

                                            // exclude some activity types from the list (optional)
                                    //        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]

                                            // present the view controller
                                            self.present(activityViewController, animated: true, completion: nil)
                                        
                                        
                                        self.get_refer_code = ""
                                        self.selectedPlacement = ""
                                        self.unselecBtn(btn1: self.leftImg, btn2: self.rightImg, btn3: self.autoImg)

                                        
                                    }


        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
  
        self.get_refer_code = ""

        self.selectedPlacement = ""
        self.unselecBtn(btn1: self.leftImg, btn2: self.rightImg, btn3: self.autoImg)

    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

