//
//  MainTabbarViewController.swift
//  TheClubToken
//
//  Created by trioangle on 05/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class MainTabbarViewController: UITabBarController,UITabBarControllerDelegate {

    lazy var lang = Language.default.object
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tabBar.items?[0].title = lang.home
        tabBar.items?[1].title = lang.wallet
        tabBar.items?[3].title = lang.genealogy
        tabBar.items?[4].title = lang.transactions
        
        let islogin = UserDefaults.standard.bool(forKey: "user_isLogin")

        self.delegate = self
        print("islogin \(islogin)")
        UserDefaults.standard.set(true, forKey: "user_isLogin")
        print("islogin \(islogin)")

        print(tabSelectedIndex)
        self.selectedIndex = tabSelectedIndex
        
        if let newButtonImage = UIImage(named: "dice-icon") {
            self.addCenterButton(withImage: newButtonImage, highlightImage: newButtonImage)
        }

//        let prominentTabBar = self.tabBar as! ProminentTabBar
//        prominentTabBar.prominentButtonCallback = prominentTabTaped
        
        // Do any additional setup after loading the view.
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {

        let index = tabBarController.viewControllers?.firstIndex(of: viewController)!
        tabSelectedIndex = index!
        let vc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainTabbarViewController") as? MainTabbarViewController
        self.navigationController?.pushViewController(vc!, animated: false)
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as? AppDelegate)!
        appDelegate.window?.rootViewController = vc
        
        
       }
    
    func prominentTabTaped() {
        selectedIndex = (tabBar.items?.count ?? 0)/2
    }
    @objc func handleTouchTabbarCenter(sender : UIButton)
    {
       if let count = self.tabBar.items?.count
       {
           let i = floor(Double(count / 2))
           self.selectedViewController = self.viewControllers?[Int(i)]
       }
    }
   
    
    func addCenterButton(withImage buttonImage : UIImage, highlightImage: UIImage) {

        let paddingBottom : CGFloat = -5

        let button = UIButton(type: .custom)
        button.autoresizingMask = [.flexibleRightMargin, .flexibleTopMargin, .flexibleLeftMargin, .flexibleBottomMargin]
       // button.frame = CGRect(x: 0.0, y: 0.0, width: buttonImage.size.width / 2.0, height: buttonImage.size.height / 2.0)
        button.frame = CGRect(x: 0.0, y: 0.0, width: 60, height: 60)
        button.setBackgroundImage(buttonImage, for: .normal)
        button.setBackgroundImage(highlightImage, for: .highlighted)

        let rectBoundTabbar = self.tabBar.bounds
        let xx = rectBoundTabbar.midX
        let yy = rectBoundTabbar.midY - paddingBottom
        button.center = CGPoint(x: xx, y: yy)

        self.tabBar.addSubview(button)
        self.tabBar.bringSubviewToFront(button)

        button.addTarget(self, action: #selector(handleTouchTabbarCenter), for: .touchUpInside)

        if let count = self.tabBar.items?.count
        {
            let i = floor(Double(count / 2))
            let item = self.tabBar.items![Int(i)]
            item.title = ""
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class ProminentTabBar: UITabBar {
    var prominentButtonCallback: (()->())?

    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        guard let items = items, items.count>0 else {
            return super.hitTest(point, with: event)
        }

        let middleItem = items[items.count/2]
        let middleExtra = middleItem.imageInsets.top
        let middleWidth = bounds.width/CGFloat(items.count)
        let middleRect = CGRect(x: (bounds.width-middleWidth)/2, y: middleExtra, width: middleWidth, height: abs(middleExtra))
        if middleRect.contains(point) {
            prominentButtonCallback?()
            return nil
        }
        return super.hitTest(point, with: event)
    }
}
