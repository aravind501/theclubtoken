//
//  EmailVerificationViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 26/04/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CDAlertView
class EmailVerificationViewController: UIViewController {

    @IBOutlet weak var bottomview: UIView!
    @IBOutlet weak var emailTxtffld: UITextField!
    @IBOutlet weak var verificationTxtfld: UITextField!
    
    @IBOutlet weak var verifyBtn: UIButton!
    @IBOutlet weak var sendVerificationCodeBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    lazy var lang = Language.default.object

    var verificationCode = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        bottomview.clipsToBounds = true
        bottomview.layer.cornerRadius = 50

        if #available(iOS 11.0, *) {
            bottomview.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        titleLbl.text = lang.PleaseEnterEmailid
        sendVerificationCodeBtn.setTitle(lang.SendVerificationcode, for: .normal)
        verifyBtn.setTitle(lang.Verify, for: .normal)
        
        setDesigns()

        
        emailTxtffld.placeholder = lang.Enter_Email_Id
        verificationTxtfld.placeholder = lang.EnterVerificationCode
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        
        if #available(iOS 11.0, *){
            
        }else{
           let rectShape = CAShapeLayer()
           rectShape.bounds = view.frame
           rectShape.position = view.center
           rectShape.path = UIBezierPath(roundedRect: view.bounds,    byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 50 , height: 50)).cgPath
           bottomview.layer.mask = rectShape
    }
    
    }
    
    override func viewWillDisappear(_ animated: Bool) {
      
        emailTxtffld.text = ""
        verificationCode = ""
        verificationTxtfld.text = ""
    }
    func setDesigns(){
        
        
//        self.emailTxtffld.borderActiveColor = .clear
//        self.emailTxtffld.borderInactiveColor = .clear
//        self.emailTxtffld.placeholderColor = .textcolor
//        self.emailTxtffld.textColor = .textcolor
//        self.emailTxtffld.delegate = self
//
//        self.verificationTxtfld.borderActiveColor = .clear
//        self.verificationTxtfld.borderInactiveColor = .clear
//        self.verificationTxtfld.placeholderColor = .textcolor
//        self.verificationTxtfld.textColor = .textcolor
//        self.verificationTxtfld.delegate = self

       
    }
   
    
    @IBAction func backact(_ sender: Any) {
        
        self.popLeft()
    }
    
    @IBAction func sendverificationcodeact(_ sender: Any) {
        
        
        if emailTxtffld.text == ""
        {
            self.view.make(toast: lang.Please_Enter_Email_Id)
        }
        
        else if emailTxtffld.text?.isEmail == false
        {
            self.view.make(toast: lang.Please_Enter_Valid_Email_Id)

        }
        
        else
        {
                         let params1 = ["userid":customer_id,
                                        "email":emailTxtffld.text!]
                         
                                     print(params1)
                                     APIManager.shared.email_otp_signup(params: params1 as [String : AnyObject]) { (response) in
                                         print("response params1 \(response)")
                                         print("CHeckparams")
                                         

                                             let msg = response["message"].stringValue
                                             let status = response["status"].stringValue

                                                 
                                                 if status == "success" {
                                                     
                                                     self.verificationCode = response["otpcode"].stringValue
                                                     Common.checkRemoveCdAlert()
                                                            let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                                    alert.circleFillColor = UIColor.buttonprimary
                                                    let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                            alert.add(action: doneAction)
                                                            alert.show() { (alert) in

                                                            }

                                                    
                                                 }
                                                     
                                                 else {
                                                     self.verificationCode = ""
                                                     let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                                     alert.circleFillColor = UIColor.buttonprimary

                                                     alert.autoHideTime = 2.0
                                                     alert.show()

                                                     
                                         }
                                                 
             }
        }
    }
   
    @IBAction func verifycodeact(_ sender: Any) {
        
        
        if verificationTxtfld.text == ""
        {
            self.view.make(toast: lang.Please_Enter_Verification_Code)
        }
        
        else if verificationTxtfld.text != verificationCode
        {
            self.view.make(toast: lang.Verification_Code_Mismatch)

        }
        
        else
        {
                        let params1 = ["userid":customer_id,
                                       "email":emailTxtffld.text!,
                                       "verifycode":verificationTxtfld.text!]
                        
                                    print(params1)
            
                                    APIManager.shared.verify_email(params: params1 as [String : AnyObject]) { (response) in
                                        print("response params1 \(response)")
                                        print("CHeckparams")
                                        

                                            let msg = response["message"].stringValue
                                            let status = response["status"].stringValue

                                                
                                                if status == "success" {
                                                    
                                UserDefaults.standard.set(customer_id, forKey: "user_id")

                                                    Common.checkRemoveCdAlert()
                                                           let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                                   alert.circleFillColor = UIColor.buttonprimary
                                                   let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                           alert.add(action: doneAction)
                                                           alert.show() { (alert) in

                                                            pinfrom = "register"
                                                            self.push(id: "PINCodeSetViewController", animation: true, fromSB: Login)

                                                           }

                                                   
                                                }
                                                    
                                                else {
                                                    
                                                    let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                                    alert.circleFillColor = UIColor.buttonprimary

                                                    alert.autoHideTime = 2.0
                                                    alert.show()

                                                    
                                        }
                                                
            }
        }
        

        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//extension EmailVerificationViewController : UITextFieldDelegate {
//
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//
//        return textField.resignFirstResponder()
//    }
//
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//
//        if textField == emailTxtffld
//        {
//        emailTxtffld.placeholder = "Enter Email Id"
//        }
//
//        else if textField == verificationTxtfld
//        {
//        verificationTxtfld.placeholder = "Enter Verification Code"
//        }
//
//    }
//
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField.text?.count == 0 {
//
//
//            if textField == emailTxtffld
//                   {
//                   emailTxtffld.placeholder = "Enter Email Id"
//                    self.emailTxtffld.borderInactiveColor = .clear
//
//                   }
//
//                   else if textField == verificationTxtfld
//                   {
//                   verificationTxtfld.placeholder = "Enter Verification Code"
//                    self.verificationTxtfld.borderInactiveColor = .clear
//
//                   }
//
//
//        }
//    }
//
//}
