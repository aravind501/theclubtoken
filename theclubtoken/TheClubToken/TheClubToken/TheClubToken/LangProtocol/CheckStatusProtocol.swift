//
//  CheckStatusProtocol.swift
//  Igniter
//
//  Created by trioangle on 03/12/19.
//  Copyright © 2019 Anonymous. All rights reserved.
//

import Foundation
//MARK: Protocols
/**
 protocol to update some data on action
 - Author: Abishek Robin
 */
protocol CheckStatusProtocol {
    func checkStatus()
}
