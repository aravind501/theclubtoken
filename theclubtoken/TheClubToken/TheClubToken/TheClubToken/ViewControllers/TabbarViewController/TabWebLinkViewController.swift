//
//  TabWebLinkViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 13/07/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import SwiftyJSON

class TabWebLinkViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var firstImgView: UIImageView!
    @IBOutlet weak var secongImgView: UIImageView!
    @IBOutlet weak var thirdImgView: UIImageView!
    
    
    @IBOutlet weak var ImgTblView: UITableView!
    var firstDict = JSON()
    var secondDict = JSON()
    var thirdDict = JSON()
    var packagesDatas = [JSON]()

    @IBOutlet weak var norecordLbl: UILabel!
    override func viewDidLoad() {
      
        super.viewDidLoad()

        getDigital_chips_websiteslist()
        
        ImgTblView.delegate = self
        ImgTblView.dataSource = self
        
        ImgTblView.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }
    
    
            func getDigital_chips_websiteslist()
            {
                
                let params1 = [":":":"]
                
                print(params1)
                
                                        APIManager.shared.digital_chips_websites() { (response) in
                                            print("response getcurrencylist \(response)")
                                            
                                         //   self.currencyDatas = response["currency_list"].arrayValue
                                            
                                            
                                            
                                            let status = response["status"].stringValue
                                            let msg = response["msg"].stringValue
                                           
                                            
                                            if status == "success"
                                            
                                            {
                                                
                                                self.ImgTblView.isHidden = false
                                                self.norecordLbl.isHidden = true
                                                
                                            let dict = response["games"].arrayValue
                                            
                                                self.packagesDatas = dict
                                                
                                                self.ImgTblView.reloadData()
                                            }
                                            
                                            else
                                            {
                                                self.ImgTblView.isHidden = true
                                                self.norecordLbl.isHidden = false
                                                
                                                
                                            }
                                                
                                         /*   if dict.count > 0
                                            {
                                                 self.firstDict = dict[0]
                                                 self.secondDict = dict[1]
                                                 self.thirdDict = dict[2]
                                                
                                                
                                                
                                                self.firstImgView.sd_setImage(with: URL(string: self.firstDict["game_image"].stringValue), completed: nil)
                                                self.secongImgView.sd_setImage(with: URL(string: self.secondDict["game_image"].stringValue), completed: nil)
                                                self.thirdImgView.sd_setImage(with: URL(string: self.thirdDict["game_image"].stringValue), completed: nil)
                                                
    //                                        for i in 0...dict.count - 1
    //                                        {
    //                                            let dicts = dict[i].dictionaryValue
    //                                            let name = dicts["url"]?.stringValue
    //                                            self.dataSourceArray.append(name!)
    //
    //                                        }
                                            }
                                                
                                            }
                                            
                                            
                                            else
                                            
                                            {
                                                
                                            }*/



                
            }
                
    }
    
    
    @IBAction func allbets(_ sender: Any) {
        
        
        
        let packageUpgradeVc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabWebParticularViewController") as? TabWebParticularViewController
        packageUpgradeVc?.urlvalue = self.firstDict["game_url"].stringValue
        packageUpgradeVc?.titleValue = self.firstDict["game_name"].stringValue
        self.navigationController?.pushViewController(packageUpgradeVc!, animated: true)

        
      /*  if #available(iOS 10.0, *) {
            guard let url = URL(string: "http://www.a11bets.com/") else { return }
            UIApplication.shared.open(url)
            
        } else {
            
            guard let url = URL(string: "http://www.a11bets.com/") else { return }
            UIApplication.shared.openURL(url)
            
        }*/

    }
    
    @IBAction func allcasino(_ sender: Any) {
        
       
        let packageUpgradeVc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabWebParticularViewController") as? TabWebParticularViewController
        packageUpgradeVc?.urlvalue = self.secondDict["game_url"].stringValue
        packageUpgradeVc?.titleValue = self.secondDict["game_name"].stringValue

        self.navigationController?.pushViewController(packageUpgradeVc!, animated: true)

        
        
    /*    if #available(iOS 10.0, *) {
            guard let url = URL(string: "http://www.a11casino.com/") else { return }
            UIApplication.shared.open(url)
            
        } else {
            
            guard let url = URL(string: "http://www.a11casino.com/") else { return }
            UIApplication.shared.openURL(url)
            
        }*/

    }
    @IBAction func allgame(_ sender: Any) {
        
        let packageUpgradeVc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabWebParticularViewController") as? TabWebParticularViewController
        packageUpgradeVc?.urlvalue = self.thirdDict["game_url"].stringValue
        packageUpgradeVc?.titleValue = self.thirdDict["game_name"].stringValue

        self.navigationController?.pushViewController(packageUpgradeVc!, animated: true)

        
       /* if #available(iOS 10.0, *) {
            guard let url = URL(string: "http://www.a11game.com/") else { return }
            UIApplication.shared.open(url)
            
        } else {
            
            guard let url = URL(string: "http://www.a11game.com/") else { return }
            UIApplication.shared.openURL(url)
            
        }*/

    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! imageCell
        

        let dict = packagesDatas[indexPath.row].dictionaryValue
        
        cell.linkBtn.isHidden = true
        if let id = dict["game_image"]
        {
        let url = dict["game_image"]?.stringValue
        
        
        cell.TctImg.sd_setImage(with: URL(string: url!), completed: nil)
            
        }
        
         return cell
     }
     
     
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return packagesDatas.count

     }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
            let dict = packagesDatas[indexPath.row].dictionaryValue
        
        
        if let id = dict["game_url"]
        
        {
            let url = id.stringValue
            
            let packageUpgradeVc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabWebParticularViewController") as? TabWebParticularViewController
            packageUpgradeVc?.urlvalue = url
            
            var tit = "The Club Token"
            
            if let id = dict["game_name"]
            {
                tit = id.stringValue
            }
            packageUpgradeVc?.titleValue = tit

            self.navigationController?.pushViewController(packageUpgradeVc!, animated: true)

        }
        
        
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class imageCell : UITableViewCell
{
    
    @IBOutlet weak var linkBtn: UIButton!
    @IBOutlet weak var TctImg: UIImageView!
}
