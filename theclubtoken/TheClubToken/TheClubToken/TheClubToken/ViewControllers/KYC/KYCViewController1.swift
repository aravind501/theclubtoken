//
//  KYCViewController1.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 03/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import DropDown
import CDAlertView
class KYCViewController1: UIViewController {

    @IBOutlet weak var firstnameTxtFld: HoshiTextField!
    @IBOutlet weak var middlenameTxtFld: HoshiTextField!
    @IBOutlet weak var lastnameTxtFld: HoshiTextField!
    @IBOutlet weak var genderTxtFld: HoshiTextField!
    @IBOutlet weak var dobTxtFld: HoshiTextField!
    @IBOutlet weak var emailTxtFld: HoshiTextField!
    @IBOutlet weak var phonenumberTxtFld: HoshiTextField!
    @IBOutlet weak var yourintensionTxtFld: HoshiTextField!
    @IBOutlet weak var downarrow: UIImageView!
    
    @IBOutlet weak var nextLbl: UILabel!
    @IBOutlet weak var previousLbl: UILabel!
    let dropDown = DropDown()
    lazy var lang = Language.default.object

    let genderArray = ["Male","Female","Others"]
    override func viewDidLoad() {
        super.viewDidLoad()

        setDesigns()
        viewprofile()
        
        previousLbl.text = lang.Previous
        nextLbl.text = lang.Next
        
        firstnameTxtFld.placeholder = lang.FirstName
        middlenameTxtFld.placeholder = lang.MiddleName
        lastnameTxtFld.placeholder = lang.LastName
        genderTxtFld.placeholder = lang.Gender
        dobTxtFld.placeholder = lang.DateofBirth
        emailTxtFld.placeholder = lang.Email
        phonenumberTxtFld.placeholder = lang.PhoneNumber
        yourintensionTxtFld.placeholder = lang.YourIntentionforPurchase
        
        // Do any additional setup after loading the view.
    }
    
    
    func viewprofile()
    {
                    let params1 = ["userid":customer_id]
                               
        
        print(params1)
        
                                APIManager.shared.view_profile(params: params1 as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    

                                    self.firstnameTxtFld.text = response["firstname"].stringValue
                                  self.middlenameTxtFld.text = response["middlename"].stringValue
                                    self.lastnameTxtFld.text = response["lastname"].stringValue
                                    self.phonenumberTxtFld.text = response["phone"].stringValue

                                    self.emailTxtFld.text = response["email"].stringValue

                                    self.dobTxtFld.text = response["dob"].stringValue
                                    self.genderTxtFld.text = response["gender"].stringValue
                                    self.yourintensionTxtFld.text = response["intention"].stringValue
                                
                                    

        }
    }
    
    func setDesigns(){
        
        self.firstnameTxtFld.borderActiveColor = .textcolor
        self.firstnameTxtFld.borderInactiveColor = .buttonsecondary
        self.firstnameTxtFld.placeholderColor = .textcolor
        self.firstnameTxtFld.textColor = .textcolor
        self.firstnameTxtFld.delegate = self
        
        self.middlenameTxtFld.borderActiveColor = .textcolor
        self.middlenameTxtFld.borderInactiveColor = .buttonsecondary
        self.middlenameTxtFld.placeholderColor = .textcolor
        self.middlenameTxtFld.textColor = .textcolor
        self.middlenameTxtFld.delegate = self
       
        self.lastnameTxtFld.borderActiveColor = .textcolor
        self.lastnameTxtFld.borderInactiveColor = .buttonsecondary
        self.lastnameTxtFld.placeholderColor = .textcolor
        self.lastnameTxtFld.textColor = .textcolor
        self.lastnameTxtFld.delegate = self
        
        self.genderTxtFld.borderActiveColor = .textcolor
        self.genderTxtFld.borderInactiveColor = .buttonsecondary
        self.genderTxtFld.placeholderColor = .textcolor
        self.genderTxtFld.textColor = .textcolor
        self.genderTxtFld.delegate = self

        self.dobTxtFld.borderActiveColor = .textcolor
        self.dobTxtFld.borderInactiveColor = .buttonsecondary
        self.dobTxtFld.placeholderColor = .textcolor
        self.dobTxtFld.textColor = .textcolor
        self.dobTxtFld.delegate = self
        
        self.emailTxtFld.borderActiveColor = .textcolor
        self.emailTxtFld.borderInactiveColor = .buttonsecondary
        self.emailTxtFld.placeholderColor = .textcolor
        self.emailTxtFld.textColor = .textcolor
        self.emailTxtFld.delegate = self
        
        self.phonenumberTxtFld.borderActiveColor = .textcolor
        self.phonenumberTxtFld.borderInactiveColor = .buttonsecondary
        self.phonenumberTxtFld.placeholderColor = .textcolor
        self.phonenumberTxtFld.textColor = .textcolor
        self.phonenumberTxtFld.delegate = self
        
        self.yourintensionTxtFld.borderActiveColor = .textcolor
        self.yourintensionTxtFld.borderInactiveColor = .buttonsecondary
        self.yourintensionTxtFld.placeholderColor = .textcolor
        self.yourintensionTxtFld.textColor = .textcolor
        self.yourintensionTxtFld.delegate = self
        downarrow.image = downarrow.image?.withRenderingMode(.alwaysTemplate)
        downarrow.tintColor = UIColor.buttonsecondary

        self.dobTxtFld.setInputViewDatePicker(target: self, selector: #selector(tapDone)) //1

    }
    
    
    @objc func tapDone() {
      
        
        if let datePicker = self.dobTxtFld.inputView as? UIDatePicker { // 2-1
            
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .medium // 2-3
            self.dobTxtFld.text = dateformatter.string(from: datePicker.date) //2-4

            let dateformatter1 = DateFormatter() // 2-2
            dateformatter1.dateFormat = "dd-MMM-yyy" // 2-3
            dobTxtFld.text = dateformatter1.string(from: datePicker.date)


        
           // formatter.dateFormat = "yyyy-MM-dd"

        }
        self.dobTxtFld.resignFirstResponder() // 2-5
    }
    
    @IBAction func backact(_ sender: Any) {
        
        //self.popLeft()
        
        let mainViewControllerVC = self.navigationController?.viewControllers.first(where: { (viewcontroller) -> Bool in
            return viewcontroller is UserProfileListViewController
        })
        if let mainViewControllerVC = mainViewControllerVC {
            navigationController?.popToViewController(mainViewControllerVC, animated: true)
        }
    }
    
    @IBAction func genderact(_ sender: Any) {
        
        self.dropDown.dataSource = self.genderArray
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
           // self.carCategoryBtn.titleLabel?.text = item
            
            self.genderTxtFld.text = item
            self.dropDown.hide()
        }
        
        dropDown.anchorView = dobTxtFld
        dropDown.show()

        
    }
    @IBAction func dobact(_ sender: Any) {
    }
    @IBAction func previousact(_ sender: Any) {
        
        //self.popLeft()
        let mainViewControllerVC = self.navigationController?.viewControllers.first(where: { (viewcontroller) -> Bool in
            return viewcontroller is UserProfileListViewController
        })
        if let mainViewControllerVC = mainViewControllerVC {
            navigationController?.popToViewController(mainViewControllerVC, animated: true)
        }

    }
    
    @IBAction func nextact(_ sender: Any) {
        

        if firstnameTxtFld.text == ""
        {
            self.view.make(toast: lang.Please_Enter_First_Name)
        }
        
        else if lastnameTxtFld.text == ""
        {
            self.view.make(toast: lang.Please_Enter_Last_Name)
        }
        else if genderTxtFld.text == ""
        {
            self.view.make(toast: lang.PleaseEnterGender)

        }
        
        else if dobTxtFld.text == ""
        {
            self.view.make(toast: lang.PleaseEnterDateofBirth)

        }

        else if emailTxtFld.text == ""
        {
            self.view.make(toast: lang.PleaseEnterEmailid)

        }

        else if phonenumberTxtFld.text == ""
        {
            self.view.make(toast: lang.Please_Enter_Phone_Number)

        }
        
            
            
            
        else
        {
            
            var mname = ""
            var inten = ""
            
            if middlenameTxtFld.text != ""
            {
                mname = middlenameTxtFld.text!
            }
            
            if yourintensionTxtFld.text != ""
            {
                inten = yourintensionTxtFld.text!
            }

            let params1 = ["userid":customer_id,
                           "firstname":firstnameTxtFld.text!,
                           "middlename":mname,
                           "lastname":lastnameTxtFld.text!,
                "phone":phonenumberTxtFld.text!,
                "email":emailTxtFld.text!,
                "gender":genderTxtFld.text!,
                "dob":dobTxtFld.text!,
                "intention":inten]
                                   
            
            print(params1)
            
                                    APIManager.shared.kyc_step1(params: params1 as [String : AnyObject]) { (response) in
                                        print("response params1 \(response)")
                                        print("CHeckparams")
                                        
                                        let status = response["status"].stringValue
                                        let msg = response["msg"].stringValue
                                        
                                        if status == "success"
                                        {
                                             Common.checkRemoveCdAlert()
                                                    let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                            alert.circleFillColor = UIColor.buttonprimary
                                            let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                    alert.add(action: doneAction)
                                                    alert.show() { (alert) in

                                                        self.push(id: "KYCViewController2", animation: false, fromSB: Profile)

                                                    }
                                        }
                                        
                                        else
                                        {
                                            Common.checkRemoveCdAlert()

                                            let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                            alert.circleFillColor = UIColor.buttonprimary

                                            alert.autoHideTime = 2.0
                                            alert.show()

                                        }

            }
        }

        
        
    }
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension KYCViewController1 : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == firstnameTxtFld
        {
            firstnameTxtFld.placeholder = lang.FirstName
        }

        else if textField == middlenameTxtFld
        {
        middlenameTxtFld.placeholder = lang.MiddleName
        }
        else if textField == lastnameTxtFld
        {
        lastnameTxtFld.placeholder = lang.LastName
        }
        else if textField == genderTxtFld
        {
        genderTxtFld.placeholder = lang.Gender
        }
        else if textField == dobTxtFld
        {
        dobTxtFld.placeholder = lang.DateofBirth
        }

        else if textField == emailTxtFld
        {
        emailTxtFld.placeholder = lang.Email
        }

        else if textField == phonenumberTxtFld
        {
        phonenumberTxtFld.placeholder = lang.PhoneNumber
        }

        else if textField == yourintensionTxtFld
        {
        yourintensionTxtFld.placeholder = lang.YourIntentionforPurchase
        }

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        if textField.text?.count == 0 {
            
            
            
            
            if textField == firstnameTxtFld
            {
            firstnameTxtFld.placeholder = lang.FirstName
                self.firstnameTxtFld.borderInactiveColor = .buttonsecondary

                
            }
            
            else if textField == middlenameTxtFld
            {
            middlenameTxtFld.placeholder = lang.MiddleName
                self.middlenameTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == lastnameTxtFld
            {
            lastnameTxtFld.placeholder = lang.LastName
                self.lastnameTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == genderTxtFld
            {
            genderTxtFld.placeholder = lang.Gender
                self.genderTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == dobTxtFld
            {
            dobTxtFld.placeholder = lang.DateofBirth
                self.dobTxtFld.borderInactiveColor = .buttonsecondary

            }
            
            else if textField == emailTxtFld
            {
            emailTxtFld.placeholder = lang.Email
                self.emailTxtFld.borderInactiveColor = .buttonsecondary

            }

            else if textField == phonenumberTxtFld
            {
            phonenumberTxtFld.placeholder = lang.PhoneNumber
                
                self.phonenumberTxtFld.borderInactiveColor = .buttonsecondary

            }

            else if textField == yourintensionTxtFld
            {
            yourintensionTxtFld.placeholder = lang.YourIntentionforPurchase
                self.yourintensionTxtFld.borderInactiveColor = .buttonsecondary

            }
            
        }
    }
    
}
extension UITextField {
    
    func setInputViewDatePicker(target: Any, selector: Selector) {
        // Create a UIDatePicker object and assign to inputView
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        self.inputView = datePicker //3
        
        // Create a toolbar and assign it to inputAccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        toolBar.tintColor = UIColor.buttonprimary
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector) //7
        toolBar.setItems([cancel, flexible, barButton], animated: false) //8
        self.inputAccessoryView = toolBar //9
    }
    
    @objc func tapCancel() {
        self.resignFirstResponder()
    }
    
}
