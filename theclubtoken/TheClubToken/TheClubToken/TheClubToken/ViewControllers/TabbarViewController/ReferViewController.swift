//
//  ReferViewController.swift
//  TheClubToken
//
//  Created by trioangle on 26/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class ReferViewController: UIViewController {

    @IBOutlet weak var referCodeLbl: UILabel!
    @IBOutlet weak var referAndEarnBtn: UIButton!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var copyBtn: UIButton!
    @IBOutlet weak var baseView: UIView!
    
    var referalCode = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.referCodeLbl.text = self.referalCode
        
        self.closeBtn.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysTemplate),
                                      for: .normal)
        self.closeBtn.tintColor = UIColor.white
        
        self.copyBtn.setImage(UIImage(named: "copy")?.withRenderingMode(.alwaysTemplate),
                                      for: .normal)
        self.copyBtn.tintColor = UIColor.white
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func referAndEarnBtnAction(_ sender: Any) {
        
        // text to share
        let referText = referCodeLbl.text

        // set up activity view controller
        let textToShare = [ referText ]
        let activityViewController = UIActivityViewController(activityItems: textToShare as [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

        // exclude some activity types from the list (optional)
//        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]

        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func copyBtnAction(_ sender: Any) {
        
        UIPasteboard.general.string = referCodeLbl.text
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
