
//
//  TransferSubAccountViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 27/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CDAlertView
import DropDown
class TransferSubAccountViewController: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var usernameTxtFld: HoshiTextField!
    @IBOutlet weak var transfertoTxtFld: HoshiTextField!
    @IBOutlet weak var emailTxtFld: HoshiTextField!
    lazy var lang = Language.default.object
    @IBOutlet weak var transferBtn: UIButton!
    var freeaccount = true
    
    var subaccountlist = [String]()
    var transfertolist = ["Email","Mobile"]
    
    let dropDown = DropDown()

    let dropDown1 = DropDown()

    @IBOutlet weak var usrnameview: UIView!
    
    @IBOutlet weak var accountlistview: UIView!
    
    var selectedItem = "Email"
    override func viewDidLoad() {
        super.viewDidLoad()
getDesign()
        getsubaccountlist()
        emailTxtFld.keyboardType = .default
        
        titleLbl.text = lang.TransferSuAccount
        usernameTxtFld.placeholder = lang.UserName
        transfertoTxtFld.placeholder = lang.TransferTo
        emailTxtFld.placeholder = lang.Email
        
        transferBtn.setTitle(lang.Transfer, for: .normal)
        
        // Do any additional setup after loading the view.
    }
    
    
    
    func getsubaccountlist()
    {
        
        let params1 = ["userid":customer_id]
        
        print(params1)
        
                                APIManager.shared.subaccount_list(params: params1 as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                                    let types = response["sub accounts"].arrayValue

                                    for i in 0...types.count - 1
                                    {
                                        let dict = types[i].dictionaryValue
                                        
                                        if let id = dict["sub_account"]
                                        {
                                        let name = dict["sub_account"]?.stringValue
                                        
                                        
                                        self.subaccountlist.append(name!)
                                        }
                                        else
                                        {
                                            let msg = dict["message"]?.stringValue
                                         
                                            let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                            alert.circleFillColor = UIColor.buttonprimary

                                            alert.autoHideTime = 2.0
                                            alert.show()

                                        }
                                    }
        }
    }
    func getDesign()
    {
        self.usernameTxtFld.borderActiveColor = .textcolor
        self.usernameTxtFld.borderInactiveColor = .buttonsecondary
        self.usernameTxtFld.placeholderColor = .textcolor
        self.usernameTxtFld.textColor = .textcolor
        self.usernameTxtFld.delegate = self

        self.transfertoTxtFld.borderActiveColor = .textcolor
        self.transfertoTxtFld.borderInactiveColor = .buttonsecondary
        self.transfertoTxtFld.placeholderColor = .textcolor
        self.transfertoTxtFld.textColor = .textcolor
        self.transfertoTxtFld.delegate = self
       
        
        self.emailTxtFld.borderActiveColor = .textcolor
        self.emailTxtFld.borderInactiveColor = .buttonsecondary
        self.emailTxtFld.placeholderColor = .textcolor
        self.emailTxtFld.textColor = .textcolor
        self.emailTxtFld.delegate = self

        
    }


    @IBAction func backact(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func usernameact(_ sender: Any) {
        
        
        if subaccountlist.count > 0
        {
            
            self.dropDown.dataSource = self.subaccountlist
            self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                
                self.usernameTxtFld.text = item

                self.dropDown.hide()

            }
            
            dropDown.anchorView = usrnameview
            dropDown.borderColor = UIColor.black
            dropDown.backgroundColor = UIColor.white
            dropDown.borderLineWidth = 1.0
            
            dropDown.show()

            

            
        }
        else
        {
            
            let alert = CDAlertView(title: AppName, message: "No subaccount linked to your account", type: .error)
            alert.circleFillColor = UIColor.buttonprimary

            alert.autoHideTime = 2.0
            alert.show()

        }
    }
    
   
    @IBAction func transfertoAct(_ sender: Any) {
        
        self.dropDown1.dataSource = self.transfertolist
        self.dropDown1.selectionAction = { [unowned self] (index: Int, item: String) in
            
            self.transfertoTxtFld.text = item
            
            self.selectedItem = item
            
            if self.selectedItem == "Email"
            
            {
                self.emailTxtFld.placeholder = self.lang.Email

            }
            else
            {
                self.emailTxtFld.placeholder = self.lang.Mobile

            }

            self.dropDown.hide()

        }
        
        dropDown1.anchorView = accountlistview
        dropDown1.borderColor = UIColor.black
        dropDown1.backgroundColor = UIColor.white
        dropDown1.borderLineWidth = 1.0
        
        dropDown1.show()

    }
    
    
    @IBAction func trasnsferAct(_ sender: Any) {
        
        if usernameTxtFld.text == ""
        {
            self.view.make(toast: lang.Please_Select_User_Name)

        }

        else if transfertoTxtFld.text == ""
        {
            self.view.make(toast: lang.Please_Select_Transfer_Account)

        }
        
            else if emailTxtFld.text == ""
            {
                if selectedItem == "Email"
                {
                    self.view.make(toast: lang.Please_Enter_Email_Id)
                }
                else
                {
                    self.view.make(toast: lang.PleaseEntermobile)

                }

            }
            
            
            
            else if selectedItem == "Email"
        {
            if emailTxtFld.text?.isEmail  == false
                   
                {
                    self.view.make(toast: lang.Please_Enter_Valid_Email_Id)

                    }
            
            else
            {
                transferaccount()
            }

        }
          
            else if selectedItem == "Mobile"
        {
            
            if emailTxtFld.text!.count < 6
            {
                self.view.make(toast: lang.Please_Enter_Valid_Mobile_Number)

            }
            else
            {
                transferaccount()

            }
        }
            
            
            

    }
    

    
    func transferaccount()
    {
        var param = [String:String]()
        var from = ""
       
        if transfertoTxtFld.text == "Email"
        {
            from = "Email"
            let params1 = ["userid":customer_id,
                           "sub_account":usernameTxtFld.text!,
                           "email":emailTxtFld.text!]
            param = params1

        }
        else
        {
            from = "Mobile"
            let params2 = ["userid":customer_id,
                           "sub_account":usernameTxtFld.text!,
                           "phone":emailTxtFld.text!]
            
            param = params2


        }
        
        

     
        
        print(param)
        
        APIManager.shared.transfer_subaccount_email(params: param as [String : AnyObject], from: from) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                                    let status = response["status"].stringValue
            
            
            
                                    let msg = response["message"].stringValue
                                    
                                    if status == "success"
                                    {
                                         Common.checkRemoveCdAlert()
                                                let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                        alert.circleFillColor = UIColor.buttonprimary
                                        let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                alert.add(action: doneAction)
                                                alert.show() { (alert) in


                                                }
                                    }
                                    
                                    else
                                    {
                                        let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                        alert.circleFillColor = UIColor.buttonprimary

                                        alert.autoHideTime = 2.0
                                        alert.show()

                                    }

        }
        
        
        
    }

    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension TransferSubAccountViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == usernameTxtFld
        {
            usernameTxtFld.placeholder = lang.UserName
        }
        
        else if textField == transfertoTxtFld
        {
            transfertoTxtFld.placeholder = lang.TransferSuAccount
        }
        else if textField == emailTxtFld
        {
            if selectedItem == "Email"
            {
       
                emailTxtFld.placeholder = lang.Email
                emailTxtFld.keyboardType = .default
            }
            else
            {
                emailTxtFld.placeholder = lang.Mobile
                emailTxtFld.keyboardType = .phonePad


            }
        }

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        if textField.text?.count == 0 {
            
            
            
            
            if textField == usernameTxtFld
            {
                usernameTxtFld.placeholder = lang.UserName
                self.usernameTxtFld.borderInactiveColor = .buttonsecondary

                
            }
            
            else if textField == transfertoTxtFld
            {
                transfertoTxtFld.placeholder = lang.TransferTo
                self.transfertoTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == emailTxtFld
            {
                
                    if selectedItem == "Email"
                    {
                        emailTxtFld.placeholder = lang.Email
                    }
                    else
                    {
                        emailTxtFld.placeholder = lang.Mobile

                    }

                self.emailTxtFld.borderInactiveColor = .buttonsecondary

            }
            
        }
    }
    
}

