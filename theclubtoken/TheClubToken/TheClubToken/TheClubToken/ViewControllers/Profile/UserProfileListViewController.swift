//
//  UserProfileListViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 09/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CDAlertView
import SDWebImage
import SVProgressHUD
class UserProfileListViewController: UIViewController {

    @IBOutlet weak var headerTitleLbl: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var memberLbl: UILabel!
    @IBOutlet weak var packageLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var editProfiletitleLbl: UILabel!
    @IBOutlet weak var memberShipTitleLbl: UILabel!
    @IBOutlet weak var registerNewSubTitleLbl: UILabel!
    @IBOutlet weak var transferSubAcTitleLbl: UILabel!
    @IBOutlet weak var viewCertificateTitleLbl: UILabel!
    @IBOutlet weak var submitKycTitleLbl: UILabel!
    @IBOutlet weak var logOutBtn: UIButton!
    var image_pic = false
    lazy var lang = Language.default.object

    override func viewDidLoad() {
        super.viewDidLoad()
viewprofile()
        
        headerTitleLbl.text = lang.Profile
        editProfiletitleLbl.text = lang.EditProfile
        memberShipTitleLbl.text = lang.Membership
        registerNewSubTitleLbl.text = lang.RegisterNewSuAccount
        transferSubAcTitleLbl.text = lang.TransferSuAccount
        viewCertificateTitleLbl.text = lang.ViewCertificate
        submitKycTitleLbl.text = lang.SubmitKYC
        
        logOutBtn.setTitle(lang.Logout, for: .normal)

        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backbtn(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
//        self.navigationController?.popToViewController(MainTabbarViewController, animated: true)
//        let vc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainTabbarViewController") as? MainTabbarViewController
//        self.navigationController?.pushViewController(vc!, animated: false)
      

        

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
    }
   
    
    func viewprofile()
    {
                    let params1 = ["userid":customer_id]
                               
        
        print(params1)
        
                                APIManager.shared.view_profile(params: params1 as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                                    let img = response["profile_picture"].stringValue
                                    self.userImg.sd_setImage(with: URL(string: img)) { (image, error, cache, urls) in
                                        if (error != nil) {
                                            self.userImg.image = UIImage(named: "user_icon")
                                        } else {
                                            self.userImg.image = image
                                            self.roundImage()
                                        }
                                    }
                                    
                                    let name = response["firstname"].stringValue

                                    UserDefaults.standard.set(name, forKey: "username")
                                    
                                    self.usernameLbl.text = response["firstname"].stringValue + " " + response["middlename"].stringValue + " " + response["lastname"].stringValue
                                    
                                    if self.usernameLbl.text == "" || self.usernameLbl.text == " " || self.usernameLbl.text == "  "
                                    {
                                        let name = response["username"].stringValue

                                        self.usernameLbl.text = response["username"].stringValue
                                        UserDefaults.standard.set(name, forKey: "username")

                                    }
                                    self.packageLbl.text = "\(self.lang.Package)" + " " + response["package"].stringValue
                                    self.statusLbl.text =  "\(self.lang.Status) \(response["kyc_status"].stringValue)"
                                    kyc_status = response["kyc_status"].stringValue
                                    self.memberLbl.text = "\(self.lang.RankMember)"
        }
    }
    
    func roundImage()
    {
        self.userImg.layer.borderWidth = 1
        self.userImg.layer.masksToBounds = false
        self.userImg.layer.borderColor = UIColor.white.cgColor
        self.userImg.layer.cornerRadius = self.userImg.frame.height/2
        self.userImg.clipsToBounds = true
    }

    @IBAction func proffileact(_ sender: UIButton) {
        
        MediaPicker.shared.showMediaPicker(imageView: userImg!, placeHolder: nil, sender: sender) { (img, check) in
            
            if check == true {
                
                self.image_pic = true
                SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
                SVProgressHUD.setContainerView(self.view)
                SVProgressHUD.show()

                let imgupload = [ImageUpload(uploadimage: self.userImg.image!, filename: "profile_picture")]
                           
                           
                           let params = ["userid": customer_id]
                           
                           
                           
                           APIManager.shared.profile_img_update(params: params as [String : AnyObject], profimage: imgupload) { (response) in
                            SVProgressHUD.dismiss()
                               print("venki\(response)")
                            
                            let msg = response["msg"].stringValue
                            let status = response["status"].stringValue
                              
                            
                            if status == "success"
                            {
                             Common.checkRemoveCdAlert()
                                    let alert = CDAlertView(title: AppName, message: msg, type: .success)
                            alert.circleFillColor = UIColor.buttonprimary
                            let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                    alert.add(action: doneAction)
                                    alert.show() { (alert) in

                                        self.viewprofile()
                                    }
                            }
                            
                            
                            else
                            {
                                let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                alert.circleFillColor = UIColor.buttonprimary

                                alert.autoHideTime = 2.0
                                alert.show()

                            }
                           }

                
            }
            
        }
        
    }
    
    @IBAction func editprofile(_ sender: Any) {
        
        self.push(id: "EditProfileViewController", animation: true, fromSB: Profile)
    }
    @IBAction func membership(_ sender: Any) {
        
        
        self.push(id: "MemberShipMainViewController", animation: true, fromSB: Profile)

    }
    
    @IBAction func registernewsubacccount(_ sender: Any) {
        
        
        self.push(id: "NewSubAccountViewController", animation: true, fromSB: Profile)

    }
    @IBAction func transfersubaccount(_ sender: Any) {
        
        
        self.push(id: "TransferSubAccountViewController", animation: true, fromSB: Profile)

    }
    @IBAction func viewcertificate(_ sender: Any) {
        

    }
    @IBAction func submitkyc(_ sender: Any) {
        
        if kyc_status == "KYC not submitted"          {
            self.push(id: "KYCViewController1", animation: true, fromSB: Profile)

        }
            else if kyc_status != "KYC Submitted Not Approved" || kyc_status != "KYC Approved"

        {
            
            if kyc_status == "KYC Submitted Not Approved"
            {
            let alert = CDAlertView(title: AppName, message: kyc_status, type: .warning)
            alert.circleFillColor = UIColor.buttonprimary

            alert.autoHideTime = 2.0
            alert.show()
            }
            else
            {
                let alert = CDAlertView(title: AppName, message: kyc_status, type: .success)
                alert.circleFillColor = UIColor.buttonprimary

                alert.autoHideTime = 2.0
                alert.show()

            }

        }
            
            
        else
        {
            
            Common.checkRemoveCdAlert()
                    let alert = CDAlertView(title: AppName, message: kyc_status, type: .error)
            alert.circleFillColor = UIColor.buttonprimary
            let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                    alert.add(action: doneAction)
                    alert.show() { (alert) in

                        self.push(id: "KYCViewController1", animation: true, fromSB: Profile)

                    }

        }
        
    }
    @IBAction func switchaccount(_ sender: Any) {
    
    
    }
    @IBAction func logout(_ sender: Any) {
        
        logoutConfirmation()
    }
    
    func logoutConfirmation(){
        
        var langCode = ""
        
        let controller = UIAlertController(title: nil, message: "Are you sure want to logout?", preferredStyle: .actionSheet)
        let yes = UIAlertAction(title: "Yes", style: .default) { (action) in

            langCode = UserDefaults.standard.string(forKey: "lang")!
            
            APPDELEGATE.timerInvalidate()
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            self.popLeft()
            
            UserDefaults.standard.set(langCode, forKey: "lang")
            UserDefaults.standard.synchronize()
            
             let welcomeVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "WelcomeViewController") as? WelcomeViewController
            let naviCont = UINavigationController(rootViewController: welcomeVC!)
            self.navigationController?.pushViewController(naviCont, animated: true)
            let appDelegate: AppDelegate = (UIApplication.shared.delegate as? AppDelegate)!
            appDelegate.window?.rootViewController = naviCont
           // self.push(id: "WelcomeViewController", animation: false, fromSB: Login)
        }
        let no = UIAlertAction(title: "No", style: .cancel) { (action) in
        }
        controller.addAction(yes)
        controller.addAction(no)
        present(controller, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

