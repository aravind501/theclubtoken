//
//  ExpandableCells.swift
//  ExpandableCell
//
//  Created by Seungyoun Yi on 2017. 8. 7..
//  Copyright © 2017년 SeungyounYi. All rights reserved.
//

import UIKit
import ExpandableCell


class NormalCell: UITableViewCell {
    static let ID = "NormalCell"
}

class ExpandableCell1: ExpandableCell {
    static let ID = "ExpandableCell"
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var firstHeadingLbl: UILabel!
    @IBOutlet weak var firstValLbl: UILabel!
    
    @IBOutlet weak var secondHeadingLbl: UILabel!
    @IBOutlet weak var secondValLbl: UILabel!
    
}

//class ExpandableCell2: ExpandableCell {
//    static let ID = "ExpandableCell"
//
//}
class ExpandableSelectableCell2: ExpandableCell {
    static let ID = "ExpandableSelectableCell2"

    override func isSelectable() -> Bool {
        return true
    }
}

class ExpandableInitiallyExpanded: ExpandableCell {
    static let ID = "InitiallyExpandedExpandableCell"
    
    override func isSelectable() -> Bool {
        return true
    }
    
    override func isInitiallyExpanded() -> Bool {
        return true
    }
}

class RewardCell: UITableViewCell {
    static let ID = "RewardCell"
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet var commTitleLabel: UILabel!
    @IBOutlet var commValLabel: UILabel!
    @IBOutlet var personalTitleLabel: UILabel!
    @IBOutlet var personalValLabel: UILabel!
}
class AssetsCell: UITableViewCell {
    static let ID = "AssetsCell"
    
    @IBOutlet weak var topupBtn: UIButton!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet var tctTitleLabel: UILabel!
    @IBOutlet var tctValLabel: UILabel!
    @IBOutlet var theClubTokenTitleLabel: UILabel!
    @IBOutlet var theClubTokenValLabel: UILabel!
    
    @IBOutlet var ethTitleLabel: UILabel!
    @IBOutlet var ethValLabel: UILabel!
    @IBOutlet var ethereumTitleLabel: UILabel!
    @IBOutlet var ethereumValLabel: UILabel!
    
    @IBOutlet var btcTitleLabel: UILabel!
    @IBOutlet var btcValLabel: UILabel!
    @IBOutlet var bitcoinTitleLabel: UILabel!
    @IBOutlet var bitcoinValLabel: UILabel!
    
    @IBOutlet var usdtTitleLabel: UILabel!
      @IBOutlet var usdtValLabel: UILabel!
      @IBOutlet var tetherTitleLabel: UILabel!
      @IBOutlet var tetherValLabel: UILabel!
    
}
class ClubBalanceCell: UITableViewCell {
    static let ID = "ClubBalanceCell"
    
    @IBOutlet weak var withDrawBtn: UIButton!
    @IBOutlet weak var exchangeBtn: UIButton!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet var digiChipTitleLabel: UILabel!
    @IBOutlet var digiChipValLabel: UILabel!
}
class RegistrationCodeCell: UITableViewCell {
    static let ID = "RegistrationCodeCell"
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet var commTitleLabel: UILabel!
    @IBOutlet var commValLabel: UILabel!
    @IBOutlet var personalTitleLabel: UILabel!
    @IBOutlet var personalValLabel: UILabel!
}
class WalletExpandableCell: UITableViewCell {
    static let ID = "WalletExpandableCell"
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var titleValLabel: UILabel!
    @IBOutlet var subTitleLabel: UILabel!
    @IBOutlet var subTitleValLabel: UILabel!
    @IBOutlet weak var copyValLbl: UILabel!
    @IBOutlet weak var copyPasteBtn: UIButton!
    @IBOutlet weak var qrBtn: UIButton!
    @IBOutlet weak var coinImgView: UIImageView!
    
    
}
class WalletCell: UITableViewCell {
    static let ID = "WalletCell"
    
    @IBOutlet weak var rewardSwapLbl: UILabel!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet var exchangeRateLabel: UILabel!
    @IBOutlet var exchangedAmountLabel: UILabel!
    @IBOutlet weak var rewardTxtFld: UITextField!
    @IBOutlet weak var assetsTxtFld: UITextField!
    @IBOutlet weak var dropDownBaseView: UIView!
    @IBOutlet weak var dropdownLbl: UILabel!
    @IBOutlet weak var dropDownBtn: UIButton!
    @IBOutlet weak var swapBtnBaseView: UIView!
    @IBOutlet weak var swapBtn: UIButton!
    
    @IBOutlet weak var rewardTitlLbl: UILabel!
    @IBOutlet weak var assetTilLbl: UILabel!
    @IBOutlet weak var exchangeRateTiLbl: UILabel!
    @IBOutlet weak var amountAfterExTiLbl: UILabel!
    
    
}
