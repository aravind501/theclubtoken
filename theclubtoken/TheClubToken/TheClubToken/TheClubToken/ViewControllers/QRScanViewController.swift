//
//  QRScanViewController.swift
//  TheClubToken
//
//  Created by trioangle on 30/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class QRScanViewController: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var scannerView: QRScannerView!
    override func viewDidLoad() {
        super.viewDidLoad()

        scannerView.delegate = self
        scannerView.startScanning()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension QRScanViewController: QRScannerViewDelegate {
    func qrScanningDidStop() {
        //let buttonTitle = scannerView.isRunning ? "STOP" : "SCAN"
        
    }
    
    func qrScanningDidFail() {
        print("Scanning Failed. Please try again")
        
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        scannerView.stopScanning()
        print(str)
        NotificationCenter.default.post(name: Notification.Name("qrCodePasser"), object: str)
        self.dismiss(animated: true, completion: nil)
        //self.qrData = QRData(codeString: str)
    }
    
    
    
}
