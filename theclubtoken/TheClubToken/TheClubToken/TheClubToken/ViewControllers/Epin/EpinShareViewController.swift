//
//  EpinShareViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 28/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CDAlertView
class EpinShareViewController: UIViewController {

    @IBOutlet weak var emailTxtfld: HoshiTextField!
    
    var epinid = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        getDesign()
        // Do any additional setup after loading the view.
    }
    
    
    func getDesign()
    {
        self.emailTxtfld.borderActiveColor = .textcolor
        self.emailTxtfld.borderInactiveColor = .buttonsecondary
        self.emailTxtfld.placeholderColor = .textcolor
        self.emailTxtfld.textColor = .textcolor
        self.emailTxtfld.delegate = self

        
    }
    @IBAction func closeact(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func submitact(_ sender: Any) {
        
         if emailTxtfld.text == ""
               
            {
                    self.view.make(toast: "Please Enter Email")

                }
              
                
            else if emailTxtfld.text?.isEmail  == false
                   
                {
                        self.view.make(toast: "Please Enter Valid Email")

                    }
        
         else
         {

             
             
             let params2 = ["userid":customer_id,
                            "email":self.emailTxtfld.text!,
                            "epinid":epinid]
          
             
             print(params2)
             
             APIManager.shared.epin_gift(params: params2 as [String : AnyObject]) { (response) in
                                         print("response params1 \(response)")
                                         print("CHeckparams")
                                         
                                         let status = response["share"]["status"].stringValue
                                         let msg = response["share"]["message"].stringValue
                                         
                                         if status == "success"
                                         {
                                              Common.checkRemoveCdAlert()
                                                     let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                             alert.circleFillColor = UIColor.buttonprimary
                                             let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                     alert.add(action: doneAction)
                                                     alert.show() { (alert) in

                                                        
                                                        self.emailTxtfld.text = ""
                                                        self.epinid = ""
                                                        
                                                        self.dismiss(animated: true, completion: nil)
                                            }
                                         }
                                         
                                         else
                                         {
                                             let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                             alert.circleFillColor = UIColor.buttonprimary

                                             alert.autoHideTime = 2.0
                                             alert.show()

                                         }

             }
             
             
             
         }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension EpinShareViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        textField.placeholder = "Email"


    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        if textField.text?.count == 0 {
            
            
            textField.placeholder = "Email"
                self.emailTxtfld.borderInactiveColor = .buttonsecondary

            
        }
    }
    
}
