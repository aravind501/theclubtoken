//
//  MemberShipViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 25/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import SwiftyJSON
class MemberShipViewController: UIViewController {

    @IBOutlet weak var globalTiLbl: UILabel!
    @IBOutlet weak var networkTiLbl: UILabel!
    @IBOutlet weak var networkearnings: UILabel!
    @IBOutlet weak var globalshare: UILabel!
    lazy var lang = Language.default.object
    override func viewDidLoad() {
        super.viewDidLoad()

        print("MemberShipViewController")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("membership"), object: nil)

        loadData()
        
        networkTiLbl.text = lang.NetworkEarnings
        globalTiLbl.text = lang.GlobalShare
        
        // Do any additional setup after loading the view.
    }
    
    func loadData()
    {
        let dict = membershipJSON[fromIndexValue].dictionaryValue
        
        networkearnings.text = dict["network_earning"]!.stringValue + "%"
        globalshare.text = dict["global_share"]!.stringValue + "%"

    }
    override func viewWillAppear(_ animated: Bool) {
        print("fromIndexValue \(fromIndexValue)")
}
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        loadData()
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
