//
//  PaymentWebViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 12/08/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD

class PaymentWebViewController: UIViewController,WKNavigationDelegate {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var geneologyWebview: WKWebView!
    lazy var lang = Language.default.object
    
    var str = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        geneologyWebview.navigationDelegate = self
        geneologyWebview.isOpaque = false
        geneologyWebview.backgroundColor = .clear
        titleLbl.text = lang.Payment
        
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
        SVProgressHUD.setContainerView(self.topMostViewController().view)
        SVProgressHUD.show()

        if str != "" && !str.isEmpty
        {
            let url1 = URL(string: str)
            self.geneologyWebview.load(URLRequest(url: url1!))

        }
        else
        {
            SVProgressHUD.dismiss()
            print("webview url empty")

        }

        // Do any additional setup after loading the view.
    }


    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        SVProgressHUD.dismiss()
        print("webview load")
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        SVProgressHUD.dismiss()
        print("webview error")
    }

    
    @IBAction func backact(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)

    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
