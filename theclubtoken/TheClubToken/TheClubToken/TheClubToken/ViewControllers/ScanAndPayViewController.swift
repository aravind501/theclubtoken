//
//  ScanAndPayViewController.swift
//  TheClubToken
//
//  Created by trioangle on 28/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CDAlertView
import SwiftyJSON

class ScanAndPayViewController: UIViewController {

    @IBOutlet weak var receiverWalletLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var scanBtn: UIButton!
    @IBOutlet weak var walletAddreddBtn: UIButton!
    @IBOutlet weak var scanBottomlbl: UILabel!
    @IBOutlet weak var walletaddressBottomLbl: UILabel!
    @IBOutlet weak var mainBaseView: UIView!
    @IBOutlet weak var walletAddressBaseView: UIView!
    @IBOutlet weak var walletAddressTxtFld: UITextField!
    @IBOutlet weak var payTransferBaseView: UIView!
    @IBOutlet weak var payTransferBtn: UIButton!
    @IBOutlet weak var headerBaseView: UIView!
    @IBOutlet weak var scannerView: QRScannerView!
    @IBOutlet weak var titleLbl: UILabel!
    
    lazy var lang = Language.default.object
    var selectedCurr = JSON()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        mainBaseView.layer.cornerRadius = 40
        headerBaseView.layer.cornerRadius = 10
        payTransferBaseView.layer.cornerRadius = payTransferBaseView.frame.height / 2
        
        walletAddressTxtFld.layer.borderWidth = 1
        walletAddressTxtFld.layer.borderColor = UIColor.borderGold.cgColor
        walletAddressTxtFld.layer.cornerRadius = 10
        
        scanBottomlbl.backgroundColor = UIColor.white
        walletaddressBottomLbl.backgroundColor = UIColor.clear

        titleLbl.text = "\(lang.ScanandPay) \(selectedCurr["currency_symbol"].stringValue)"
        scanBtn.setTitle(lang.Scan, for: .normal)
        walletAddreddBtn.setTitle(lang.enterWallet, for: .normal)
        receiverWalletLbl.text = lang.receiverWallet
        payTransferBtn.setTitle(lang.PayTransfer, for: .normal)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        scannerView.delegate = self
        scannerView.startScanning()
        scannerView.isHidden = false
        walletAddressBaseView.isHidden = true
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func scanBtnAction(_ sender: Any) {
        
        scanBottomlbl.backgroundColor = UIColor.white
        walletaddressBottomLbl.backgroundColor = UIColor.clear
        
        scannerView.isHidden = false
        walletAddressBaseView.isHidden = true
        scannerView.startScanning()
    }
    
    @IBAction func walletAddressBtnAction(_ sender: Any) {
        
        
        scanBottomlbl.backgroundColor = UIColor.clear
        walletaddressBottomLbl.backgroundColor = UIColor.white
        
        scannerView.isHidden = true
        walletAddressBaseView.isHidden = false
        scannerView.stopScanning()
    }
    @IBAction func payTransferBtnAction(_ sender: Any) {
        
        if walletAddressTxtFld.text!.count > 0 {
            
            let receInfoVc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "ReceiverInfoViewController") as? ReceiverInfoViewController
            receInfoVc!.isQr = false
            receInfoVc!.currencyId = selectedCurr["id"].stringValue
            receInfoVc!.address = walletAddressTxtFld.text!
            self.navigationController?.pushViewController(receInfoVc!, animated: true)
            
        }
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ScanAndPayViewController: QRScannerViewDelegate {
    func qrScanningDidStop() {
        //let buttonTitle = scannerView.isRunning ? "STOP" : "SCAN"
        
    }
    
    func qrScanningDidFail() {
        print("Scanning Failed. Please try again")
        
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        print(str)
        
        let receInfoVc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "ReceiverInfoViewController") as? ReceiverInfoViewController
        receInfoVc!.isQr = true
        receInfoVc!.currencyId = selectedCurr["id"].stringValue
        receInfoVc!.address = str!
        self.navigationController?.pushViewController(receInfoVc!, animated: true)
        //self.qrData = QRData(codeString: str)
    }
    
    
    
}
