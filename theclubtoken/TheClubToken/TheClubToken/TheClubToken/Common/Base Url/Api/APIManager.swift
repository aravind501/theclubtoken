//
//  APIManager.swift
//  Noor
//
//  Created by Uplogic-user on 19/05/17.
//  Copyright © 2017 Uplogic. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation
import SVProgressHUD
import SwiftyJSON

typealias coordinatesCompletionBlock = (_ coordinate: CLLocationCoordinate2D?) -> Void
typealias responseCompletionBlock = (_ response: JSON) -> Void
typealias readJsonBlock = (_ response: [AnyObject]) -> Void

class APIManager: NSObject {
    
    static let shared = APIManager()
    
   func Register(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.userRegister.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    func Home(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.home.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    func Home_package_info(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.package_info.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func Home_get_referralcode(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.get_referralcode.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    
    func Refer_Code(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.referral.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }

    func get_refercode(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.get_refercode.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }

    func Promotions(callback: @escaping responseCompletionBlock){
        apiGetRequest(stringApi: BASEAPI.URL + Base.promotions.rawValue, method: .get) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func digital_chips_websites(callback: @escaping responseCompletionBlock){
        apiGetRequest(stringApi: BASEAPI.URL + Base.digital_chips_websites.rawValue, method: .get) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func transfer_chips(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.transfer_chips.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func currency_list(callback: @escaping responseCompletionBlock){
        apiGetRequest(stringApi: BASEAPI.URL + Base.currency_list.rawValue, method: .get) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func TopUpShow(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.topup_show.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func TopUpShowWithOutProgress(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
           
        apiRequestWithoutProgress(stringApi: BASEAPI.URL + Base.topup_show.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
       }
    
    func PayTransferShow(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.transfer_show.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    func PayTransfer(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
           apiRequest(stringApi: BASEAPI.URL + Base.transfer_pay.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
               callback(apiresponse)
           }
       }
    func EpinUpgradePackageShow(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.upgrade_package_epinshow.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    func EpinUpgradePackage(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
           apiRequest(stringApi: BASEAPI.URL + Base.upgrade_package_epin.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
               callback(apiresponse)
           }
       }
       func TopUpAsset(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
           apiRequest(stringApi: BASEAPI.URL + Base.topup_asset.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
               callback(apiresponse)
           }
       }
    
    func Wallet(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.wallet.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    func Swap(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.swap.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func SwapShow(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
           apiRequestWithoutProgress(stringApi: BASEAPI.URL + Base.swap_show.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
               callback(apiresponse)
           }
       }
    
    func GetQrCode(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.get_qrcode.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func ExchangeChipShow(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
           apiRequestWithoutProgress(stringApi: BASEAPI.URL + Base.exchange_chipshow.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
               callback(apiresponse)
           }
       }
       func ExchangeChips(params: [String: AnyObject],callback: @escaping responseCompletionBlock){
           apiRequest(stringApi: BASEAPI.URL + Base.exchange_chips.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
               callback(apiresponse)
           }
       }
    
   func phone_otp_signup(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.phone_otp_signup.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }

    
    func verify_phone(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
         apiRequest(stringApi: BASEAPI.URL + Base.verify_phone.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
             callback(apiresponse)
         }
     }

    func email_otp_signup(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
         apiRequest(stringApi: BASEAPI.URL + Base.email_otp_signup.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
             callback(apiresponse)
         }
     }

    func verify_email(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
         apiRequest(stringApi: BASEAPI.URL + Base.verify_email.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
             callback(apiresponse)
         }
     }

    func email_otp_signin(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.email_otp_signin.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func phone_otp_signin(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.phone_otp_signin.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }

    
    func login_email(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.login_email.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func login_phone(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.login_phone.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func update_profile(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.update_profile.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }

    func view_profile(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.view_profile.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func announcements(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.announcements.rawValue, method: .get, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }

   
    func profile_img_update(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.profile_img_update.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
   
    func kyc_step1(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.kyc_step1.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func kyc_step2(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.kyc_step2.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    
    func kyc_step3(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.kyc_step3.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }

    
    func countriesList(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.countries.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }

    func statesList(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.states.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }

    func citiesList(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.cities.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }

    
    func profile_img_update(params: [String: AnyObject]?,profimage : [ImageUpload],callback: @escaping responseCompletionBlock){
        
        
        
        postFormRequest(stringApi: BASEAPI.URL + Base.profile_img_update.rawValue, parameters: params, images: profimage) { (apiresponse) in
            callback(apiresponse)
        }
    }
    
    
    func kyc_img_update(params: [String: AnyObject]?,profimage : [ImageUpload],profimage1 : [ImageUpload],callback: @escaping responseCompletionBlock){
        
        
        
        postFormRequestImages(stringApi: BASEAPI.URL + Base.kyc_step3.rawValue, parameters: params, images: profimage, images1: profimage1) { (apiresponse) in
            callback(apiresponse)
        }
    }

    
     func view_notification(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
         apiRequest(stringApi: BASEAPI.URL + Base.notifications.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
             callback(apiresponse)
         }
     }
    
     func membership(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
         apiRequest(stringApi: BASEAPI.URL + Base.membership.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
             callback(apiresponse)
         }
     }
    
     func create_sub_account(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
         apiRequest(stringApi: BASEAPI.URL + Base.create_sub_account.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
             callback(apiresponse)
         }
     }
    
    func subaccount_list(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.subaccount_list.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }

    
    func support_category(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.support_category.rawValue, method: .get, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func support(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.support.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func feedback(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.feedback.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
   
    func user_epins(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.user_epins.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    func generate_epinbarcode(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.generate_epinbarcode.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    func epin_gift(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.epin_gift.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    func create_epin(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.create_epin.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }

    func packages(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.packages.rawValue, method: .get, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    
    func currency_list1(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.currency_list.rawValue, method: .get, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func upgrade_package_show(params: [String: AnyObject]?,from:String,callback: @escaping responseCompletionBlock){
        
        var str = ""
        if from == "TCT"
        {
            str = BASEAPI.URL + Base.upgrade_package_show.rawValue
        }
        else
        {
            str = BASEAPI.URL + Base.upgrade_package_cryptoshow.rawValue

        }
        apiRequest(stringApi: str, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    
    func upgrade_package(params: [String: AnyObject]?,from:String,callback: @escaping responseCompletionBlock){
        
        var str = ""
        if from == "TCT"
        {
            str = BASEAPI.URL + Base.upgrade_package.rawValue
        }
        else
        {
            str = BASEAPI.URL + Base.upgrade_package_crypto.rawValue

        }
        apiRequest(stringApi: str, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    
    func payment(params: [String: AnyObject]?,from:String,callback: @escaping responseCompletionBlock){
        
                apiRequest(stringApi: BASEAPI.URL + Base.payment.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }

    }
    
    
    
    func payment_packages(params: [String: AnyObject]?,from:String,callback: @escaping responseCompletionBlock){
        
                apiRequest(stringApi: BASEAPI.URL + Base.payment_packages.rawValue, method: .get, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }

    }

    

    
    
    func get_currency_balance(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.get_currency_balance.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func create_epin_show(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.create_epin_show.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func upgrade_package_epinshow(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.upgrade_package_epinshow.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func upgrade_package_cryptoshow(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.upgrade_package_cryptoshow.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    

    func termsandconditions(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.cms.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }

    

    
    func transfer_subaccount_email(params: [String: AnyObject]?,from:String,callback: @escaping responseCompletionBlock){
        
        var str = ""
        
        if from == "Email"
        {
           str = BASEAPI.URL + Base.transfer_subaccount_email.rawValue
        }
        else
        {
            str = BASEAPI.URL + Base.transfer_subaccount_phone.rawValue

        }
        apiRequest(stringApi: BASEAPI.URL + str, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    

    func genealogy_teamview(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.genealogy_teamview.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    func genealogy_treeview(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.genealogy_treeview.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }

    
    func transaction_filter_view(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.transaction_filters.rawValue, method: .get, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }

    func transaction_filter(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.transaction_filter.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }
    
    func transactions(params: [String: AnyObject]?,callback: @escaping responseCompletionBlock){
        apiRequest(stringApi: BASEAPI.URL + Base.transactions.rawValue, method: .post, parameters: params) { (apiresponse) -> Void in
            callback(apiresponse)
        }
    }


    
    
    private func apiGetRequest(stringApi: String,method: HTTPMethod,showloading: Bool = true,encoding: ParameterEncoding = URLEncoding.default,header: [String: String] = [:],callback: @escaping responseCompletionBlock){
      
        if(!hasInternet()){
            callback(JSON())
            self.showAlertView()
            return
        }
        
        
        if showloading == true {
            SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
            SVProgressHUD.setContainerView(topMostViewController().view)
            SVProgressHUD.show()
            
        }
        let myheader = ["X-Authorization":""]
                        //"Authorization":"Bearer "+APPDELEGATE.bearerToken]
        

        Alamofire.request(stringApi, method: method,encoding:encoding,headers: myheader).responseJSON {(response )in
            SVProgressHUD.dismiss()
            do {
                print(JSON(response.data!))
                callback(JSON(response.data!))
            }
            catch {
                
                callback(JSON())
            }
        }
        
    }
    
    private func apiRequestWithoutProgress(stringApi: String,method: HTTPMethod, parameters: [String: AnyObject]?,showloading: Bool = true,encoding: ParameterEncoding = URLEncoding.default,header: [String: String] = [:],callback: @escaping responseCompletionBlock){
      
        if(!hasInternet()){
            callback(JSON())
            self.showAlertView()
            return
        }
        
        let myheader = ["X-Authorization":""]
                        //"Authorization":"Bearer "+APPDELEGATE.bearerToken]
        
        print("param \(String(describing: parameters)) stringApi \(stringApi)")
        
        Alamofire.request(stringApi, method: method,parameters: parameters,encoding:encoding,headers: myheader).responseJSON {(response )in
            do {
                print(JSON(response.data!))
                callback(JSON(response.data!))
            }
            catch {
                
                callback(JSON())
            }
        }
        
    }

    
    
    private func apiRequest(stringApi: String,method: HTTPMethod, parameters: [String: AnyObject]?,showloading: Bool = true,encoding: ParameterEncoding = URLEncoding.default,header: [String: String] = [:],callback: @escaping responseCompletionBlock){
      
        if(!hasInternet()){
            callback(JSON())
            self.showAlertView()
            return
        }
        
        
        if showloading == true {
            SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
            SVProgressHUD.setContainerView(topMostViewController().view)
            SVProgressHUD.show()
            
        }
        let myheader = ["X-Authorization":""]
                        //"Authorization":"Bearer "+APPDELEGATE.bearerToken]
        
        print("param \(String(describing: parameters)) stringApi \(stringApi)")
        
        Alamofire.request(stringApi, method: method,parameters: parameters,encoding:encoding,headers: myheader).responseJSON {(response )in
            SVProgressHUD.dismiss()
            do {
                print(JSON(response.data!))
                callback(JSON(response.data!))
            }
            catch {
                
                callback(JSON())
            }
        }
        
    }
    
    // MARK:- POST Image Upload
    private func postFormRequest(stringApi: String, parameters: [String: AnyObject]?,images: [ImageUpload],header: [String: String] = [:], callback: @escaping responseCompletionBlock) {
        
        print("postRequest :",stringApi)
        if(!hasInternet()){
            callback(JSON())
            self.showAlertView()
            return
        }
       // SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
      //  SVProgressHUD.setContainerView(topMostViewController().view)
     //   SVProgressHUD.show()
        let URL = try! URLRequest(url: stringApi, method: .post,headers: header)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for imageupload in images {
                let currentTimeStamp = NSDate().timeIntervalSince1970.toString()
                let filename = "\(currentTimeStamp)_img.jpg"
                
                if(imageupload.image.size == .zero){                    
                    multipartFormData.append("".data(using: String.Encoding.utf8)!, withName: "")
                }
                else{
                   
        let datas = imageupload.image.jpegData(compressionQuality: 0.6)
                    if let imageData = imageupload.image.jpegData(compressionQuality: 0.6)  {
                        multipartFormData.append(imageData, withName: imageupload.name, fileName: filename, mimeType: "image/jpeg")
                        
                    }
                }
            }
            
            for (key, value) in parameters! {
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
                
            }
        }, with: URL, encodingCompletion: { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                 //   SVProgressHUD.dismiss()
                    print(response.request!)  // original URL request
                    print(response.response!) // URL response
                    print(response.data!)     // server data
                    print(response.result)   // result of response serialization

                    do {
                        let apiResponse = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments)
                        print(apiResponse)
                        callback(JSON(response.data!))
                    }
                    catch {
                       // SVProgressHUD.dismiss()
                        callback(JSON())
                    }
                }
                
            case .failure(let encodingError):
                SVProgressHUD.dismiss()
                callback(JSON())
                print(encodingError)
            }
            
        })
    }
    
    
    private func postFormRequestImages(stringApi: String, parameters: [String: AnyObject]?,images: [ImageUpload],images1: [ImageUpload],header: [String: String] = [:], callback: @escaping responseCompletionBlock) {
        
        print("postRequest :",stringApi)
        if(!hasInternet()){
            callback(JSON())
            self.showAlertView()
            return
        }
       // SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
      //  SVProgressHUD.setContainerView(topMostViewController().view)
     //   SVProgressHUD.show()
        let URL = try! URLRequest(url: stringApi, method: .post,headers: header)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for imageupload in images {
                let currentTimeStamp = NSDate().timeIntervalSince1970.toString()
                let filename = "\(currentTimeStamp)_img.jpg"
                
                if(imageupload.image.size == .zero){
                    multipartFormData.append("".data(using: String.Encoding.utf8)!, withName: "")
                }
                else{
                   
        let datas = imageupload.image.jpegData(compressionQuality: 0.6)
                    if let imageData = imageupload.image.jpegData(compressionQuality: 0.6)  {
                        multipartFormData.append(imageData, withName: imageupload.name, fileName: filename, mimeType: "image/jpeg")
                        
                    }
                }
            }
            
            
                for imageupload in images1 {
                    let currentTimeStamp = NSDate().timeIntervalSince1970.toString()
                    let filename = "\(currentTimeStamp)_img.jpg"
                    
                    if(imageupload.image.size == .zero){
                        multipartFormData.append("".data(using: String.Encoding.utf8)!, withName: "")
                    }
                    else{
                       
            let datas = imageupload.image.jpegData(compressionQuality: 0.6)
                        if let imageData = imageupload.image.jpegData(compressionQuality: 0.6)  {
                            multipartFormData.append(imageData, withName: imageupload.name, fileName: filename, mimeType: "image/jpeg")
                            
                        }
                    }
                }
            
            for (key, value) in parameters! {
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
                
            }
        }, with: URL, encodingCompletion: { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                 //   SVProgressHUD.dismiss()
                    print(response.request!)  // original URL request
                    print(response.response!) // URL response
                    print(response.data!)     // server data
                    print(response.result)   // result of response serialization

                    do {
                        let apiResponse = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments)
                        print(apiResponse)
                        callback(JSON(response.data!))
                    }
                    catch {
                       // SVProgressHUD.dismiss()
                        callback(JSON())
                    }
                }
                
            case .failure(let encodingError):
                SVProgressHUD.dismiss()
                callback(JSON())
                print(encodingError)
            }
            
        })
    }
    func readJson(callback:readJsonBlock) {
        do {
            if let file = Bundle.main.url(forResource: "countries", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: AnyObject] {
                    // json is a dictionary
                    callback([AnyObject]())
                    print(object)
                } else if let object = json as? [AnyObject] {
                    // json is an array
                    print(object)
                    callback(object)
                } else {
                    callback([AnyObject]())
                    print("JSON is invalid")
                }
            } else {
                callback([AnyObject]())
                print("no file")
            }
        } catch {
            callback([AnyObject]())
            print(error.localizedDescription)
        }
    }
    
    func getCoordinates(address: String,callback: @escaping coordinatesCompletionBlock){
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) { (placemarks, error) in
            if(error != nil){
                callback(CLLocationCoordinate2D())
            }
            else{
                if(!(placemarks?.isEmpty)!){
                    callback(placemarks?.first?.location?.coordinate)
                }
                else{
                    callback(CLLocationCoordinate2D())
                }
            }
        }
    }
    
    
}

class ImageUpload{
    var image: UIImage!
    var name: String!
    
    init(uploadimage: UIImage,filename: String){
        image = uploadimage
        name = filename
    }
}

extension Double {
    func format(f: String) -> String {
        return NSString(format: "%\(f)f" as NSString, self) as String
    }
    
    func toString() -> String {
        return String(format: "%f",self)
    }
    
    func toInt() -> Int{
        let temp:Int64 = Int64(self)
        return Int(temp)
    }
}

