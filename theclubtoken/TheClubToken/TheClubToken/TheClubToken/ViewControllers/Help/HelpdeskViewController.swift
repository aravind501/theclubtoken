//
//  HelpdeskViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 27/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CDAlertView
import DropDown

class HelpdeskViewController: UIViewController,UITextViewDelegate {

    @IBOutlet weak var nameTxtFld: HoshiTextField!
    @IBOutlet weak var emailTxtFld: HoshiTextField!
    @IBOutlet weak var messageTxtView: UITextView!
    @IBOutlet weak var bottomview: UIView!
   
    @IBOutlet weak var dropdownLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var acceptPrivacyTiLbl: UILabel!
    @IBOutlet weak var sendBtn: UIButton!
    
    let dropDown = DropDown()
    var categoryList = [String]()
    lazy var lang = Language.default.object

    override func viewDidLoad() {
        super.viewDidLoad()

      getcategory()
        
        messageTxtView.text = lang.Message
        messageTxtView.delegate = self
     
        getDesign()
        
        titleLbl.text = lang.HelpdeskSupport
        nameTxtFld.placeholder = lang.name
        emailTxtFld.placeholder = lang.Email
        dropdownLbl.text = lang.Category
        messageTxtView.text = lang.Message
        acceptPrivacyTiLbl.text = lang.IacceptPrivacyPolicyandTermsofConditions
        sendBtn.setTitle(lang.Send, for: .normal)
        // Do any additional setup after loading the view.
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == lang.Message {
            textView.text = ""
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == "" {
            textView.text = lang.Message
        }
    }

    
    func getDesign()
    {
        self.nameTxtFld.borderActiveColor = .textcolor
        self.nameTxtFld.borderInactiveColor = .buttonsecondary
        self.nameTxtFld.placeholderColor = .textcolor
        self.nameTxtFld.textColor = .textcolor
        self.nameTxtFld.delegate = self
       
        
        self.emailTxtFld.borderActiveColor = .textcolor
        self.emailTxtFld.borderInactiveColor = .buttonsecondary
        self.emailTxtFld.placeholderColor = .textcolor
        self.emailTxtFld.textColor = .textcolor
        self.emailTxtFld.delegate = self

        
    }

    func getcategory()
    {
        
        let params1 = [":":":"]
        
        print(params1)
        
                                APIManager.shared.support_category(params: params1 as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                                    let types = response["support_category"].arrayValue

                                    for i in 0...types.count - 1
                                    {
                                        let dict = types[i].dictionaryValue
                                        let name = dict["category_name"]?.stringValue
                                        self.categoryList.append(name!)
                                    }
        }
    }
    @IBAction func backact(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

        
    }
    @IBAction func dropdownact(_ sender: Any) {
        
        self.dropDown.dataSource = self.categoryList
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            self.dropdownLbl.text = item

            self.dropDown.hide()

        }
        
        dropDown.anchorView = bottomview
        dropDown.borderColor = UIColor.black
        dropDown.backgroundColor = UIColor.white
        dropDown.borderLineWidth = 1.0
        
        dropDown.show()

        
    }
    @IBAction func sendact(_ sender: Any) {
        
        if nameTxtFld.text == ""
        {
            self.view.make(toast: lang.Please_Enter_User_Name)

        }

    else if emailTxtFld.text == ""
           
        {
            self.view.make(toast: lang.Please_Enter_Email_Id)

            }
          
            
        else if emailTxtFld.text?.isEmail  == false
               
            {
                self.view.make(toast: lang.Please_Enter_Valid_Email_Id)

                }
         
        else if dropdownLbl.text == lang.Category
               
            {
                self.view.make(toast: lang.Please_Select_Category)

                }
            
        else if messageTxtView.text == lang.Message
               
            {
                self.view.make(toast: lang.Please_Enter_Your_Message)

                }
            

        else
        {

            
            
            let params2 = ["userid":customer_id,
                           "name":nameTxtFld.text!,
                           "category":dropdownLbl.text!,
                           "message":messageTxtView.text!,
                           "email":emailTxtFld.text!]

         
            
            print(params2)
            
            APIManager.shared.support(params: params2 as [String : AnyObject]) { (response) in
                                        print("response params1 \(response)")
                                        print("CHeckparams")
                                        
                                        let status = response["support"]["status"].stringValue
                                        let msg = response["support"]["msg"].stringValue
                                        
                                        if status == "success"
                                        {
                                             Common.checkRemoveCdAlert()
                                                    let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                            alert.circleFillColor = UIColor.buttonprimary
                                            let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                    alert.add(action: doneAction)
                                                    alert.show() { (alert) in

                                                        self.nameTxtFld.text = ""
                                                        self.emailTxtFld.text = ""
                                                        self.dropdownLbl.text = self.lang.Category
                                                        self.messageTxtView.text = self.lang.Message

                                                    }
                                        }
                                        
                                        else
                                        {
                                            let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                            alert.circleFillColor = UIColor.buttonprimary

                                            alert.autoHideTime = 2.0
                                            alert.show()

                                        }

            }
            
            
            
        }
    }
    
    @IBAction func privacyact(_ sender: Any) {
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension HelpdeskViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == nameTxtFld
        {
            nameTxtFld.placeholder = lang.Name
        }
        
        else if textField == emailTxtFld
        {
            emailTxtFld.placeholder = lang.Email
        }

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        if textField.text?.count == 0 {
            
            
            
            
            if textField == nameTxtFld
            {
                nameTxtFld.placeholder = lang.Name
                self.nameTxtFld.borderInactiveColor = .buttonsecondary

                
            }
            
            else if textField == emailTxtFld
            {
                emailTxtFld.placeholder = lang.Email
                self.emailTxtFld.borderInactiveColor = .buttonsecondary

            }
            
        }
    }
    
}
