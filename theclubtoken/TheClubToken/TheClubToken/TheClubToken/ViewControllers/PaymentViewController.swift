//
//  PaymentViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 12/08/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import SwiftyJSON
import CDAlertView

class PaymentViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var packageBtn: UIButton!
 @IBOutlet weak var currencyBtn: UIButton!
    @IBOutlet weak var backbtnblack: UIButton!
    
    @IBOutlet weak var locationview: UIView!
    @IBOutlet weak var tblviewTitleLbl: UILabel!
    @IBOutlet weak var locationtableview: UITableView!
    @IBOutlet weak var viewheight: NSLayoutConstraint!

    var selectedList = "packages"

    @IBOutlet weak var packagetitle: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var packageTypeLbl: UILabel!
    @IBOutlet weak var paybyLbl: UILabel!
    @IBOutlet weak var proceedBtn: UIButton!
   // var packagesDatas = ["$50","$100","$200","$500","$1000","$5000","$10000","$50000","$100000"]
    
    
    var packagesDatas = [JSON]()

    var currencyDatas = ["USD"]
    
    
    
    lazy var lang = Language.default.object

    override func viewDidLoad() {
        super.viewDidLoad()

         locationtableview.delegate = self
         locationtableview.dataSource = self
         
         locationtableview.tableFooterView = UIView()
         locationtableview.estimatedRowHeight = 60.0
         locationtableview.rowHeight = UITableView.automaticDimension

        
        titleLbl.text = lang.Payment
        packagetitle.text = lang.paypackagetitle
        packageTypeLbl.text = lang.paypackagetype
        paybyLbl.text = lang.paypackagepayby

        
        packageBtn.setTitle(lang.PackageTit, for: .normal)
        currencyBtn.setTitle(lang.Currency, for: .normal)
        proceedBtn.setTitle(lang.payproceed, for: .normal)
        
        
        listpackages()
        
        // Do any additional setup after loading the view.
    }
    
    
    func listpackages()
    {
        let params1 = [":":":"]
            
let pack = ""
        print(params1)
        
            APIManager.shared.payment_packages(params: params1 as [String : AnyObject], from: pack) { (response) in
                                  
                
                print("response payment \(response)")
                                  


                var msg = ""
                
                var status = ""
                status = response["status"].stringValue
                msg = response["message"].stringValue

             
               if status == "success"
                                    {

                                        self.packagesDatas = response["packages"].arrayValue
                                        self.locationtableview.reloadData()
                                    }
                                    
                                    else
                                    {
                                        let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                        alert.circleFillColor = UIColor.buttonprimary

                                        alert.autoHideTime = 2.0
                                        alert.show()

                                    }

        }
    }
    
         func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
             let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! locationcells
             
            
                    if selectedList == "packages"
                    {
                        
                        cell.name.text = packagesDatas[indexPath.row].stringValue

            }
            
            else
                    {

                        cell.name.text = currencyDatas[indexPath.row]
            }
             

            // cell.name.text = stringarray[indexPath.row]
             return cell
         }
         
         
         func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
             return UITableView.automaticDimension
         }
        
         func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            if selectedList == "packages"
            {
                return packagesDatas.count

            }
            else
            {
                return currencyDatas.count

            }
         }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            
            if selectedList == "packages"
            {
                let name = packagesDatas[indexPath.row].stringValue
            
                self.packageBtn.setTitle(name, for: .normal)
            }
            else
            {
                let name = currencyDatas[indexPath.row]
                self.currencyBtn.setTitle(name, for: .normal)

            }
            
            
            
            
            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                
                self.locationview.isHidden = true
                
                self.backbtnblack.isHidden = true
            })
            
            
            
        }
        
        @IBAction func backact(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)
        }
        
        @IBAction func viewcloseact(_ sender: Any) {
            
            
            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                
                self.locationview.isHidden = true
                
                self.backbtnblack.isHidden = true
                
            })
            
            
        }
        
        @IBAction func selectamountBtn(_ sender: Any) {
            
    selectedList = "packages"
            tblviewTitleLbl.text = "Select Package"
            self.viewheight.constant = 380

            self.locationtableview.reloadData()
            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                
                self.locationview.isHidden = false
                
                self.backbtnblack.isHidden = false
                
            })
            
        }
        
        @IBAction func selectCurrencyBtn(_ sender: Any) {
            selectedList = "currency"
            tblviewTitleLbl.text = "Select Currency"

            
             self.viewheight.constant = 150

            
            self.locationtableview.reloadData()
            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                
                self.locationview.isHidden = false
                
                self.backbtnblack.isHidden = false
                
            })
            
        }
      
    @IBAction func proceedBtn(_ sender: Any) {
        
        if packageBtn.currentTitle == lang.PackageTit
         {
             self.view.make(toast: "Please Select Package")
         }
        else if currencyBtn.currentTitle == lang.Currency
         {
             self.view.make(toast: "Please Select Currency")

         }
        
         else
         {
             
             
             let package = self.packageBtn.currentTitle
            let currency = self.currencyBtn.currentTitle

            let packa = package?.replacingOccurrences(of: "$", with: "")
             var pack = ""
          
            let params1 = ["package_amount":packa!,
                            "currency_symbol": currency!]
             

         print(params1)
         
             APIManager.shared.payment(params: params1 as [String : AnyObject], from: pack) { (response) in
                                   
                 
                 print("response payment \(response)")
                                   


                 var msg = ""
                 
                 var status = ""
                 status = response["status"].stringValue
                 msg = response["message"].stringValue

                let url = response["payment_url"].stringValue
              
                if status == "success"
                                     {
                                         
                                        let packageUpgradeVc = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "PaymentWebViewController") as? PaymentWebViewController
                                        
                                      /*  if url != ""
                                        {
                                            if #available(iOS 10.0, *) {
                                                guard let url = URL(string: url) else { return }
                                                UIApplication.shared.open(url)
                                                
                                            } else {
                                                
                                                guard let url = URL(string: url) else { return }
                                                UIApplication.shared.openURL(url)
                                                
                                            }
                                        }*/
                                        
                                        packageUpgradeVc?.str = url
                                        self.navigationController?.pushViewController(packageUpgradeVc!, animated: true)


                                     }
                                     
                                     else
                                     {
                                         let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                         alert.circleFillColor = UIColor.buttonprimary

                                         alert.autoHideTime = 2.0
                                         alert.show()

                                     }

         }
         }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
