//
//  ExchangeViewController.swift
//  TheClubToken
//
//  Created by trioangle on 27/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CDAlertView
import SwiftyJSON
import DropDown

class ExchangeViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var dropDownBaseView: UIView!
    @IBOutlet weak var downArrowImgView: UIImageView!
    @IBOutlet weak var selectedValLbl: UILabel!
    @IBOutlet weak var dropDownBtn: UIButton!
    @IBOutlet weak var btcTxtFld: UITextField!
    @IBOutlet weak var balanceValLbl: UILabel!
    @IBOutlet weak var totalDigValLbl: UILabel!
    @IBOutlet weak var exchageBtn: UIButton!
    @IBOutlet weak var otherCoinBaseView: UIView!
    @IBOutlet weak var tctCoinBaseView: UIView!
    @IBOutlet weak var topBalanceValLbl: UILabel!
    
    @IBOutlet weak var assetsTxtFld: UITextField!
    @IBOutlet weak var rewardTxtFld: UITextField!
    
    @IBOutlet weak var tctBalValLbl: UILabel!
    @IBOutlet weak var tctToalDigValLbl: UILabel!
    @IBOutlet weak var BTCCATEGORYTITLE: UILabel!
    @IBOutlet weak var BTCTITLE: UILabel!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var assetsTitleLbl: UILabel!
    @IBOutlet weak var exchangeDigitalChipsTitleLbl: UILabel!
    @IBOutlet weak var balanceTitleLbl: UILabel!
    @IBOutlet weak var tctWalletCategory: UILabel!
    @IBOutlet weak var rewardsTitleLbl: UILabel!
    @IBOutlet weak var assetTitleLbl: UILabel!
    @IBOutlet weak var balanceTctTitleLbl: UILabel!
    @IBOutlet weak var totalDigitalTitleLbl: UILabel!
    @IBOutlet weak var balanceBtcTitleLbl: UILabel!
    @IBOutlet weak var totaldigitalBtcTitileLbl: UILabel!
    
    var exchangeChipDict = JSON()
    let dropDown = DropDown()
    var selCurrId = String()
    lazy var lang = Language.default.object
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLbl.text = lang.ExchangeDigitalChips
        assetsTitleLbl.text = lang.assets
        exchangeDigitalChipsTitleLbl.text = lang.ExchangeDigitalChips
        balanceTitleLbl.text = lang.Balance
        
        tctWalletCategory.text = lang.tctWalletCategory
        rewardsTitleLbl.text = lang.rewards
        assetTitleLbl.text = lang.assets
        
        balanceTctTitleLbl.text = lang.Balance
        totalDigitalTitleLbl.text = lang.TotalDigitalChips
        balanceBtcTitleLbl.text = lang.Balance
        totaldigitalBtcTitileLbl.text = lang.TotalDigitalChips
        
        exchageBtn.setTitle(lang.exchange, for: .normal)
        

        self.closeBtn.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysTemplate),
                                      for: .normal)
        self.closeBtn.tintColor = UIColor.white
        
        self.dropDownBaseView.layer.cornerRadius = 5
        
        self.btcTxtFld.layer.cornerRadius = 5
        self.btcTxtFld.layer.borderWidth = 1
        self.btcTxtFld.layer.borderColor = UIColor.borderGold.cgColor
        
        self.rewardTxtFld.layer.cornerRadius = 5
        self.rewardTxtFld.layer.borderWidth = 1
        self.rewardTxtFld.layer.borderColor = UIColor.borderGold.cgColor
        
        self.assetsTxtFld.layer.cornerRadius = 5
        self.assetsTxtFld.layer.borderWidth = 1
        self.assetsTxtFld.layer.borderColor = UIColor.borderGold.cgColor
        
        self.btcTxtFld.delegate = self
        self.rewardTxtFld.delegate = self
        self.assetsTxtFld.delegate = self
        
        let dict = APPDELEGATE.currenctListArry.first
        selCurrId = "\(dict!["id"].stringValue)"
        self.selectedValLbl.text = "\(exchangeChipDict["currency_symbol"].stringValue)"
        BTCCATEGORYTITLE.text = "BTC \(lang.WalletCategory)"
        if self.selectedValLbl.text == "TCT" {
            
            self.tctCoinBaseView.isHidden = false
            self.otherCoinBaseView.isHidden = true
            
            self.tctBalValLbl.text = "\(exchangeChipDict["balance"].stringValue)"
            self.topBalanceValLbl.text = "\(exchangeChipDict["balance"].stringValue)"
            self.tctToalDigValLbl.text = "0.0"
        }
        else
        {
            self.tctCoinBaseView.isHidden = true
            self.otherCoinBaseView.isHidden = false
            
            self.balanceValLbl.text = "\(exchangeChipDict["balance"].stringValue)"
            self.topBalanceValLbl.text = "\(exchangeChipDict["balance"].stringValue)"
            self.totalDigValLbl.text = "0.0"
                //String(describing: Double(exchangeChipDict["total_digital_chips"].stringValue)!)
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func exchangeBtnAction(_ sender: Any) {
        
        callExchangeChipsApi(currency: self.selCurrId)
    }
    @IBAction func dropDownBtnAction(_ sender: Any) {
        
        var dropDownList = [String]()
        
        
        for dict in APPDELEGATE.currenctListArry {
            
            dropDownList.append("\(dict["currency_symbol"].stringValue)")
        }
        
        self.dropDown.dataSource = dropDownList
            self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                
                self.assetsTxtFld.text = ""
                self.rewardTxtFld.text = ""
                self.btcTxtFld.text = ""
                let selDict = APPDELEGATE.currenctListArry[index]
                self.selCurrId = "\(selDict["id"].stringValue)"
                //self.selectedValLbl.text = item
                self.callGet_currency_balanceApi(currency: "\(selDict["id"].stringValue)")

                self.dropDown.hide()

            }
            
           dropDown.anchorView = self.dropDownBaseView
            dropDown.borderColor = UIColor.black
            dropDown.backgroundColor = UIColor.white
            dropDown.borderLineWidth = 1.0
            
            dropDown.show()
    }
    @IBAction func closeBtnAction(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    func callGet_currency_balanceApi(currency:String) {

        let params = ["userid":customer_id,"currency":currency]
                                print(params)
        
                                APIManager.shared.get_currency_balance(params: params as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")

                                    if response.count > 0 {
                                        
                                        self.exchangeChipDict = response
                                        
                                        self.selectedValLbl.text = "\(self.exchangeChipDict["currency_symbol"].stringValue)"
                                        
                                        if self.selectedValLbl.text == "TCT" {
                                            
                                            self.tctCoinBaseView.isHidden = false
                                            self.otherCoinBaseView.isHidden = true
                                            
                                            self.tctBalValLbl.text = "\(self.exchangeChipDict["balance"].stringValue)"
                                            self.topBalanceValLbl.text = "\(self.exchangeChipDict["balance"].stringValue)"
                                            self.tctToalDigValLbl.text = "0.0"
                                        }
                                        else
                                        {
                                            self.BTCCATEGORYTITLE.text = self.selectedValLbl.text! + " " + "\(self.lang.WalletCategory)"
                                            self.BTCTITLE.text = self.selectedValLbl.text!
                                            self.tctCoinBaseView.isHidden = true
                                            self.otherCoinBaseView.isHidden = false
                                            
                                            self.balanceValLbl.text = "\(self.exchangeChipDict["balance"].stringValue)"
                                            self.topBalanceValLbl.text = "\(self.exchangeChipDict["balance"].stringValue)"
                                            self.totalDigValLbl.text = "0.0"
                                        }
                        
                                    }
                                            else {
                                                Common.checkRemoveCdAlert()

                                                let alert = CDAlertView(title: AppName, message: "EXCHANGECHIPS", type: .error)
                                                alert.circleFillColor = UIColor.buttonprimary

                                                alert.autoHideTime = 2.0
                                                alert.show()

                                                
                                    }
        }
    }
    
    func callExchangeTotalChipshowApi(currency:String,assets:String,rewards:String) {
        
        var params = [String:Any]()
        
        if currency == "TCT" {

         params = ["userid":customer_id,"currency":currency,"assets":assets,"rewards":rewards]
            
        }
        else
        {
            params = ["userid":customer_id,"currency":currency,"assets":assets]
        }
        
                                print(params)
        
                                APIManager.shared.ExchangeChipShow(params: params as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")

                                    if response.count > 0 {
                                        
                                         let id = response["message"].stringValue
                                        
                                         
                                        if id != "" {
                                            
                                            Common.checkRemoveCdAlert()

                                            let alert = CDAlertView(title: AppName, message: id, type: .error)
                                            alert.circleFillColor = UIColor.buttonprimary

                                            alert.autoHideTime = 2.0
                                            alert.show()

                                            return
                                        }
                                        
                                        
                                    
                                        if self.selectedValLbl.text == "TCT" {
                                            
                                            self.tctCoinBaseView.isHidden = false
                                            self.otherCoinBaseView.isHidden = true
                                            
                                            if "\(response["assets_balance"].stringValue)" == "" {
                                                self.tctBalValLbl.text = "0.0"
                                            }
                                            else
                                            {
                                            self.tctBalValLbl.text = "\(response["assets_balance"].stringValue)"
                                            }
                                            
                                            if "\(response["total_digital_chips"].stringValue)" == "" {
                                                self.tctToalDigValLbl.text = "0.0"
                                            }
                                            else
                                            {
                                            self.tctToalDigValLbl.text = "\(response["total_digital_chips"].stringValue)"
                                            }
   
                                        }
                                        else
                                        {
                                            self.tctCoinBaseView.isHidden = true
                                            self.otherCoinBaseView.isHidden = false
                                            if "\(response["balance"].stringValue)" == "" {
                                                self.balanceValLbl.text = "0.0"
                                            }
                                            else
                                            {
                                            self.balanceValLbl.text = String(describing: Double("\(response["balance"].stringValue)")!)
                                            }
                                            
                                            if "\(response["total_digital_chips"].stringValue)" == "" {
                                                self.totalDigValLbl.text = "0.0"
                                            }
                                            else
                                            {
                                            self.totalDigValLbl.text = String(describing: Double(response["total_digital_chips"].stringValue)!)
                                            }
                                            
                                            
                                            
                                        }
                        
                                    }
                                            else {
                                                Common.checkRemoveCdAlert()

                                                let alert = CDAlertView(title: AppName, message: "EXCHANGECHIPS", type: .error)
                                                alert.circleFillColor = UIColor.buttonprimary

                                                alert.autoHideTime = 2.0
                                                alert.show()

                                                
                                    }
        }
    }
    
    func callExchangeChipsApi(currency:String) {
            //"DEMOPUSH"
            var params = [String:Any]()
            if self.selectedValLbl.text == "TCT" {

                
                var reward = "0"
                var asset = "0"

                if rewardTxtFld.text! == "" && assetsTxtFld.text! == "" {
                       return
                   }
                
                else
                {
                    if rewardTxtFld.text != "" || assetsTxtFld.text != ""
                    {
                        
                        if rewardTxtFld.text != ""
                        {
                            reward = rewardTxtFld.text!
                        }
                        
                        if assetsTxtFld.text != ""
                        {
                            asset = assetsTxtFld.text!
                        }

                    }
                }

                
                
                
                
                params = ["userid":customer_id,"currency":self.selCurrId,"rewards":reward,"assets":asset]
                
            }
            else
            {
                params = ["userid":customer_id,"currency":self.selCurrId,"rewards":"0","assets":self.btcTxtFld.text!]
            }
            
            
                                    print(params)
            
                                    APIManager.shared.ExchangeChips(params: params as [String : AnyObject]) { (response) in
                                        print("response params1 \(response)")
                                        print("CHeckparams")
                                        
                                        let status = response["status"].stringValue
                                        let msg = response["msg"].stringValue

                                        if status == "success"
                                        {

                                            
                                            Common.checkRemoveCdAlert()
                                                    let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                            alert.circleFillColor = UIColor.buttonprimary
                                            let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                    alert.add(action: doneAction)
                                                    alert.show() { (alert) in
                                                        NotificationCenter.default.post(name: Notification.Name("chipexchange"), object: nil)
                                                         self.dismiss(animated: false, completion: nil)


                                                    }
                                            
                                            
//                                            let alert = CDAlertView(title: AppName, message: msg, type: .success)
//                                            alert.circleFillColor = UIColor.buttonprimary
//
//                                            alert.autoHideTime = 1.0
//
//                                            alert.show { (alert) in
//                                                NotificationCenter.default.post(name: Notification.Name("chipexchange"), object: nil)
//                                                 self.dismiss(animated: false, completion: nil)
//
//                                            }
                                            //alert.show()

                                            //NotificationCenter.default.post(name: Notification.Name("chipexchange"), object: nil)
                                           // self.dismiss(animated: false, completion: nil)

                                            

                                            
    //                                        let referral_code = response["referral_code"].dictionary
    //                                        self.referalCode = referral_code!["referral_code"]!.stringValue
                            
                                        }
                                                else {
                                                 
                                            let msg = response["msg"].stringValue

                                                    let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                                    alert.circleFillColor = UIColor.buttonprimary

                                                    alert.autoHideTime = 2.0
                                                    alert.show()


                                        }
            }
        }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method

    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        
                if textField == btcTxtFld {
            
                    self.callExchangeTotalChipshowApi(currency: selCurrId, assets: textField.text!, rewards: "")
        }
        else
        {
            if assetsTxtFld.text == "" {
                
                self.callExchangeTotalChipshowApi(currency: selCurrId, assets: "0", rewards: textField.text!)
            }
            if rewardTxtFld.text == "" {
                
                self.callExchangeTotalChipshowApi(currency: selCurrId, assets: textField.text!, rewards: "0")
            }
            
            if rewardTxtFld.text != "" && assetsTxtFld.text != ""{
                
                if textField == rewardTxtFld {
                    
                    self.callExchangeTotalChipshowApi(currency: selCurrId, assets: self.assetsTxtFld.text!, rewards: textField.text!)
                }
                else if textField == assetsTxtFld {
                    
                    self.callExchangeTotalChipshowApi(currency: selCurrId, assets: textField.text!, rewards: self.rewardTxtFld.text!)
                }
            }
  
        }

        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let assVal = String(describing: textField.text!) + string
        
        if textField == btcTxtFld {
            
            self.callExchangeTotalChipshowApi(currency: selCurrId, assets: assVal, rewards: "")
        }
        else
        {
            if assetsTxtFld.text == "" {
                
                self.callExchangeTotalChipshowApi(currency: selCurrId, assets: "0", rewards: assVal)
            }
            if rewardTxtFld.text == "" {
                
                self.callExchangeTotalChipshowApi(currency: selCurrId, assets: assVal, rewards: "0")
            }
            
            if rewardTxtFld.text != "" && assetsTxtFld.text != ""{
                
                if textField == rewardTxtFld {
                    
                    self.callExchangeTotalChipshowApi(currency: selCurrId, assets: self.assetsTxtFld.text!, rewards: assVal)
                }
                else if textField == assetsTxtFld {
                    
                    self.callExchangeTotalChipshowApi(currency: selCurrId, assets: assVal, rewards: self.rewardTxtFld.text!)
                }
                
                
            }
        
        
            
        }
        
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
      textField.resignFirstResponder()

        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
