//
//  TransactionsViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 26/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import SwiftyJSON
import DropDown
class TransactionsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var transactionTitleLbl: UILabel!
    @IBOutlet weak var norecordLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var categoryBtn: UIButton!
    @IBOutlet weak var typeBtn: UIButton!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dateBtn: UIButton!
    @IBOutlet weak var transactionTblView: UITableView!
    @IBOutlet weak var catbaseview: UIView!
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var tybaseview: UIView!
    var searchDatas = [JSON]()
    lazy var lang = Language.default.object

    var categoryDatas = [String]()
    
    var types = [String]()
    
    let dropDown = DropDown()

    let dropDown1 = DropDown()

    
    var toolBar = UIToolbar()
    var datePicker  = UIDatePicker()

    
    @IBOutlet weak var bottomview: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        if fromtranscation == "home"
        {
            backBtn.isHidden = false
            backImg.isHidden = false
        }
        else
        {            backBtn.isHidden = true
                     backImg.isHidden = true

            
        }
        transactionTitleLbl.text = lang.transactions
        categoryLbl.text = lang.Category
        typeLbl.text = lang.type
        dateLbl.text = lang.Date
        
        transactionTblView.delegate = self
        transactionTblView.dataSource = self
        self.transactionTblView.tableFooterView = UIView()

        
        transactionTblView.rowHeight = UITableView.automaticDimension
        transactionTblView.estimatedRowHeight = 100.0

        
viewtransactions()
    
        loadTransaction()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        fromtranscation = ""
    }
    
    func viewtransactions()
    {
        let params1 = ["":""]
                               
        
        print(params1)
        
                                APIManager.shared.transaction_filter_view(params: params1 as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    
                                    
                                    
                                    let filters = response["filters"]["category"].arrayValue
                                    
                                    let types = response["filters"]["types"].arrayValue

                                    if filters.count > 0
                                    {
                                    for i in 0...filters.count - 1
                                    {
                                        let name = filters[i].stringValue.capitalizingFirstLetter()
                                        self.categoryDatas.append(name)
                                    }
                                    }
                                    
                                    if types.count > 0
                                    {
                                    
                                    for i in 0...types.count - 1
                                    {
                                        let name = types[i].stringValue
                                        self.types.append(name)
                                    }

                                    }
                                    print("categoryDatas \(self.categoryDatas)")
                                    print("types \(self.types)")

                                    
                                    

                                    
        }
    }
    
    
    func loadTransaction()
    {
        
        let params1 = ["userid":customer_id]
        
        
        
        print(params1)
        
                                APIManager.shared.transactions(params: params1 as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    
                                 
                                    self.searchDatas = response.arrayValue

                                    
                                    if self.searchDatas.count > 0
                                    {
                                        
                                        self.transactionTblView.isHidden = false
                                        self.norecordLbl.isHidden = true

                                        self.transactionTblView.reloadData()


                                    }
                                    else
                                    {
                                        self.transactionTblView.isHidden = true
                                        self.norecordLbl.isHidden = false

                                    }

                                    

                                    
        }
    }
    
    
    @IBAction func backact(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func categoryBtn(_ sender: Any) {
        
        self.dropDown.dataSource = self.categoryDatas
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            self.categoryLbl.text = item
            self.loadData()

            self.dropDown.hide()

        }
        
        dropDown.anchorView = catbaseview
        dropDown.borderColor = UIColor.black
        dropDown.backgroundColor = UIColor.white
        dropDown.borderLineWidth = 1.0
        
        dropDown.show()
        
    }
    
    @IBAction func dateBtn(_ sender: UIButton) {
       
        
        
        
        DatePickerDialog().show("Select Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", maximumDate: Date(), datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                
                

                formatter.locale = Locale(identifier: "en_US_POSIX")



                self.dateLbl.text = formatter.string(from: dt)
                
                self.loadData()
            }
        }
        
        
        }


        
    
    @IBAction func typeBtn(_ sender: Any) {
        
        self.dropDown1.dataSource = self.types
        self.dropDown1.selectionAction = { [unowned self] (index: Int, item: String) in
            
            self.typeLbl.text = item
            self.loadData()
            self.dropDown.hide()
        }
        
        dropDown1.anchorView = tybaseview
        dropDown1.borderColor = UIColor.black
        dropDown1.backgroundColor = UIColor.white
        dropDown1.borderLineWidth = 1.0
        
        dropDown1.show()

    }
    
    
    
    func loadData()
    {
        
        var params = ["userid":customer_id]
        
        if categoryLbl.text != lang.Category && typeLbl.text == lang.type && dateLbl.text == lang.Date
        {
            params["category"] = categoryLbl.text!
        }
            
        else  if categoryLbl.text != lang.Category && typeLbl.text != lang.type && dateLbl.text == lang.Date
            {
                params["category"] = categoryLbl.text!
                params["type"] = typeLbl.text!

            }
            
        else  if categoryLbl.text != lang.Category && typeLbl.text == lang.type && dateLbl.text != lang.Date
              {
                  params["category"] = categoryLbl.text!
                  params["date"] = dateLbl.text!

              }

            

        
        else if categoryLbl.text == lang.Category && typeLbl.text != lang.type && dateLbl.text == lang.Date

        {
            params["type"] = typeLbl.text!

        }
            
            
        else if categoryLbl.text == lang.Category && typeLbl.text != lang.type && dateLbl.text != lang.Date

            {
                params["type"] = typeLbl.text!
                params["date"] = dateLbl.text!


            }
            
            
            else if categoryLbl.text != lang.Category && typeLbl.text != lang.type && dateLbl.text == lang.Date

            {
                params["category"] = categoryLbl.text!
                params["type"] = typeLbl.text!

            }


        
        else if categoryLbl.text == lang.Category && typeLbl.text == lang.type && dateLbl.text != lang.Date

        {
            params["date"] = dateLbl.text!

        }
        
        else if categoryLbl.text != lang.Category && typeLbl.text == lang.type && dateLbl.text != lang.Date

        {
            params["date"] = dateLbl.text!
            params["category"] = categoryLbl.text!

        }

        else if categoryLbl.text == lang.Category && typeLbl.text != lang.type && dateLbl.text != lang.Date

        {
            params["date"] = dateLbl.text!
            params["type"] = typeLbl.text!

        }


        
        
       else if categoryLbl.text != lang.Category && typeLbl.text != lang.type && dateLbl.text != lang.Date
        {
            
            
            
            params["date"] = dateLbl.text!
            params["type"] = typeLbl.text!
            params["category"] = categoryLbl.text!

        }

//            let params1 = ["userid":customer_id,
//                           "date":dateLbl.text!,
//                           "category":categoryLbl.text!,
//                           "type":typeLbl.text!]
                   
            
            
            
            print(params)
            
                                    APIManager.shared.transaction_filter(params: params as [String : AnyObject]) { (response) in
                                        print("response params1 \(response)")
                                        
                                     
                                        let msg = response["transaction_filter"]["status"].stringValue

                                        
                                        if msg == "failure"
                                        {
                                            
                                            self.transactionTblView.isHidden = true
                                            self.norecordLbl.isHidden = false

                                        }
                                        else
                                        {
                                            self.searchDatas = response["transaction_filter"].arrayValue
                                            self.transactionTblView.isHidden = false
                                            self.norecordLbl.isHidden = true

                                            self.transactionTblView.reloadData()
                                        }

                                        

                                        
            }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchDatas.count
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }



    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
               
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! transactionCell
        
        let dict = searchDatas[indexPath.row].dictionaryValue
        
        if let id = dict["category"]
        {
            
        let trans_type = dict["category"]!.stringValue
        
        print("trans_type \(trans_type)")
        if trans_type == "assets"
        {
            cell.titleLbbl.text = lang.assets
        }
        
        if trans_type == "rewards"
        {
            cell.titleLbbl.text = lang.rewards
        }

        
        
        }
        
        
       // cell.titleLbbl.text = trans_type?.replacingOccurrences(of: "_", with: " ")
        
        
        let langa = UserDefaults.standard.string(forKey: "lang")
        
        if langa == "zh" {
         
            cell.codeLbl.text = dict["date_zh"]?.stringValue

        }
        else{
            
            cell.codeLbl.text = dict["date"]?.stringValue

        }
        

        
       // cell.codeLbl.text = dict["date"]?.stringValue
        
        
        
        let amount = dict["amount"]?.stringValue
        
        if let cur = dict["currency_symbol"]
        {
            if amount != ""
            {
                cell.amountLbl.text = amount! + " " + dict["currency_symbol"]!.stringValue

            }
            else
            {
                cell.amountLbl.text = "0" + " " + dict["currency_symbol"]!.stringValue

            }

        }
        
        
        if let id = dict["type"]
        
        {
        let type = dict["type"]!.stringValue
        
        print("TypeVAlyue \(type)")
        if type == "Upgrade Package"
        {
            cell.typeLbl.text = lang.upgradePackage
        }
        
        if type == "Swap"
        {
            cell.typeLbl.text = lang.Swap

        }
        
        if type == "Exchange"
        {
            cell.typeLbl.text = lang.exchange
        }
        
        if type == "Topup"
        {
            cell.typeLbl.text = lang.topup

        }
        
        if type == "Personal Rewards"
        {
            cell.typeLbl.text = lang.personalRewards

        }
        
        
        if type == "Epin Creation"
        {
            cell.typeLbl.text = lang.EpinCreation

        }
        
        if type == "Transfer"
        {
            cell.typeLbl.text = lang.Transfer

        }
        
        if type == "Level Bonus"
        {
            cell.typeLbl.text = lang.LevelBonus

        }
        if type == "Team Bonus"
        {
            cell.typeLbl.text = lang.teamBonus

        }


    }



        
        
       // cell.typeLbl.text = dict["type"]?.stringValue.capitalizingFirstLetter()
        
        
        return cell

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class transactionCell : UITableViewCell
{
    
    @IBOutlet weak var codeLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var titleLbbl: UILabel!
    @IBOutlet weak var transactionImg: UIImageView!
}

