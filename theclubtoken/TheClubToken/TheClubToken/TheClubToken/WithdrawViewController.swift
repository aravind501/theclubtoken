//
//  WithdrawViewController.swift
//  TheClubToken
//
//  Created by trioangle on 24/07/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CDAlertView
import SwiftyJSON

class WithdrawViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")!
        
        let CheckBtn = cell.viewWithTag(1) as! UIButton
        let txtLbl = cell.viewWithTag(2) as! UILabel
        
        let dict = dataSourceArray[indexPath.row]
        
        txtLbl.text = dict["game_name"].stringValue
        
        if selectedIndex == indexPath.row {
            CheckBtn.setImage(UIImage(named: "radio-on-button"), for: .normal)
        }
        else
        {
            CheckBtn.setImage(UIImage(named: "empty"), for: .normal)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedIndex = indexPath.row
        
        let dict = dataSourceArray[indexPath.row]
        
        selectedSite = dict["game_url"].stringValue
        
        self.tablaViewOutlet.reloadData()
    }
    

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var chipsTxtFld: UITextField!
    @IBOutlet weak var tablaViewOutlet: UITableView!
    @IBOutlet weak var withDrawBtn: UIButton!
    @IBOutlet weak var digitalChipTitleLbl: UILabel!
    
    var dataSourceArray = [JSON]()
       // ["A11 BETS","All CASINO","A11 GAME"]
    var selectedIndex = 0
    var selectedSite = ""
    lazy var lang = Language.default.object
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLbl.text = lang.withdraw
        digitalChipTitleLbl.text = lang.digitalChips
        
        withDrawBtn.setTitle(lang.withdraw, for: .normal)
        
        chipsTxtFld.layer.cornerRadius = 10
        chipsTxtFld.layer.borderWidth = 1
        chipsTxtFld.layer.borderColor = UIColor.borderGold.cgColor

        tablaViewOutlet.delegate = self
        tablaViewOutlet.dataSource = self
        
        getDigital_chips_websiteslist()
        // Do any additional setup after loading the view.
    }
    
        func getDigital_chips_websiteslist()
        {
            
            let params1 = [":":":"]
            
            print(params1)
            
                                    APIManager.shared.digital_chips_websites() { (response) in
                                        print("response getcurrencylist \(response)")
                                        
                                     //   self.currencyDatas = response["currency_list"].arrayValue
                                        
                                        let dict = response["games"].arrayValue
                                        
                                        if dict.count > 0
                                        {
                                            
                                        self.selectedSite = dict.first!["game_url"].stringValue
                                            
                                            self.dataSourceArray  = dict
                                            
//                                        for i in 0...dict.count - 1
//                                        {
//                                            let dicts = dict[i].dictionaryValue
//                                            let name = dicts["url"]?.stringValue
//                                            self.dataSourceArray.append(name!)
//
//                                        }
                                        }

                                        self.tablaViewOutlet.reloadData()


            }
        }
    
    func calltransfer_chipsApi() {
        let params = ["userid":customer_id,"amount":String(describing: self.chipsTxtFld.text!),"website":selectedSite]
                                print(params)
        
                                APIManager.shared.transfer_chips(params: params as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                                    if let dict = response["transfer_chips"].dictionary {

                                        
                                        let msg = dict["message"]?.stringValue
                                        let status = dict["status"]?.stringValue
                                        
                                        
                                        if status == "success"
                                        {
                                            Common.checkRemoveCdAlert()
                                                    let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                            alert.circleFillColor = UIColor.buttonprimary
                                            let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                    alert.add(action: doneAction)
                                                    alert.show() { (alert) in

                                                        NotificationCenter.default.post(name: Notification.Name("topup"), object: nil)
                                                        self.dismiss(animated: false, completion: nil)

                                                    }
                                            
                                            
                                        }
                                        
                                        else
                                        {
                                            
                                            Common.checkRemoveCdAlert()
                                                    let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                            alert.circleFillColor = UIColor.buttonprimary
                                            let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                    alert.add(action: doneAction)
                                                    alert.show() { (alert) in


                                                    }
                                            
                                        }

                                    }
                                            else {
                                                Common.checkRemoveCdAlert()

                                                let alert = CDAlertView(title: AppName, message: "error", type: .error)
                                                alert.circleFillColor = UIColor.buttonprimary

                                                alert.autoHideTime = 2.0
                                                alert.show()

                                                
                                    }

        }
    }
    
    @IBAction func withDrawBtnAction(_ sender: Any) {
        
        if chipsTxtFld.text != "" {
            chipsTxtFld.resignFirstResponder()
            calltransfer_chipsApi()
            
        }
    }
    
    @IBAction func closebtnAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
