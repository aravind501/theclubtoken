//
//  AppDelegate.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 25/04/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SwiftyJSON
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate {

    var window: UIWindow?
    var screenTimer: Timer?
    var screenlock = Double()
    var currenctListArry = [JSON]()
    lazy var lang = Language.default.object
    var device_tokens = "12123"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        //UserDefaults.standard.set("zh", forKey:  "lang")
        FirebaseApp.configure()

        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self as! UNUserNotificationCenterDelegate
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        
        application.registerForRemoteNotifications()


        
        UIView.appearance().semanticContentAttribute = Language.default.getSemantic

      IQKeyboard()
                if let timer = UserDefaults.standard.string(forKey: "timer")
                {
                    print("screenlock \(timer)")
                    if timer == "onemin"
                    {
                       screenlock = 60
                    }
                    else if timer == "tenmin"
                    {
                        screenlock = 600

                    }
                    else if timer == "twentymin"
                    {
                        screenlock = 1200

                    }
                    
                    else if timer == "thirtymin"
                    {
                        screenlock = 1800

                    }
                    
                    
                    timercall()

                }
                
                else
                {
                    print("screenlock Not Set")

                    //screenlock = 60

                 //   UserDefaults.standard.set("onemin", forKey: "timer")

                }

        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("timerchange"), object: nil)


        return true
    }

    func timercall()
    {
        print("TimerCall \(screenlock)")
        screenTimer?.invalidate()
        screenTimer = Timer.scheduledTimer(timeInterval: screenlock, target: self, selector: #selector(update1), userInfo: nil, repeats: true)

    }
    
    func timerInvalidate()
    {
        print("timerInvalidate")
        screenTimer?.invalidate()

    }
   
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        print("TimerCall notification called")

            timercall()
    }

    
    @objc func update1 () {
        
        print("Screentimer calling")
        if topMostViewController().isKind(of: UserLoginViewController.self)
        {
            print("Screentimer UserLoginViewController")

        }
        else
        {
        let forgotVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "UserLoginViewController") as? UserLoginViewController
            self.topMostViewController().navigationController?.pushViewController(forgotVC!, animated: true)
        }
    }

}

    
    // MARK:- Google
    
extension AppDelegate {
    

    
    private func IQKeyboard() {
        

        IQKeyboardManager.shared.enable = true
    }
    

}


extension AppDelegate {
    
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Pass device token to auth
        // Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.sandbox)
        // Messaging.messaging().apnsToken = deviceToken
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                let tokens = result.token
                self.device_tokens = tokens
                device_token = tokens
                print("checkktokenns\(tokens)")
            }
        }
    }
    var applicationStateString: String {
        if UIApplication.shared.applicationState == .active {
            return "active"
        } else if UIApplication.shared.applicationState == .background {
            return "background"
        }else {
            return "inactive"
        }
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        NSLog("[RemoteNotification] didRefreshRegistrationToken: \(fcmToken)")
        self.device_tokens = fcmToken
        print("checkktokenns\(fcmToken)")
        
    }
    
    // iOS9, called when presenting notification in foreground
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        NSLog("[RemoteNotification] applicationState: \(applicationStateString) didReceiveRemoteNotification for iOS9: \(userInfo)")
        if UIApplication.shared.applicationState == .active {
            //TODO: Handle foreground notification
        } else {
            //TODO: Handle background notification
        }
    }
    
    
    
    
    
}


extension AppDelegate {
    
    
    
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification notification: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("Notification  :  ", notification)
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            content.sound = UNNotificationSound.default
        } else {
            // Fallback on earlier versions
        }
        
        completionHandler(.newData)
        
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("Error in Notification  \(error.localizedDescription)")
    }
    
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    // iOS10+, called when presenting notification in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        NSLog("[UserNotificationCenter] applicationState: \(applicationStateString) willPresentNotification: \(userInfo)")
        //TODO: Handle foreground notification
        completionHandler([.alert])
    }
    
    // iOS10+, called when received response (default open, dismiss or custom action) for a notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        NSLog("[UserNotificationCenter] applicationState: \(applicationStateString) didReceiveResponse: \(userInfo)")
        //TODO: Handle background notification
        completionHandler()
        
        if UIApplication.shared.applicationState == .active {
            //TODO: Handle foreground notification
        } else {
            //TODO: Handle background notification
        }
        
        // driverRequest(userInfo: userInfo)
        
    }
}

extension AppDelegate {

// MARK:- Register Push
private func registerPush(forApp application : UIApplication){
    if #available(iOS 10.0, *) {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options:[.alert, .sound]) { (granted, error) in
            
            if granted {
                DispatchQueue.main.async {
                    application.registerForRemoteNotifications()
                }
            }
        }
    } else {
        // Fallback on earlier versions
    }
    
}
}
