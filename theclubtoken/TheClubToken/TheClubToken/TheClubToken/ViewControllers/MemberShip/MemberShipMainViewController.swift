//
//  MemberShipMainViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 25/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class MemberShipMainViewController: UIViewController {

    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    lazy var lang = Language.default.object
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLbl.text = lang.Membership
        
        let controller = storyboard!.instantiateViewController(withIdentifier: "MembershipSwipeViewController")
        addChild(controller)
        controller.view.frame = CGRect(x: 0, y: 0, width: baseView.frame.width, height: baseView.frame.height)
        baseView.addSubview(controller.view)
        controller.didMove(toParent: self)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backact(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
