//
//  FeedbackViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 27/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import Cosmos
import CDAlertView
class FeedbackViewController: UIViewController,UITextViewDelegate{

    @IBOutlet weak var ratingview: CosmosView!
    @IBOutlet weak var messageTxtView: UITextView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    lazy var lang = Language.default.object
    override func viewDidLoad() {
        super.viewDidLoad()

        messageTxtView.text = lang.Message
        messageTxtView.delegate = self

        titleLbl.text = lang.Feedback
        messageTxtView.text = lang.Message
        submitBtn.setTitle(lang.Submit, for: .normal)
        // Do any additional setup after loading the view.
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == lang.Message {
            textView.text = ""
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == "" {
            textView.text = lang.Message
        }
    }
    
    @IBAction func backact(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func submitact(_ sender: Any) {
        
        if messageTxtView.text == lang.Message
        {
            self.view.make(toast: lang.Please_Enter_Your_Message)

        }
        
        else
        {
            let rating = String(format: "%f", self.ratingview.rating.rounded())

            
            
                let params2 = ["userid":customer_id,
                               "rating":rating,
                               "message":messageTxtView.text!]

             
                
                print(params2)
                
                APIManager.shared.feedback(params: params2 as [String : AnyObject]) { (response) in
                                            print("response params1 \(response)")
                                            print("CHeckparams")
                                            
                                            let status = response["feedback"]["status"].stringValue
                                            let msg = response["feedback"]["msg"].stringValue
                                            
                                            if status == "success"
                                            {
                                                 Common.checkRemoveCdAlert()
                                                        let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                                alert.circleFillColor = UIColor.buttonprimary
                                                let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                        alert.add(action: doneAction)
                                                        alert.show() { (alert) in

                                                            self.messageTxtView.text = self.lang.Message
                                                            self.ratingview.rating = 0.0
                                                        }
                                            }
                                            
                                            else
                                            {
                                                let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                                alert.circleFillColor = UIColor.buttonprimary

                                                alert.autoHideTime = 2.0
                                                alert.show()

                                            }

                }
                
                
                
            
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
