//
//  QRViewController.swift
//  TheClubToken
//
//  Created by trioangle on 28/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class QRViewController: UIViewController {

    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var qrImgView: UIImageView!
    @IBOutlet weak var closeImg: UIButton!
    
    var titleStr = String()
    var qrImgStr = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.closeImg.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysTemplate),
                                      for: .normal)
        self.closeImg.tintColor = UIColor.white
        titleLbl.text = titleStr
        let img = qrImgStr
        qrImgView.sd_setImage(with: URL(string: img), completed: nil)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
