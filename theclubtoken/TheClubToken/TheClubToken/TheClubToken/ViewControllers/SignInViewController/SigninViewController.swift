//
//  SigninViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 26/04/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CDAlertView
import LabelSwitch
import CountryPickerView
import libPhoneNumber_iOS

class SigninViewController: UIViewController,LabelSwitchDelegate {

    @IBOutlet weak var welcomeLbl: UILabel!
    @IBOutlet weak var emailTxtffld: HoshiTextField!
    @IBOutlet weak var verificationTxtfld: HoshiTextField!
    @IBOutlet weak var signupLbl: UILabel!
    @IBOutlet weak var mobileTxtffld: UITextField!

    @IBOutlet weak var bottomview: UIView!

    var myString = "Don't have an account?Signup now"
    @IBOutlet weak var countryImg: UIImageView!
    @IBOutlet weak var countryCodeLbl: UILabel!
    
    @IBOutlet weak var sendVeriCodeBtn: UIButton!
    @IBOutlet weak var SignInBtn: UIButton!
    
    let countryPickerView = CountryPickerView()

    var verificationCode = ""
    lazy var lang = Language.default.object

    var selected = "Email"
    
    @IBOutlet weak var phoneview: UIView!
    @IBOutlet weak var lableswitch: LabelSwitch!
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomview.clipsToBounds = true
        bottomview.layer.cornerRadius = 50

        if #available(iOS 11.0, *) {
            bottomview.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        setDesigns()
        loadswitch()
        
        countryPickerView.delegate = self
        countryPickerView.dataSource = self

        countryImg.image = #imageLiteral(resourceName: "india")
        
        welcomeLbl.text = lang.WelcometoTheClubToken
        mobileTxtffld.placeholder = lang.MobileNumber
        emailTxtffld.placeholder = lang.YourEmail
        verificationTxtfld.placeholder = lang.EnterVerificationCode
        
        sendVeriCodeBtn.setTitle(lang.SendVerificationcode, for: .normal)
        SignInBtn.setTitle(lang.SignIn, for: .normal)
       // signupLbl.text = lang.Donthaveanaccountyet

        
        // Do any additional setup after loading the view.
    }
    @IBAction func countryBtnAct(_ sender: Any) {
        
        countryPickerView.showCountriesList(from: self)

    }
    
    func sendverificationcodeMobile()
    
        {
         
         let mob = self.mobileTxtffld.text!
         let country = self.countryCodeLbl.text!

         var isValidNumber: Bool! = false

         let phoneUtil = NBPhoneNumberUtil.sharedInstance()
         
         print("countrycountry \(country)")
         
         
         if country.contains("+") {
             
             let newcode = country.replacingOccurrences(of: "+", with: "")
             
             let ioscode = phoneUtil?.getRegionCode(forCountryCode: NSNumber(value:Int(newcode)!))
             
             var phoneNumber: NBPhoneNumber!
             
             do {
                 phoneNumber = try phoneUtil!.parse(mob, defaultRegion: ioscode)
             }
             catch let error as NSError {
                 print(error.localizedDescription)
             }
             
             isValidNumber = phoneUtil?.isValidNumber(phoneNumber)
             
             print("code \(newcode) \(phoneNumber)")
             
         }
         
         print("isValidNumber \(isValidNumber)")
         

         
         
         if mobileTxtffld.text == ""
         {
            self.view.make(toast: lang.Please_Enter_Mobile_Number)
         }
        
         else if !isValidNumber {
            self.view.make(toast: lang.Please_Enter_Valid_Mobile_Number)

         }

         else
         {
             
                         let params1 = ["country_code":countryCodeLbl.text,
                                        "phone":mobileTxtffld.text!]
                         
                                     print(params1)
                                     APIManager.shared.phone_otp_signin(params: params1 as [String : AnyObject]) { (response) in
                                         print("response params1 \(response)")
                                         print("CHeckparams")
                                         

                                             let msg = response["message"].stringValue
                                             let status = response["status"].stringValue

                                                 
                                                 if status == "success" {
                                                     
                                                     self.verificationCode = response["otpcode"].stringValue
                                                     Common.checkRemoveCdAlert()
                                                            let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                                    alert.circleFillColor = UIColor.buttonprimary
                                                    let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                            alert.add(action: doneAction)
                                                            alert.show() { (alert) in

                                                            }

                                                    
                                                 }
                                                     
                                                 else {
                                                     self.verificationCode = ""
                                                     let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                                     alert.circleFillColor = UIColor.buttonprimary

                                                     alert.autoHideTime = 2.0
                                                     alert.show()

                                                     
                                         }
                                                 
             }

         }
         
        
    }
    
    
    func sendverificationEmail()
    {
        
        
        if emailTxtffld.text == ""
        {
            self.view.make(toast: lang.Please_Enter_Email_Id)
        }
        
        else if emailTxtffld.text?.isEmail == false
        {
            self.view.make(toast: lang.Please_Enter_Valid_Email_Id)

        }
        
        else
        {
                         let params1 = ["email":emailTxtffld.text!]
                         
                                     print(params1)
                                     APIManager.shared.email_otp_signin(params: params1 as [String : AnyObject]) { (response) in
                                         print("response params1 \(response)")
                                         print("CHeckparams")
                                         

                                             let msg = response["message"].stringValue
                                             let status = response["status"].stringValue

                                                 
                                                 if status == "success" {
                                                     
                                                     self.verificationCode = response["otpcode"].stringValue
                                                     Common.checkRemoveCdAlert()
                                                            let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                                    alert.circleFillColor = UIColor.buttonprimary
                                                    let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                            alert.add(action: doneAction)
                                                            alert.show() { (alert) in

                                                            }

                                                    
                                                 }
                                                     
                                                 else {
                                                     self.verificationCode = ""
                                                     let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                                     alert.circleFillColor = UIColor.buttonprimary

                                                     alert.autoHideTime = 2.0
                                                     alert.show()

                                                     
                                         }
                                                 
             }
        }
    }
    
    
    func loadswitch()
    {
        lableswitch.delegate = self
        lableswitch.curState = .L
        lableswitch.circleShadow = false
        lableswitch.fullSizeTapEnabled = true
        

    }
    
    
    func switchChangToState(sender: LabelSwitch) {
        switch sender.curState {
        case .L:
            print("left")
            
            self.emailTxtffld.isHidden = false
            self.phoneview.isHidden = true
            selected = "Email"
            self.verificationTxtfld.text = ""
        case .R: print("right")
            self.emailTxtffld.isHidden = true
            self.phoneview.isHidden = false
            selected = "Mobile"
            self.verificationTxtfld.text = ""


        }
    }
    
    override func viewDidLayoutSubviews() {
        
        if #available(iOS 11.0, *){
            
        }else{
           let rectShape = CAShapeLayer()
           rectShape.bounds = view.frame
           rectShape.position = view.center
           rectShape.path = UIBezierPath(roundedRect: view.bounds,    byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 50 , height: 50)).cgPath
           bottomview.layer.mask = rectShape
    }
    
    }
    
    
    func setDesigns(){
        
        self.emailTxtffld.borderActiveColor = .textcolor
        self.emailTxtffld.borderInactiveColor = .lightGray
        self.emailTxtffld.placeholderColor = .textcolor
        self.emailTxtffld.textColor = .textcolor
        self.emailTxtffld.delegate = self
        
        self.verificationTxtfld.borderActiveColor = .textcolor
        self.verificationTxtfld.borderInactiveColor = .lightGray
        self.verificationTxtfld.placeholderColor = .textcolor
        self.verificationTxtfld.textColor = .textcolor
        self.verificationTxtfld.delegate = self
       
        
        myString = "\(lang.Donthaveanaccountyet)\(lang.Signupnow)"
        
        let SignupRange = (myString as NSString).range(of: lang.Signupnow)
        
        let myMutableString = NSMutableAttributedString(string: myString, attributes: [NSAttributedString.Key.font :UIFont(name: "Montserrat-Regular", size: 17.0)!])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.buttonprimary, range: SignupRange)
        signupLbl.isUserInteractionEnabled = true

        signupLbl.attributedText = myMutableString
        signupLbl.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(tapLabel(gesture:))))

    }
    
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
       let termsRange = (myString as NSString).range(of: lang.Signupnow)
       // comment for now
       //let privacyRange = (text as NSString).range(of: "Privacy Policy")

       if gesture.didTapAttributedTextInLabel(label: signupLbl, inRange: termsRange) {

        self.push(id: "RegisterViewController", animation: true, fromSB: Login)
       }  else {
           print("Tapped none")
       }
    }

  
    @IBAction func backact(_ sender: Any) {
        
        self.popLeft()
        
    }
    
    
    
    @IBAction func sendcodeact(_ sender: Any) {
        
        if selected == "Email"
        {
            sendverificationEmail()
        }
        else
        {
sendverificationcodeMobile()
        }
        
    }
    
    
    
    
    @IBAction func signinact(_ sender: Any) {
        
        
        if selected == "Email"
        {
        
        if emailTxtffld.text == ""
        {
            self.view.make(toast: lang.Please_Enter_Email_Id)
        }
        
        else if verificationTxtfld.text == ""
        {
            self.view.make(toast: lang.Please_Enter_Verification_Code)
        }
        
        else
        {
            let params1 = ["email":emailTxtffld.text!,
                           "verifycode":verificationTxtfld.text!,
                           "device_id":device_token]
                        
                                    print(params1)
            
                                    APIManager.shared.login_email(params: params1 as [String : AnyObject]) { (response) in
                                        print("response params1 \(response)")
                                        print("CHeckparams")
                                        

                                            let msg = response["msg"].stringValue
                                            let status = response["status"].stringValue

                                                
                                                if status == "success" {
                                                    
                                                    customer_id = response["userid"].stringValue
                               
                                                    UserDefaults.standard.set(customer_id, forKey: "user_id")


                                                            if let pin_id = UserDefaults.standard.string(forKey: "pin")

                                                            {
                                                                print("Pin Enabled Already\(pin_id)")
                                                            
                                                                UserDefaults.standard.set(true, forKey: "user_isLogin")

                                                                let vc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainTabbarViewController") as? MainTabbarViewController
                                                                self.navigationController?.pushViewController(vc!, animated: true)

                                                            }
                                                            
                                                            else
                                                            {
                                                                print("Pin Not Enabled")
                                                                pinfrom = "register"
                                                                self.push(id: "PINCodeSetViewController", animation: true, fromSB: Login)


                                                                }
                                                                
                                                                

                                                           

                                                   
                                                }
                                                    
                                                else {
                                                    
                                                    let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                                    alert.circleFillColor = UIColor.buttonprimary

                                                    alert.autoHideTime = 2.0
                                                    alert.show()

                                                    
                                        }
                                                
            }
        }
            
        }
        
        
        else
        {
            
             let mob = self.mobileTxtffld.text!
             let country = self.countryCodeLbl.text!

             var isValidNumber: Bool! = false

             let phoneUtil = NBPhoneNumberUtil.sharedInstance()
             
             print("countrycountry \(country)")
             
             
             if country.contains("+") {
                 
                 let newcode = country.replacingOccurrences(of: "+", with: "")
                 
                 let ioscode = phoneUtil?.getRegionCode(forCountryCode: NSNumber(value:Int(newcode)!))
                 
                 var phoneNumber: NBPhoneNumber!
                 
                 do {
                     phoneNumber = try phoneUtil!.parse(mob, defaultRegion: ioscode)
                 }
                 catch let error as NSError {
                     print(error.localizedDescription)
                 }
                 
                 isValidNumber = phoneUtil?.isValidNumber(phoneNumber)
                 
                 print("code \(newcode) \(phoneNumber)")
                 
             }
             
             print("isValidNumber \(isValidNumber)")
             

             
             
             if mobileTxtffld.text == ""
             {
                self.view.make(toast: lang.Please_Enter_Mobile_Number)
             }
            
             else if !isValidNumber {
                self.view.make(toast: lang.Please_Enter_Valid_Mobile_Number)

             }

                else if verificationTxtfld.text == ""
                {
                    self.view.make(toast: lang.Please_Enter_Verification_Code)
                }

             else
             {
                 let params1 = ["phone":mobileTxtffld.text!,
                                "verifycode":verificationTxtfld.text!,
                                "device_id":device_token]
                             
                                         print(params1)
                 
                                         APIManager.shared.login_phone(params: params1 as [String : AnyObject]) { (response) in
                                             print("response params1 \(response)")
                                             print("CHeckparams")
                                             

                                                 let msg = response["msg"].stringValue
                                                 let status = response["status"].stringValue

                                                     
                                                     if status == "success" {
                                                         
                                                         customer_id = response["userid"].stringValue
                                    
                                                         UserDefaults.standard.set(customer_id, forKey: "user_id")


                                                                 if let pin_id = UserDefaults.standard.string(forKey: "pin")

                                                                 {
                                                                     print("Pin Enabled Already\(pin_id)")
                                                                 
                                                                     UserDefaults.standard.set(true, forKey: "user_isLogin")

                                                                     let vc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainTabbarViewController") as? MainTabbarViewController
                                                                     self.navigationController?.pushViewController(vc!, animated: true)

                                                                 }
                                                                 
                                                                 else
                                                                 {
                                                                     print("Pin Not Enabled")
                                                                     pinfrom = "register"
                                                                     self.push(id: "PINCodeSetViewController", animation: true, fromSB: Login)


                                                                     }
                                                                     
                                                                     

                                                                

                                                        
                                                     }
                                                         
                                                     else {
                                                         
                                                         let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                                         alert.circleFillColor = UIColor.buttonprimary

                                                         alert.autoHideTime = 2.0
                                                         alert.show()

                                                         
                                             }
                                                     
                 }
             }
            
            
        }

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SigninViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == emailTxtffld
        {
        emailTxtffld.placeholder = lang.YourEmail
        }
        
        else if textField == verificationTxtfld
        {
            verificationTxtfld.placeholder = lang.EnterVerificationCode
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.count == 0 {
            
            
            if textField == emailTxtffld
                   {
                    emailTxtffld.placeholder = lang.YourEmail
                    self.emailTxtffld.borderInactiveColor = .lightGray

                   }
                   
                   else if textField == verificationTxtfld
                   {
                   verificationTxtfld.placeholder = lang.EnterVerificationCode
                    self.verificationTxtfld.borderInactiveColor = .lightGray

                   }
                   

        }
    }
    
}
extension UITapGestureRecognizer {

    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)

        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)

        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize

        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        //let textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                              //(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)

        //let locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
                                                        // locationOfTouchInLabel.y - textContainerOffset.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }

}
extension SigninViewController: CountryPickerViewDelegate, CountryPickerViewDataSource {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        // Only countryPickerInternal has it's delegate set
        let message = "Name: \(country.name) \nCode: \(country.code) \nPhone: \(country.phoneCode)"
        
        print("Selected Country \(message)")
        self.countryCodeLbl.text = country.phoneCode
       self.countryImg.image = country.flag
    
    }
}
