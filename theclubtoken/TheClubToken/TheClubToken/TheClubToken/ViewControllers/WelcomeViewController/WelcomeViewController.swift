//
//  WelcomeViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 25/04/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    @IBOutlet weak var bottomview: UIView!
    
    @IBOutlet weak var welcome: UILabel!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var openAccount: UIButton!
    lazy var lang = Language.default.object
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomview.clipsToBounds = true
        bottomview.layer.cornerRadius = 50
        if #available(iOS 11.0, *) {
            bottomview.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
        
        welcome.text = lang.Welcometothe
        signInBtn.setTitle(lang.SignIn, for: .normal)
        openAccount.setTitle(lang.OpenNewAccount, for: .normal)
    }
    
    override func viewDidLayoutSubviews() {
        
        if #available(iOS 11.0, *){
            
        }else{
           let rectShape = CAShapeLayer()
           rectShape.bounds = view.frame
           rectShape.position = view.center
           rectShape.path = UIBezierPath(roundedRect: view.bounds,    byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 50 , height: 50)).cgPath
           bottomview.layer.mask = rectShape
    }
    
    }
    
    
    @IBAction func signinact(_ sender: Any) {
      

    self.push(id: "SigninViewController", animation: true, fromSB: Login)
    }
    
    @IBAction func opennewaccountact(_ sender: Any) {
        
        self.push(id: "RegisterViewController", animation: true, fromSB: Login)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

