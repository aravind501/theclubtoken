//
//  UserLoginViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 27/04/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import BiometricAuthentication
import CDAlertView
class UserLoginViewController: UIViewController {

    @IBOutlet weak var usernamelbl: UILabel!
    @IBOutlet weak var signuplbl: UILabel!
    @IBOutlet weak var signInwithLbl: UIButton!
    
    var myString = "Don't have an account?Signup now"
    lazy var lang = Language.default.object

    var type = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
         setDesigns()
        
        signInwithLbl.setTitle(lang.Signinwithanotheraccount, for: .normal)
        
        if let f_id = UserDefaults.standard.string(forKey: "username")

        {
            usernamelbl.text = "Hello," + " " + f_id
        }
        
        else
        {
            usernamelbl.text = ""

        }
        // Do any additional setup after loading the view.
    }
    
    func setDesigns(){
        
        myString = "\(lang.Donthaveanaccountyet)\(lang.Signupnow)"
        
        let SignupRange = (myString as NSString).range(of: lang.Signupnow)
        
        let myMutableString = NSMutableAttributedString(string: myString, attributes: [NSAttributedString.Key.font :UIFont(name: "Montserrat-Regular", size: 17.0)!])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.buttonprimary, range: SignupRange)
        signuplbl.isUserInteractionEnabled = true

        signuplbl.attributedText = myMutableString
        signuplbl.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(tapLabel(gesture:))))

    }
    
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
     
        let termsRange = (myString as NSString).range(of: lang.Signupnow)
       // comment for now
       //let privacyRange = (text as NSString).range(of: "Privacy Policy")

       if gesture.didTapAttributedTextInLabel(label: signuplbl, inRange: termsRange) {

        self.push(id: "RegisterViewController", animation: true, fromSB: Login)
        
       }  else {
           print("Tapped none")
       }
    }
    
    @IBAction func signinotheraccount(_ sender: Any) {
        
        self.push(id: "SigninViewController", animation: true, fromSB: Login)
    }
    @IBAction func fingerbtn(_ sender: Any) {
       
        if  BioMetricAuthenticator.shared.touchIDAvailable() {
            
            if let f_id = UserDefaults.standard.string(forKey: "finger")

            {
                print("Finger Enabled Already")
                checkBiometric()

            }
            
            else
            {
                print("Finger Not Enabled")

                
                 Common.checkRemoveCdAlert()
                
                let alert = CDAlertView(title: AppName, message: lang.touchidnotenabled, type: .warning)
                alert.circleFillColor = UIColor.buttonprimary
                let yesAction = CDAlertViewAction(title: lang.OK, font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: { (yesAction) -> Bool in
                     
                     print("ok")
                     
                     touchIDfrom = "userlogin"
                     self.push(id: "TouchIDSetViewController", animation: true, fromSB: Login)



                     return true
                 })
                 
                 alert.add(action: yesAction)
                 
                let NoAction = CDAlertViewAction(title: lang.Cancel, font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: { (yesAction) -> Bool in
                     
                     print("no no no")
                     
                     return true
                 })
                 
                 alert.add(action: NoAction)
                 
                 
                 alert.show()

                
//                self.showAlertWithYesNo(title: appNameShort, message: "TouchID Not Enabled. Do you want to enable?") { (true) in
//                    touchIDfrom = "userlogin"
//                    self.push(id: "TouchIDSetViewController", animation: true, fromSB: Login)
//
//                }
                
            }

        }
        
        else
        {
            
             Common.checkRemoveCdAlert()
            let alert = CDAlertView(title: AppName, message: lang.sorryunabletoauthenticatetouchid, type: .error)
            alert.circleFillColor = UIColor.buttonprimary
            let doneAction = CDAlertViewAction(title: lang.OK, font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                    alert.add(action: doneAction)
                    alert.show() { (alert) in

                    }

        }

    }
    @IBAction func pinlogin(_ sender: Any) {
       
        pinfrom = "login"

        self.push(id: "PINCodeSetViewController", animation: true, fromSB: Login)
    }
    
    @IBAction func facelogin(_ sender: Any) {
        
        if  BioMetricAuthenticator.shared.faceIDAvailable() {
            
            if let f_id = UserDefaults.standard.string(forKey: "face")

            {
                print("Face Enabled Already")
                checkBiometric()

            }
            
            else
            {
                print("Face Not Enabled")

                
                 Common.checkRemoveCdAlert()
                
                let alert = CDAlertView(title: AppName, message: lang.faceidnotenabled, type: .warning)
                alert.circleFillColor = UIColor.buttonprimary

                let yesAction = CDAlertViewAction(title: lang.OK, font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: { (yesAction) -> Bool in
                     
                     print("ok")
                     
                     faceIDfrom = "userlogin"
                     self.push(id: "FaceIDSetViewController", animation: true, fromSB: Login)


                     return true
                 })
                 
                 alert.add(action: yesAction)
                 
                let NoAction = CDAlertViewAction(title: lang.Cancel, font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: { (yesAction) -> Bool in
                     
                     print("no no no")
                     
                     return true
                 })
                 
                 alert.add(action: NoAction)
                 
                 
                 alert.show()

                
                
//                self.showAlertWithYesNo(title: appNameShort, message: "FaceID Not Enabled. Do you want to enable?") { (true) in
//                    faceIDfrom = "userlogin"
//                    self.push(id: "FaceIDSetViewController", animation: true, fromSB: Login)
//
//                }
                
            }

        }
        
        else
        {

            
             Common.checkRemoveCdAlert()
            let alert = CDAlertView(title: AppName, message: lang.sorryunabletoauthenticatefaceid, type: .error)
            alert.circleFillColor = UIColor.buttonprimary
            let doneAction = CDAlertViewAction(title: lang.OK, font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                    alert.add(action: doneAction)
                    alert.show() { (alert) in

                    }
            
        
           // self.showAlertView(message: "Sorry, Unable to Authenticate FACE ID in Your Device")
        }

    }
    
    func checkBiometric()
    {
        // start authentication
        BioMetricAuthenticator.authenticateWithBioMetrics(reason: "", success: {
            
            let vc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainTabbarViewController") as? MainTabbarViewController
            self.navigationController?.pushViewController(vc!, animated: true)
           // self.showAlertView(message: "Authentication Verified")
            
        }, failure: { [weak self] (error) in
            
            // do nothing on canceled
            if error == .canceledByUser || error == .canceledBySystem {
               
                print("cancelled by user")
                return
            }
                
                // device does not support biometric (face id or touch id) authentication
            else if error == .biometryNotAvailable {
                self?.showErrorAlert(message: error.message())
            }
                
                // show alternatives on fallback button clicked
            else if error == .fallback {
                // here we're entering username and password
                // self?.txtUsername.becomeFirstResponder()
            }
                
                // No biometry enrolled in this device, ask user to register fingerprint or face
            else if error == .biometryNotEnrolled {
                self?.showGotoSettingsAlert(message: error.message())
            }
                
                // Biometry is locked out now, because there were too many failed attempts.
                // Need to enter device passcode to unlock.
            else if error == .biometryLockedout {
                self?.showPasscodeAuthentication(message: error.message())
                
            }
                
                // show error on authentication failed
            else {
                self?.showErrorAlert(message: error.message())
            }
        })
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension UserLoginViewController {
    
    func showAlert(title: String, message: String) {
        
        let okAction = AlertAction(title: OKTitle)
        let alertController = getAlertViewController(type: .alert, with: title, message: message, actions: [okAction], showCancel: false) { (button) in
        }
        present(alertController, animated: true, completion: nil)
    }
    
    func showLoginSucessAlert() {
        
        
        //showAlert(title: "Success", message: "Login successful")
    }
    
    func showErrorAlert(message: String) {
        showAlert(title: "Error", message: message)
    }
    
    func showGotoSettingsAlert(message: String) {
        let settingsAction = AlertAction(title: "Go to settings")
        
        let alertController = getAlertViewController(type: .alert, with: "Error", message: message, actions: [settingsAction], showCancel: true, actionHandler: { (buttonText) in
            if buttonText == CancelTitle { return }
            
            // open settings
            // let url = URL(string: "App-Prefs:root=TOUCHID_PASSCODE".localiz())
            let url = URL(string: UIApplication.openSettingsURLString)
            if UIApplication.shared.canOpenURL(url!) {
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            }
            
        })
        present(alertController, animated: true, completion: nil)
    }
    
    func showPasscodeAuthentication(message: String) {
        
        BioMetricAuthenticator.authenticateWithPasscode(reason: message, success: {
            // passcode authentication success
            self.showLoginSucessAlert()
            
        }) { (error) in
            print(error.message())
        }
    }
}
