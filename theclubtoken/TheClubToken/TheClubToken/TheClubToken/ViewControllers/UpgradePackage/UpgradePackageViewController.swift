//
//  UpgradePackageViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 28/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class UpgradePackageViewController: UIViewController {

    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var epinBtn: UIButton!
    @IBOutlet weak var packageBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var epinLbl: UILabel!
    @IBOutlet weak var upgradePackageLbl: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    
    var selectedItem = ""
    var packageName = ""
    lazy var lang = Language.default.object
    override func viewDidLoad() {
        super.viewDidLoad()

        
        titleLbl.text = lang.upgradePackage
        upgradePackageLbl.text = lang.upgradePackage
        submitBtn.setTitle(lang.Submit, for: .normal)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeBtn(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)

    }
    @IBAction func epinact(_ sender: Any) {
        
        selectedItem = "epin"
        selecBtn(btn1: epinBtn, btn2: packageBtn)
    }
    
    @IBAction func upgradepackageact(_ sender: Any) {
        selectedItem = "package"
        selecBtn(btn1: packageBtn, btn2: epinBtn)

    }
  
    
    func selecBtn(btn1:UIButton,btn2:UIButton)
    {
        
        let image = UIImage(named: "radio-on-button")
        let image1 = UIImage(named: "empty")


        btn1.setImage(image, for: .normal)
        btn2.setImage(image1, for: .normal)


    }
    @IBAction func submitact(_ sender: Any) {
        
        if selectedItem == ""
        {
            self.view.make(toast: "Select any one item")
        }
        
        
        else
        {

            if selectedItem == "epin"
            {
                NotificationCenter.default.post(name: Notification.Name("packageupgradepush"), object: nil)
                self.dismiss(animated: false, completion: nil)
            }
            else
            {
                NotificationCenter.default.post(name: Notification.Name("upgradepackagepush"), object: nil)
                self.dismiss(animated: false, completion: nil)


            }
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        selectedItem = ""
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
