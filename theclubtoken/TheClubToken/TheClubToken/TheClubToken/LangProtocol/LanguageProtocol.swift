////
////  LangProtocal.swift
//// Gofer
////
////  Created by Trioangle on 29/07/19.
////  Copyright Â© 2019 Vignesh Palanivel. All rights reserved.
////
//
import Foundation
import UIKit
protocol LanguageProtocol{
     func isRTLLanguage() -> Bool
    
    var success : String {get set}
    
    
    var rewards : String {get set}
    var assets : String {get set}
    var registrationCode : String {get set}
    var clubBalance : String {get set}
    var currentPackage : String {get set}
    
    var community : String {get set}
    var personal : String {get set}
    var topup : String {get set}
    
    var digitalChips : String {get set}
    var withdraw : String {get set}
    var exchange : String {get set}
    
    var sendPayment : String {get set}
    var myEpin : String {get set}
    var transactionHistory : String {get set}
    var upgradePackage : String {get set}
    var refer : String {get set}
    
    
    var home : String {get set}
    var wallet : String {get set}
    var genealogy : String {get set}
    var transactions : String {get set}
    var sixdigitPIN : String {get set}
    var confirmSixdigitPIN : String {get set}
    var PleaseEntermobile : String {get set}

    var login : String {get set}
    var name : String {get set}
    var rewardsCap : String {get set}
    var personalRewards : String {get set}
    var teamBonus : String {get set}
    var binaryMatchingBonus : String {get set}
    var communityCap : String {get set}
    
    
    
    var tctBalance : String {get set}
     var tctWalletCategory : String {get set}
     var transationType : String {get set}
     var fee : String {get set}
     var totalValue : String {get set}
     var assetNewBalance : String {get set}
    
    
    var Welcometo : String {get set}
    var TheClubToken : String {get set}
    var SignIn : String {get set}
    var OpenNewAccount : String {get set}
    var Cancel : String {get set}
    var Usepin : String {get set}
    var Signin : String {get set}
    var OK : String {get set}
    var Next : String {get set}
    var Forgot : String {get set}
    var Inputpincodeorusefingerprint : String {get set}
    var Settings : String {get set}
    var Nofingerprintsfound : String {get set}
    var NofingerprintsfoundText : String {get set}
    var Fingerprintnotrecognized : String {get set}
    var Fingerprintrecognized : String {get set}
    var Confirmfingerprinttocontinue : String {get set}
    var Touchsensor : String {get set}
    var Fingerprinticon : String {get set}
    var YourAccountDetails : String {get set}
    var Continue : String {get set}
    var EnterMobileNumber : String {get set}
    var EnterCountryCode : String {get set}
    var EnterValidMobileNumber : String {get set}
    var InvalidMobileNumber : String {get set}
    var EnterVerificationCode : String {get set}
    var InvalidVerificationCode : String {get set}
    var WelcometoTheClubToken : String {get set}
    var YourEmail : String {get set}
    var Donthaveanaccountyet : String {get set}
    var SignUp : String {get set}
    var Signupnow : String {get set}
    var Verify : String {get set}
    var SendVerificationcode : String {get set}
    var PleaseEnterEmailid : String {get set}
   // var EnterVerificationCode : String {get set}
   // var Signupnow : String {get set}
   // var Donthaveanaccountyet : String {get set}
    var Hello : String {get set}
    var Signinwithanotheraccount : String {get set}
    var Home : String {get set}
    var Wallet : String {get set}
    var Genealogy : String {get set}
    var Transactions : String {get set}
    var EditProfile : String {get set}
    var Email : String {get set}
    var PhoneNumber : String {get set}
    var EnablePINforTransfer : String {get set}
    var BioAuthentication : String {get set}
    var AutolockSetTimer : String {get set}
    var ChangeSecondaryPIN : String {get set}
    var ChangePIN : String {get set}
    var Support : String {get set}
    var Feedback : String {get set}
    var FAQtext : String {get set}
    var TermsandConditions : String {get set}
    var PrivacyPolicy : String {get set}
    var Account : String {get set}
    var Authentication : String {get set}
    var Help : String {get set}
    var RegistrationCodes : String {get set}
    var Rewards : String {get set}
    var Assets : String {get set}
    var ClubBalance : String {get set}
    var DigitalChips : String {get set}
    var Exchange : String {get set}
    var Community : String {get set}
    var Personal : String {get set}
 //   var TheClubToken : String {get set}
    var Topup : String {get set}
    var Bitcoin : String {get set}
    var ExchangeDigitalChips : String {get set}
    var NewSubAccount : String {get set}
    var TransferSubAccount : String {get set}
    var UserName : String {get set}
    var ReferralCode : String {get set}
    var EPIN : String {get set}
    var FreeAccount : String {get set}
    var Register : String {get set}
    var Notifications : String {get set}
    var ReferandEarnBonusRewards : String {get set}
    var ClickbelowtoshareyouruniqueReferralCode : String {get set}
    var ByinvitingthemtoenjoyanyTCTpackage : String {get set}
    var Placement : String {get set}
    var Left : String {get set}
    var Auto : String {get set}
    var Right : String {get set}
    var Membership : String {get set}
    var Profile : String {get set}
    var RegisterNewSuAccount : String {get set}
    var TransferSuAccount : String {get set}
    var ViewCertificate : String {get set}
    var SubmitKYC : String {get set}
    var SwitchAccount : String {get set}
    var Logout : String {get set}
    var TODO : String {get set}
    var Balance : String {get set}
    var AreyousureyouwanttoLogout : String {get set}
    var Yes : String {get set}
    var No : String {get set}
    var TCT : String {get set}
    var shareusingtct : String {get set}
    var Fieldisempty : String {get set}
    var isempty : String {get set}
    var Otpmismatch : String {get set}
    var Gender : String {get set}
    var EnableBioAuthentication : String {get set}
    var UseIt : String {get set}
    var SKIP : String {get set}
    var TakePhoto : String {get set}
    var ChoosePhoto : String {get set}
    var Galleryisnotopening : String {get set}
    var Permissionnecessary : String {get set}
    var Tousegalleryitisnecessary : String {get set}
    var TouseCameraitisnecessary : String {get set}
    var TakePictureFailedorcanceled : String {get set}
 //   var Wallet : String {get set}
    var TotalBalance : String {get set}
   // var TCT : String {get set}
    var USD : String {get set}
    var Anyrewardsswapwillincurfee : String {get set}
    var ExchangeRate : String {get set}
    var AmountAfterExchanged : String {get set}
    var Swap : String {get set}
    var CurrentPackage : String {get set}
    var ScanandPayTCT : String {get set}
    var ReceiverWalletAddress : String {get set}
    var ReceiverName : String {get set}
    var PayTransfer : String {get set}
    var ReferralCodeCopied : String {get set}
    var ReferralCodeEmpty : String {get set}
    //var Transactions : String {get set}
    //var Type : String {get set}
    var Category : String {get set}
    var Date : String {get set}
    var Copied : String {get set}
    var CodeEmpty : String {get set}
    var TCTBalance : String {get set}
    var TopUpTCTAsset : String {get set}
    var WalletCategory : String {get set}
    var TransactionType : String {get set}
    var Fee : String {get set}
    var TotalValue : String {get set}
    var AssetNewBalance : String {get set}
    var TopUp : String {get set}
    var ReceiverInformation : String {get set}
    var Name : String {get set}
    var Address : String {get set}
    var ScanandPay : String {get set}
    var enterWallet : String {get set}
    var receiverWallet : String {get set}
   // var Balance : String {get set}
    var Total : String {get set}
    var NetworkEarnings : String {get set}
    var GlobalShare : String {get set}
    var SignInwithMobileNumber : String {get set}
    var or : String {get set}
    var SignInwithEmail : String {get set}
    var Gift : String {get set}
    var Save : String {get set}
   // var WalletCategory : String {get set}
    var TotalDigitalChips : String {get set}
    var ExchangeBy : String {get set}
    var Submit : String {get set}
    var Message : String {get set}
  //  var Genealogy : String {get set}
  //  var Name : String {get set}
    var RewardsCap : String {get set}
    var PersonalRewards : String {get set}
    var TeamBonus : String {get set}
    var BinaryMatchingBonus : String {get set}
    var CommunityCap : String {get set}
    var Share : String {get set}
    var HelpdeskSupport : String {get set}
    var Send : String {get set}
    var UpgradePackage : String {get set}
    var Epin : String {get set}
    var Bycryptos : String {get set}
    var UploadIDProof : String {get set}
    var FirstName : String {get set}
    var MiddleName : String {get set}
    var LastName : String {get set}
   // var PhoneNumber : String {get set}
    var Edit : String {get set}
    var NoRecordsFound : String {get set}
    var CreateEpin : String {get set}
    var PersonalWalletAmount : String {get set}
    var EPINInformation : String {get set}
    var AmountinUSD : String {get set}
    var AmountrequiredBTC : String {get set}
    var GeneratePIN : String {get set}
    var EPINs : String {get set}
    var CurrentRankMember : String {get set}
    var TotalRevenue : String {get set}
    var TotalLeft : String {get set}
    var TotalRight : String {get set}
    var TotalCommunityROIGeneratedLeft : String {get set}
    var TotalCommunityROIGeneratedRight : String {get set}
    var RankPromotionRemaining : String {get set}
    var TeamSize : String {get set}
    var Member : String {get set}
    var Coach : String {get set}
    var Executive : String {get set}
    var Manager : String {get set}
    var Director : String {get set}
    var President : String {get set}
    var TreeView : String {get set}
    var SendPayment : String {get set}
    var MyEpin : String {get set}
    var TransactionHistory : String {get set}
  //  var UpgradePackage : String {get set}
    var Refer : String {get set}
    var DateofBirth : String {get set}
    var YourIntentionforPurchase : String {get set}
    var Previous : String {get set}
  //  var Next : String {get set}
    var KYC : String {get set}
    var ResidentialAddress : String {get set}
    var CityTown : String {get set}
    var PostalCode : String {get set}
    var Nationality : String {get set}
    var Country : String {get set}
    var RegionState : String {get set}
    var Front : String {get set}
    var UploadIdentity : String {get set}
    var Back : String {get set}
    var IdentificationType : String {get set}
    var IdentificationNumber : String {get set}
    var MobileNumber : String {get set}
    var RankMember : String {get set}
    var Package : String {get set}
//    var FirstName : String {get set}
//    var LastName : String {get set}
//    var ReferralCode : String {get set}
    var EPINoptinal : String {get set}
   // var FreeAccount : String {get set}
    var IacceptPrivacyPolicyandTermsofConditions : String {get set}
    var ScanQRcode : String {get set}
    var OpenFlashlight : String {get set}
    var Amount : String {get set}
    var PackageType : String {get set}
    var Free : String {get set}
    var PackageInformation : String {get set}
    var EPINAddress : String {get set}
    var UpgradePackageWith : String {get set}
    var Payby : String {get set}
//    var AmountinUSD : String {get set}
//    var AmountrequiredBTC : String {get set}
    var YourBalance : String {get set}
    var PaymentAmount : String {get set}
    var TransferTo : String {get set}
    var Phone : String {get set}
    var Transfer : String {get set}
    var SuccessfulloginfromnewIP : String {get set}
    var UnsuccessfulloginfromnewIP : String {get set}
    var Thesystemhasdetectedthatyouraccountislogged : String {get set}
    var Thesystemhasdetectedthatyouraccountistrytologged : String {get set}
    var Rewardsandassetsareempty : String {get set}
  //  var Fieldisempty : String {get set}
    var EntertheName : String {get set}
    var Entertheemail : String {get set}
    var Enterthemessage : String {get set}
    var Kindlyaccepttermsandcondition : String {get set}
    var Enterthefeedback : String {get set}
    var Currency : String {get set}
    var SearchCurrency : String {get set}
    var Available : String {get set}
    var Used : String {get set}
    var KYCSubmittedNotApproved : String {get set}
    var PleasesubmityourKYC : String {get set}
    var PermissionisnotavailableRequestingcamerapermission : String {get set}
    var Chooseanyitem : String {get set}
    var Chooseoption : String {get set}
    var EntertheRewards : String {get set}
    var Camerapermissionwasgranted : String {get set}
    var Camerapermissionrequestwasdenied : String {get set}
    var UploadFrontIdentity : String {get set}
    var UploadBackIdentity : String {get set}
    var StatusKYCApproved : String {get set}
    var StatusKYCNotApproved : String {get set}
    var KYCApproved : String {get set}
    //var KYCSubmittedNotApproved : String {get set}
    var Scan : String {get set}
    var EnterWalletAddress : String {get set}
    var EnterRewards : String {get set}
    var EnterAssets : String {get set}
    var Pinchangedsuccessfully : String {get set}
    var Codevalidationerror : String {get set}
    var Unlockwithyourpincode : String {get set}
    var SetdigitPIN : String {get set}
    var Pleaseinputcodeagain : String {get set}
   // var Category : String {get set}
    var SearchCategory : String {get set}
   // var Type : String {get set}
    var SearchType : String {get set}
    var SearchUsername : String {get set}
    var EPINfieldisempty : String {get set}
    var PackageTit : String {get set}
    var SearchPackage : String {get set}
    var EnterAddress : String {get set}
    var ReferEarnNow : String {get set}
    var type : String {get set}
    var Status : String {get set}
    var Welcometothe : String {get set}
    var set : String {get set}
    var Payment : String {get set}
    var paypackagetitle : String {get set}
    var paypackagetype : String {get set}
    var paypackagepayby : String {get set}
    var payproceed : String {get set}
    var purchase : String {get set}
    var Hours_ago : String {get set}
    var Personal_reward : String {get set}
    var Days_ago : String {get set}
    var LevelBonus : String {get set}
    var EpinCreation : String {get set}
    var ReferralBonus : String {get set}
    var touchidnotenabled : String {get set}
    var sorryunabletoauthenticatetouchid : String {get set}
    var faceidnotenabled : String {get set}
    var sorryunabletoauthenticatefaceid: String {get set}
    var Pin_Successfully_Set : String {get set}
    var Your_Authentication_Successfully_Enabled : String {get set}
    var Please_Enter_Mobile_Number : String {get set}
    var Please_Enter_Valid_Mobile_Number : String {get set}
    var Please_Enter_Email_Id : String {get set}
    var Mobile : String {get set}
    var Please_Enter_Your_Email : String {get set}
    var Please_Enter_Verification_Code : String {get set}
    var Please_Enter_Valid_Email_Id : String {get set}
    var Please_Enter_Name : String {get set}
    var Please_Enter_Last_Name : String {get set}
    var Please_Enter_Referral_Code : String {get set}
    var Please_Accept_Privacy_Policy_and_Terms_and_Conditions : String {get set}

    var Verification_Code_Mismatch : String {get set}
    var Enter_Email_Id : String {get set}

    var Please_Enter_First_Name : String {get set}

    
    var Please_Enter_Phone_Number : String {get set}

    var Please_Enter_User_Name : String {get set}
    var Please_Enter_Your_Message : String {get set}
    var Please_Select_Category : String {get set}

    var Please_Select_User_Name : String {get set}
    var Please_Select_Transfer_Account : String {get set}
    var PleaseEnterGender : String {get set}
    var PleaseEnterDateofBirth : String {get set}

    var SelectNationality : String {get set}
    var SelectCountry : String {get set}
    var SelectState : String {get set}
    var PleaseEnterResidentialAddress : String {get set}
    var PleaseEnterCity : String {get set}
    var PleaseEnterPostalCode : String {get set}
    var PleaseSelectNationality : String {get set}
    var PleaseSelectCountry : String {get set}
    var PleaseSelectState : String {get set}

    var PleaseSeletIdentificationType : String {get set}
    var PleaseEnterIdentificationNumber : String {get set}
    var PleaseUploadIdentificationProofFront : String {get set}
    var PleaseUploadIdentificationProofBack : String {get set}

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
extension LanguageProtocol{
    func getBackBtnText() -> String{
        return self.isRTLLanguage() ? "I" : "e"
    }
    func semantic() -> UISemanticContentAttribute{
        return self.isRTLLanguage() ? .forceRightToLeft : .forceLeftToRight
    }
}
