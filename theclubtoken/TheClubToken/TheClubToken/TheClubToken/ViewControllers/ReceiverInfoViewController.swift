//
//  ReceiverInfoViewController.swift
//  TheClubToken
//
//  Created by trioangle on 28/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CDAlertView
import SwiftyJSON

class ReceiverInfoViewController: UIViewController {

    
    @IBOutlet weak var nameTitleLbl: UILabel!
    @IBOutlet weak var addressTitleLbl: UILabel!
    @IBOutlet weak var balanceTitleLbl: UILabel!
    @IBOutlet weak var totalTitleLbl: UILabel!
    @IBOutlet weak var rewardsCenTitleLbl: UILabel!
    
    
    
    @IBOutlet weak var receiverInfoLbl: UILabel!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var nameValLbl: UILabel!
    @IBOutlet weak var addressValLbl: UILabel!
    
    @IBOutlet weak var rewardsTitleLbl: UILabel!
    @IBOutlet weak var totalValLbl: UILabel!
    @IBOutlet weak var rewardValLbl: UILabel!
    
    @IBOutlet weak var headingTitleLbl: UILabel!
    @IBOutlet weak var tctRewardValTxtFld: UITextField!
    @IBOutlet weak var assetValTxtFld: UITextField!
    
    @IBOutlet weak var payTranferBaseView: UIView!
    @IBOutlet weak var payTranferBtn: UIButton!
    
    @IBOutlet weak var tctAssetTitleLbl: UILabel!
    @IBOutlet weak var tctRewardTitleLbl: UILabel!
    
    var isQr = Bool()
    var address = String()
    var currencyId = String()
    var currency = String()
    lazy var lang = Language.default.object
    
    override func viewDidLoad() {
        super.viewDidLoad()

        callPayTranferShowApi()
        baseView.layer.cornerRadius = 30
        payTranferBaseView.layer.cornerRadius = payTranferBaseView.frame.height / 2
        
        self.tctRewardValTxtFld.layer.cornerRadius = 5
        self.tctRewardValTxtFld.layer.borderWidth = 1
        self.tctRewardValTxtFld.layer.borderColor = UIColor.borderGold.cgColor
        
        self.assetValTxtFld.layer.cornerRadius = 5
        self.assetValTxtFld.layer.borderWidth = 1
        self.assetValTxtFld.layer.borderColor = UIColor.borderGold.cgColor
        
        receiverInfoLbl.text = lang.ReceiverInformation
        nameTitleLbl.text = lang.name
        addressTitleLbl.text = lang.Address
        balanceTitleLbl.text = lang.Balance
        rewardsCenTitleLbl.text = lang.rewards
        payTranferBtn.setTitle(lang.PayTransfer, for: .normal)
        

    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func payTranferBtnAction(_ sender: Any) {
        
        callPayTranferApi()
    }
    func callPayTranferShowApi() {
        let params = ["userid":customer_id,"currency":currencyId,"address":address]
                                print(params)
        
                                APIManager.shared.PayTransferShow(params: params as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                                    if response.count > 0 {

                                        self.addressValLbl.text = response["receiver_address"].stringValue
                                        self.nameValLbl.text = response["receiver_name"].stringValue
                                        self.currency = response["currency_symbol"].stringValue
                                        
                                        self.headingTitleLbl.text = "\(self.currency) \(self.lang.WalletCategory)"
                                        self.totalValLbl.text = String(describing: Double(response["total_balance"].stringValue)!)
                                        
                                        if self.currency  == "TCT" {
                                            self.rewardValLbl.isHidden = false
                                            self.rewardsCenTitleLbl.isHidden = false
                                            self.rewardValLbl.text = String(describing: Double(response["rewards_balance"].stringValue)!)
                                            
                                            self.tctAssetTitleLbl.isHidden = false
                                            self.tctRewardTitleLbl.isHidden = false
                                            self.tctRewardValTxtFld.isHidden = false
                                            self.assetValTxtFld.isHidden = false
                                            
                                            self.tctRewardTitleLbl.text = self.lang.rewards
                                            self.tctAssetTitleLbl.text = self.lang.assets
                                            
                                        }
                                        else
                                        {
                                            self.rewardValLbl.isHidden = true
                                            self.rewardsCenTitleLbl.isHidden = true
                                            
                                            self.tctAssetTitleLbl.isHidden = true
                                            self.tctRewardTitleLbl.isHidden = false
                                            self.tctRewardValTxtFld.isHidden = false
                                            self.assetValTxtFld.isHidden = true
                                            
                                            self.tctRewardTitleLbl.text = self.currency
                                            self.tctAssetTitleLbl.text = self.lang.assets
                                        }

                                    }
                                            else {
                                                Common.checkRemoveCdAlert()

                                                let alert = CDAlertView(title: AppName, message: "HOME", type: .error)
                                                alert.circleFillColor = UIColor.buttonprimary

                                                alert.autoHideTime = 2.0
                                                alert.show()

                                                
                                    }

        }
    }
    func callPayTranferApi() {
        
        var params = [String:Any]()
        if self.currency  == "TCT" {
            
            var reward = "0"
            var asset = "0"

            if tctRewardValTxtFld.text! == "" && assetValTxtFld.text! == "" {
                   return
               }
            
            else
            {
                if tctRewardValTxtFld.text != "" || assetValTxtFld.text != ""
                {
                    
                    if tctRewardValTxtFld.text != ""
                    {
                        reward = tctRewardValTxtFld.text!
                    }
                    
                    if assetValTxtFld.text != ""
                    {
                        asset = assetValTxtFld.text!
                    }

                }
            }
            
            params = ["userid":customer_id,"currency":currencyId,"address":self.addressValLbl.text!,"rewards":reward,"assets":asset]
        }
        else
        {
            if self.tctRewardValTxtFld.text! == "" {
                return
            }
            
            params = ["userid":customer_id,"currency":currencyId,"address":self.addressValLbl.text!,"assets":self.tctRewardValTxtFld.text!]
        }
        
                                print(params)
        
                                APIManager.shared.PayTransfer(params: params as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                                    if let dict = response["transfer"].dictionary {
                                        

                                        if dict["status"]!.stringValue == "success" {

                                            
                                             Common.checkRemoveCdAlert()
                                                    let alert = CDAlertView(title: AppName, message: dict["message"]?.stringValue, type: .success)
                                            alert.circleFillColor = UIColor.buttonprimary
                                            let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                    alert.add(action: doneAction)
                                                    alert.show() { (alert) in

                                                        
                                                        let vc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainTabbarViewController") as? MainTabbarViewController
                                                        let appDelegate: AppDelegate = (UIApplication.shared.delegate as? AppDelegate)!
                                                        appDelegate.window?.rootViewController = vc

                                                    }
                                        }
                                                else {
                                                    Common.checkRemoveCdAlert()

                                            let alert = CDAlertView(title: AppName, message: dict["msg"]?.stringValue, type: .error)
                                                    alert.circleFillColor = UIColor.buttonprimary

                                                    alert.autoHideTime = 2.0
                                                    alert.show()

                                                    
                                        }
                                        

                                    }
                                            else {
                                                Common.checkRemoveCdAlert()

                                                let alert = CDAlertView(title: AppName, message: "HOME", type: .error)
                                                alert.circleFillColor = UIColor.buttonprimary

                                                alert.autoHideTime = 2.0
                                                alert.show()

                                                
                                    }

        }
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
