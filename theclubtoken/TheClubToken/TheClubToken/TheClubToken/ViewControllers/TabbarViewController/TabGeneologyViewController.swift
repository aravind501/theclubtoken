//
//  TabGeneologyViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 26/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import SwiftyJSON
class TabGeneologyViewController: UIViewController {

    @IBOutlet weak var currentrankLbl: UILabel!
    @IBOutlet weak var totalrevenueLbl: UILabel!
    @IBOutlet weak var totalleftLbl: UILabel!
    @IBOutlet weak var totalrightLbl: UILabel!
    @IBOutlet weak var totalcommunityLeftLbl: UILabel!
    @IBOutlet weak var totalcommunityRightLbl: UILabel!
    @IBOutlet weak var rankpromotionLbl: UILabel!
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var memberLbl: UILabel!
    @IBOutlet weak var coachLbl: UILabel!
    @IBOutlet weak var executiveLbl: UILabel!
    
    @IBOutlet weak var managerLbl: UILabel!
    @IBOutlet weak var directorLbl: UILabel!
    @IBOutlet weak var presidentLbl: UILabel!
    
    @IBOutlet weak var headerTitleLbl: UILabel!
    @IBOutlet weak var currentRankMemTiLbl: UILabel!
    @IBOutlet weak var totalRevanTiLbl: UILabel!
    @IBOutlet weak var totalLeftTiLbl: UILabel!
    @IBOutlet weak var totalRightTitLbl: UILabel!
    @IBOutlet weak var totalcommLeftTiLbl: UILabel!
    @IBOutlet weak var totalcomRightTiLbl: UILabel!
    @IBOutlet weak var rankPromTiLbl: UILabel!
    @IBOutlet weak var teamsizeTiLbl: UILabel!
    @IBOutlet weak var treeViewBtn: UIButton!
    lazy var lang = Language.default.object
    override func viewDidLoad() {
        super.viewDidLoad()

        
        mainview.clipsToBounds = true
        mainview.layer.cornerRadius = 50
        if #available(iOS 11.0, *) {
            mainview.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }

viewgeneology()
        
        
        headerTitleLbl.text = lang.Genealogy
        currentRankMemTiLbl.text = lang.CurrentRankMember
        totalRevanTiLbl.text = lang.TotalRevenue
        totalLeftTiLbl.text = lang.TotalLeft
        totalRightTitLbl.text = lang.TotalRight
        totalcommLeftTiLbl.text = lang.TotalCommunityROIGeneratedLeft
        totalcomRightTiLbl.text = lang.TotalCommunityROIGeneratedRight
        rankPromTiLbl.text = lang.RankPromotionRemaining
        teamsizeTiLbl.text = lang.TeamSize
        treeViewBtn.setTitle(lang.TreeView, for: .normal)
        
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidLayoutSubviews() {
        if #available(iOS 11.0, *){
            
        }else{
           let rectShape = CAShapeLayer()
           rectShape.bounds = view.frame
           rectShape.position = view.center
           rectShape.path = UIBezierPath(roundedRect: view.bounds,    byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 50 , height: 50)).cgPath
           mainview.layer.mask = rectShape
    }
    
    }
    
    
    func viewgeneology()
    {
                    let params1 = ["userid":customer_id]
                               
        
        print(params1)
        
                                APIManager.shared.genealogy_teamview(params: params1 as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    
                                    
                                    let dict = response["genealogy_teamview"].dictionaryValue
                                    
                                    if dict.count > 0
                                    {
                                    let dict1 = response["genealogy_teamview"]["team_ranking"].dictionaryValue
                                    
                                    print("team_ranking \(dict1)")
                                 
                                    self.currentrankLbl.text = dict["current_rank_member"]?.stringValue
                                    self.totalrevenueLbl.text = dict["total_revenue"]?.stringValue
                                    self.totalleftLbl.text = dict["total_left"]?.stringValue
                                    self.totalrightLbl.text = dict["total_right"]?.stringValue
                                    self.totalcommunityLeftLbl.text = dict["total_community_roi_left"]?.stringValue
                                    self.totalcommunityRightLbl.text = dict["total_community_roi_right"]?.stringValue
                                    self.rankpromotionLbl.text = dict["rank_promotion_remaining"]?.stringValue

                                        self.memberLbl.text = "\(self.lang.Member)" + dict1["Member"]!.stringValue
                                    self.coachLbl.text = "\(self.lang.Coach)" + dict1["Coach"]!.stringValue
                                    self.executiveLbl.text = "\(self.lang.Executive)" + dict1["Executive"]!.stringValue
                                    self.managerLbl.text = "\(self.lang.Manager)" + dict1["Manager"]!.stringValue
                                    self.directorLbl.text = "\(self.lang.Director)" + dict1["Director"]!.stringValue
                                    self.presidentLbl.text = "\(self.lang.President)" + dict1["Coach"]!.stringValue
                                    }


        }
    }
    
    @IBAction func backbtn(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func treeviewact(_ sender: Any) {
        
        self.push(id: "GeneologyWebViewController", animation: true, fromSB: Tabbar)

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
