//
//  UIColor+LCAdditions.swift
//  Trabezah
//
//  Created by UPLOGIC on 03/10/18.
//  Copyright © 2018 UPLOGIC. All rights reserved.
//

import UIKit

extension UIColor {
    
    
    //MARK: - init method with hex string and alpha(default: 1)
    public convenience init?(hexString: String, alpha: CGFloat = 1.0) {
        var formatted = hexString.replacingOccurrences(of: "0x", with: "")
        formatted = formatted.replacingOccurrences(of: "#", with: "")
        if let hex = Int(formatted, radix: 16) {
            let red = CGFloat(CGFloat((hex & 0xFF0000) >> 16)/255.0)
            let green = CGFloat(CGFloat((hex & 0x00FF00) >> 8)/255.0)
            let blue = CGFloat(CGFloat((hex & 0x0000FF) >> 0)/255.0)
            self.init(red: red, green: green, blue: blue, alpha: alpha)        }
        else {
            return nil
        }
    }
    
  class var lcDenimBlue: UIColor { 
    return UIColor(red: 59.0 / 255.0, green: 89.0 / 255.0, blue: 153.0 / 255.0, alpha: 1.0)
  }
    class var lcLoginBtn: UIColor {
        return UIColor(red: 0.0 / 255.0, green: 93.0 / 255.0, blue: 243.0 / 255.0, alpha: 1.0)
    }
    class var lcBalackWithTransperent: UIColor {
        return UIColor(white: 0.0, alpha: 0.25)
    }

}
