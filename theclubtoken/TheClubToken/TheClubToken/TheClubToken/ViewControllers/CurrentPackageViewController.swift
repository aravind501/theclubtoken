//
//  CurrentPackageViewController.swift
//  TheClubToken
//
//  Created by trioangle on 27/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import SwiftyJSON
import CDAlertView
class CurrentPackageViewController: UIViewController {

    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var rewardCapLbl: UILabel!
    @IBOutlet weak var personalRewardsLbl: UILabel!
    @IBOutlet weak var teamBonusLbl: UILabel!
    @IBOutlet weak var binaryMatchLbl: UILabel!
    @IBOutlet weak var commCapLbl: UILabel!
    
    @IBOutlet weak var nameTitleLbl: UILabel!
    @IBOutlet weak var rewardCapTitleLbl: UILabel!
    @IBOutlet weak var personalRewardTitleLbl: UILabel!
    @IBOutlet weak var teamBonusTitleLbl: UILabel!
    @IBOutlet weak var binaryMatchingBonusTitleLbl: UILabel!
    @IBOutlet weak var communityCapTitleLbl: UILabel!
    
    
    @IBOutlet weak var purchaseBtn: UIButton!
    var currentPackage = String()
    
    var datas = [String : JSON]()
    lazy var lang = Language.default.object
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loaddata()
        
        nameTitleLbl.text = lang.name
        rewardCapTitleLbl.text = lang.rewardsCap
        personalRewardTitleLbl.text = lang.personalRewards
        teamBonusTitleLbl.text = lang.teamBonus
        binaryMatchingBonusTitleLbl.text = lang.binaryMatchingBonus
        communityCapTitleLbl.text = lang.communityCap
        
        purchaseBtn.setTitle(lang.purchase, for: .normal)
        self.closeBtn.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysTemplate),
                                      for: .normal)
        self.closeBtn.tintColor = UIColor.white
        
        
        print("Datas \(datas)")
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func PURCHASEACT(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name("payment"), object: nil)
        self.dismiss(animated: false, completion: nil)

    }
    
    func loaddata()
    {
        if datas.count > 0
        {
            
            
        namelbl.text = datas["package_name"]?.stringValue

            if let id = datas["rewards_cap"]
            {
                rewardCapLbl.text = datas["rewards_cap"]?.stringValue

            }
            else
            {
                rewardCapLbl.text = "--"

            }
            
            if let id = datas["personal_rewards"]
            {
                personalRewardsLbl.text = datas["personal_rewards"]?.stringValue

            }
            else
            {
                personalRewardsLbl.text = "--"

            }
            
            if let id = datas["team_bonus"]
            {
                teamBonusLbl.text = datas["team_bonus"]?.stringValue

            }
            else
            {
                teamBonusLbl.text = "--"

            }
         
            if let id = datas["binary_matching_bonus"]
            {
                binaryMatchLbl.text = datas["binary_matching_bonus"]?.stringValue

            }
            else
            {
                binaryMatchLbl.text = "--"

            }
            
            if let id = datas["community_cap"]
            {
                commCapLbl.text = datas["community_cap"]?.stringValue

            }
            else
            {
                commCapLbl.text = "--"

            }

        }
        else
        {
            callHomePackageDetailApi()
        }

    }
    
    func callHomePackageDetailApi() {
        let params = ["userid":customer_id]
                                print(params)
        
                                APIManager.shared.Home_package_info(params: params as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                                    
                                    
                                    
                                    if let _ = response["package_info"].dictionary {

                                        self.datas = response["package_info"].dictionaryValue

                                        self.loaddata()
                                        
                                    }
                                            else {
                                                Common.checkRemoveCdAlert()

                                                let alert = CDAlertView(title: AppName, message: "PACKAGE", type: .error)
                                                alert.circleFillColor = UIColor.buttonprimary

                                                alert.autoHideTime = 2.0
                                                alert.show()

                                                
                                    }
                                
        }
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
