//
//  CreateEpinViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 28/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import SwiftyJSON
import CDAlertView

class CreateEpinViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var walletTitle: UILabel!
    @IBOutlet weak var walletamountLbl: UILabel!
    
    @IBOutlet weak var amountLblusd: UILabel!
    @IBOutlet weak var generatePinBtn: UIButton!
    
    @IBOutlet weak var epinInfoLbl: UILabel!
    @IBOutlet weak var personalWalletAmountLbl: UILabel!
    @IBOutlet weak var headerTitleLbl: UILabel!
    @IBOutlet weak var amountlblbtc: UILabel!
    @IBOutlet weak var tctwalletTitle: UILabel!
    @IBOutlet weak var rewardTitleLbl: UILabel!
    @IBOutlet weak var rewardsAmountLbl: UILabel!
    @IBOutlet weak var assetTitleLbl: UILabel!
    @IBOutlet weak var assetAmntLbl: UILabel!
    @IBOutlet weak var packageBtn: UIButton!
    
    @IBOutlet weak var viewheight: NSLayoutConstraint!
    @IBOutlet weak var currencyBtn: UIButton!
    
    @IBOutlet weak var backbtnblack: UIButton!
    
    @IBOutlet weak var locationview: UIView!
    
    @IBOutlet weak var tblviewTitleLbl: UILabel!
    @IBOutlet weak var locationtableview: UITableView!

    var packagesDatas = [JSON]()
    var currencyDatas = [JSON]()

    var packagesList = [String]()
    var currencyList = [String]()
    
    var selectedList = "packages"
    
    var packageId = ""
    var packageRewards = ""
    var currencyId = ""
    
    var assets = ""
    var rewards = ""
    lazy var lang = Language.default.object
    
    @IBOutlet weak var assetview: UIView!
    
    @IBOutlet weak var cryptoLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        assetview.isHidden = true
        
        locationtableview.delegate = self
        locationtableview.dataSource = self
        
        
        headerTitleLbl.text = lang.CreateEpin
        personalWalletAmountLbl.text = lang.PersonalWalletAmount
        epinInfoLbl.text = lang.EPINInformation
        packageBtn.setTitle(lang.PackageTit, for: .normal)
        currencyBtn.setTitle(lang.Currency, for: .normal)
        tblviewTitleLbl.text = lang.PackageTit
        generatePinBtn.setTitle(lang.GeneratePIN, for: .normal)
//        amountlblbtc.text = lang.PersonalWalletAmount
//        amountLblusd.text = lang.EPINInformation
        
        locationtableview.tableFooterView = UIView()
        locationtableview.estimatedRowHeight = 60.0
        locationtableview.rowHeight = UITableView.automaticDimension

        getpackages()

        getcurrencylist()
        
        get_currency_balance()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("createpin"), object: nil)

        // Do any additional setup after loading the view.
    }
    
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        self.navigationController?.popViewController(animated: true)
    }

    
    func getpackages()
    {
        
        let params1 = [":":":"]
        
        print(params1)
        
                                APIManager.shared.packages(params: params1 as [String : AnyObject]) { (response) in
                                    print("response getpackages \(response)")
                                    
                                    self.packagesDatas = response["packages"].arrayValue
                                    self.locationtableview.reloadData()
                                    
                                    

                                    

        }
    }
    
    
    func getcurrencylist()
    {

        let params1 = [":":":"]
        
        print(params1)
        
                                APIManager.shared.currency_list1(params: params1 as [String : AnyObject]) { (response) in
                                    print("response getcurrencylist \(response)")
                                    
                                   // self.currencyDatas = response["currency_list"].arrayValue
                                                                        let dict = response["currency_list"].arrayValue
                                                                        
                                    if dict.count > 0
                                    {
                                                                        for i in 0...dict.count - 1
                                                                        {
                                                                            let dicts = dict[i].dictionaryValue
                                                                            let name = dicts["currency_symbol"]?.stringValue
                                                                            
                                                                                    if current_package == "FREE"
                                                                                    {
                                                                                        
                                                                                        if name != "TCT"
                                                                                        {
                                                                                            let dicts = dict[i]

                                                                                            self.currencyDatas.append(dicts)
                                                                                        }
                                                                                        
                                                                            }
                                                                            else
                                                                                    {
                                                                                    
                                                                                        self.currencyDatas = response["currency_list"].arrayValue

                                    //                                                    if name == "TCT"
                                    //                                                    {
                                    //                                                        let dicts = dict[i]
                                    //
                                    //                                                        self.currencyDatas.append(dicts)
                                    //                                                    }
                                                                            }
                                                                        }
                                        
                                    }
                                    self.locationtableview.reloadData()

        }
    }
    
    func get_currency_balance()
    {
        var cur = ""
       
        if current_package == "FREE"
        {
            cur = "1"
        }
        else
        {
            cur = "3"
        }
        let params1 = ["userid":customer_id,
                       "currency": cur]
        
        print(params1)
        
                                APIManager.shared.get_currency_balance(params: params1 as [String : AnyObject]) { (response) in
                                    print("response get_currency_balance \(response)")
                                    
                                    let cur = response["currency_symbol"].stringValue
                                    if cur == "TCT"
                                    {
                                        self.walletamountLbl.text = response["balance"].stringValue
                                        self.walletTitle.text = "TCT (The Club Token)"
                                    }
                                    
                                    else if cur == "BTC"
                                    {
                                        self.walletamountLbl.text = response["balance"].stringValue
                                        self.walletTitle.text = "BTC (Bitcoin)"
                                    }
                                    else if cur == "ETH"
                                    {
                                        self.walletamountLbl.text = response["balance"].stringValue
                                        self.walletTitle.text = "ETH (Ethereum)"
                                    }
                                    else if cur == "USDT"
                                    {
                                        self.walletamountLbl.text = response["balance"].stringValue
                                        self.walletTitle.text = "USDT (Tether)"
                                    }
                                    else
                                    {
                                        self.walletamountLbl.text = ""
                                        self.walletTitle.text = "TCT (The Club Token)"

                                    }

                                    


        }
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! locationcells
         
        
                if selectedList == "packages"
                {
                    let dict = packagesDatas[indexPath.row].dictionaryValue
                    
                    cell.name.text = dict["package_name"]?.stringValue

        }
        
        else
                {
                    let dict = currencyDatas[indexPath.row].dictionaryValue

                    cell.name.text = dict["currency_symbol"]!.stringValue + " " + "(" + dict["currency_name"]!.stringValue + ")"

        }
         

        // cell.name.text = stringarray[indexPath.row]
         return cell
     }
     
     
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return UITableView.automaticDimension
     }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectedList == "packages"
        {
            return packagesDatas.count

        }
        else
        {
            return currencyDatas.count

        }
     }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if selectedList == "packages"
        {
            let dict = packagesDatas[indexPath.row].dictionaryValue
            
            let name = dict["package_name"]?.stringValue
            packageId = dict["package_id"]!.stringValue
            packageRewards = dict["personal_rewards"]!.stringValue

            self.packageBtn.setTitle(name, for: .normal)
        }
        else
        {
            let dict = currencyDatas[indexPath.row].dictionaryValue
            let name = dict["currency_symbol"]?.stringValue
            self.currencyBtn.setTitle(name, for: .normal)
            currencyId = dict["id"]!.stringValue

        }
        
        
        
        
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            
            self.locationview.isHidden = true
            
            self.backbtnblack.isHidden = true
        })
        
        
        let package = self.packageBtn.currentTitle
        let currencyValue = self.currencyBtn.currentTitle
        
        if package! != lang.Package && currencyValue! != lang.Currency
        {
            create_epin_show()
        }
        
    }
    
    @IBAction func backact(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func viewcloseact(_ sender: Any) {
        
        
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            
            self.locationview.isHidden = true
            
            self.backbtnblack.isHidden = true
            
        })
        
        
    }
    
    @IBAction func selectamountBtn(_ sender: Any) {
        
selectedList = "packages"
        tblviewTitleLbl.text = "Select \(lang.PackageTit)"
        self.viewheight.constant = 380

        self.locationtableview.reloadData()
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            
            self.locationview.isHidden = false
            
            self.backbtnblack.isHidden = false
            
        })
        
    }
    
    @IBAction func selectCurrencyBtn(_ sender: Any) {
        selectedList = "currency"
        tblviewTitleLbl.text = "Select \(lang.Currency)"

        self.viewheight.constant = 250
        self.locationtableview.reloadData()
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            
            self.locationview.isHidden = false
            
            self.backbtnblack.isHidden = false
            
        })
        
    }
  
    
    func create_epin_show()
    {
        
        
        let params1 = ["userid":customer_id,
                       "package":packageId,
                       "currency": currencyId,
                       "rewards":packageRewards]
        
        print(params1)
        
                                APIManager.shared.create_epin_show(params: params1 as [String : AnyObject]) { (response) in
                                    print("response create_epin_show \(response)")
                                    
                                    let dict = response["create_epin_show"].dictionaryValue

                                    let cur = self.currencyBtn.currentTitle
                                    self.amountLblusd.text = "\(self.lang.AmountinUSD)" + "\n" + dict["Amount_in_usd"]!.stringValue

                                    if let id = dict["Amount_in_BTC"]
                                    {
                                        self.amountlblbtc.text = "\(self.lang.AmountrequiredBTC)" + " " + cur! + "\n" + dict["Amount_in_BTC"]!.stringValue
                                        
                                        self.tctwalletTitle.text = "BTC (Bitcoin)"
                                        self.cryptoLbl.text = dict["Amount_in_BTC"]!.stringValue
                                        
                                        self.walletamountLbl.text = dict["Personal_Wallet_Amount_BTC"]?.stringValue
                                        self.walletTitle.text = "BTC (Bitcoin)"

                                        if let id = dict["Assets"]
                                        {
                                            self.assets = dict["Assets"]!.stringValue
                                        }
                                        self.hideshow(lb1: false, lb2: true)
                                    }
                                    
                                    if let id = dict["Amount_in_ETH"]
                                    {
                                        self.amountlblbtc.text = "\(self.lang.AmountrequiredBTC)" + " " + cur! + "\n" + dict["Amount_in_ETH"]!.stringValue
                                        self.tctwalletTitle.text = "ETH (Ethereum)"
                                        self.cryptoLbl.text = dict["Amount_in_ETH"]!.stringValue
                                        
                                        self.walletamountLbl.text = dict["Personal_Wallet_Amount_ETH"]?.stringValue
                                        self.walletTitle.text = "ETH (Ethereum)"
                                        if let id = dict["Assets"]
                                        {
                                            self.assets = dict["Assets"]!.stringValue
                                        }

                                        
                                        self.hideshow(lb1: false, lb2: true)

                                    }
                                    
                                    if let id = dict["Amount_in_TCT"]
                                    {
                                        self.amountlblbtc.text = "\(self.lang.AmountrequiredBTC)" + " " + cur! + "\n" + dict["Amount_in_TCT"]!.stringValue
                                       
                                        self.tctwalletTitle.text = self.lang.tctWalletCategory
                                        self.rewardsAmountLbl.text = dict["Rewards"]?.stringValue
                                        self.assetAmntLbl.text = dict["Assets"]?.stringValue
                                        self.walletamountLbl.text = dict["Personal_Wallet_Amount_TCT"]?.stringValue
                                        self.walletTitle.text = "TCT (The Club Token)"

                                        if let id = dict["Assets"]
                                        {
                                            self.assets = dict["Assets"]!.stringValue
                                        }
                                        
                                        if let id = dict["Rewards"]
                                        {
                                            self.rewards = dict["Rewards"]!.stringValue
                                        }

                                        

                                        self.hideshow(lb1: true, lb2: false)

                                    }

                                    if let id = dict["Amount_in_USDT"]
                                    {
                                        self.amountlblbtc.text = "\(self.lang.AmountrequiredBTC)" + " " + cur! + "\n" + dict["Amount_in_USDT"]!.stringValue
                                        self.tctwalletTitle.text = "USDT (Tether)"
                                        self.cryptoLbl.text = dict["Amount_in_USDT"]!.stringValue
                                        
                                        self.walletamountLbl.text = dict["Personal_Wallet_Amount_USDT"]?.stringValue
                                        self.walletTitle.text = "USDT (Tether)"
                                     
                                        if let id = dict["Assets"]
                                        {
                                            self.assets = dict["Assets"]!.stringValue
                                        }

                                        
                                        self.hideshow(lb1: false, lb2: true)

                                    }



        }
        
    }
    
    func hideshow(lb1:Bool,lb2:Bool)
    {
        self.assetview.isHidden = false

        self.cryptoLbl.isHidden = lb1
        self.rewardTitleLbl.isHidden = lb2
        self.rewardsAmountLbl.isHidden = lb2
        self.assetAmntLbl.isHidden = lb2
        self.assetTitleLbl.isHidden = lb2

    }
    
    
    @IBAction func createepinbtn(_ sender: Any) {
        
        
        if packageBtn.currentTitle == lang.PackageTit
        {
            self.view.make(toast: "Please Select Package")
        }
        else if currencyBtn.currentTitle == lang.Currency
        {
            self.view.make(toast: "Please Select Currency")

        }
       
        else
        {
            
            
               let package = self.currencyBtn.currentTitle

            
                   var params1 = ["userid":customer_id,
                                  "package":packageId,
                                  "currency": currencyId,
                                  "assets":assets]

               print(params1)

               if package == "TCT"

               {
                   params1["rewards"] = rewards
               }
               else
               {

               }

            
            
        
                                APIManager.shared.create_epin(params: params1 as [String : AnyObject]) { (response) in
                                    print("response create_epin \(response)")
                                  
                                    

                                    let status = response["status"].stringValue
                                    let msg = response["message"].stringValue
                                    
                                    if status == "success"
                                    {
                                        
                                        
                                         Common.checkRemoveCdAlert()
                                                let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                        alert.circleFillColor = UIColor.buttonprimary
                                        let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                alert.add(action: doneAction)
                                                alert.show() { (alert) in

                                                    
                                                    let referVc = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "EpinBarCodeViewController") as? EpinBarCodeViewController
                                                    referVc?.modalPresentationStyle = .overCurrentContext
                                                    referVc?.from = "createpin"
                                                    referVc?.package = response["package_amount"].stringValue
                                                    referVc?.code = response["epincode"].stringValue
                                                    referVc?.status = response["status"].stringValue

                                                    referVc?.barcode = response["barcode"].stringValue

                                                    self.navigationController?.present(referVc!, animated: false, completion: nil)


                                                }
                                    }
                                    
                                    else
                                    {
                                        let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                        alert.circleFillColor = UIColor.buttonprimary

                                        alert.autoHideTime = 2.0
                                        alert.show()

                                    }

        }
        }

    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class locationcells : UITableViewCell
{
    @IBOutlet weak var name: UILabel!
    
}
