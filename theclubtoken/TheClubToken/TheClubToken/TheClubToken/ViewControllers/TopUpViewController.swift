//
//  TopUpViewController.swift
//  TheClubToken
//
//  Created by trioangle on 27/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CDAlertView
import SwiftyJSON

class TopUpViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    
    @IBOutlet weak var tctBalValLbl: UILabel!
    @IBOutlet weak var rewardValLbl: UILabel!
    @IBOutlet weak var transationTypeLbl: UILabel!
    @IBOutlet weak var feeLbl: UILabel!
    @IBOutlet weak var totalValueLbl: UILabel!
    @IBOutlet weak var assetsNewBalLbl: UILabel!
    @IBOutlet weak var topUpBtn: UIButton!
    @IBOutlet weak var rewardTxtFld: UITextField!
    
    @IBOutlet weak var topTitleLbl: UILabel!
    @IBOutlet weak var tctBalanceTitleLbl: UILabel!
    @IBOutlet weak var rewardsTitleLbl: UILabel!
    @IBOutlet weak var tctWalletCategoryTitleLbl: UILabel!
    @IBOutlet weak var transationTypeTitleLbl: UILabel!
    @IBOutlet weak var rewardsDownTitleLbl: UILabel!
    @IBOutlet weak var feeTitleLbl: UILabel!
    @IBOutlet weak var totalValTitleLbl: UILabel!
    @IBOutlet weak var assetNewBalTitleLbl: UILabel!
    
    
    lazy var lang = Language.default.object
    var topUpDict = JSON()
    var currId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.rewardTxtFld.delegate = self
        self.rewardTxtFld.layer.cornerRadius = 5
        self.rewardTxtFld.layer.borderWidth = 1
        self.rewardTxtFld.layer.borderColor = UIColor.borderGold.cgColor
        
        self.closeBtn.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysTemplate),
                                      for: .normal)
        self.closeBtn.tintColor = UIColor.white
        self.setValues()
        rewardTxtFld.text = String(describing: Double(topUpDict["rewards"].stringValue)!)
        // Do any additional setup after loading the view.
    }
    
    func setValues() {
        
        topTitleLbl.text = lang.TopUpTCTAsset
        tctBalanceTitleLbl.text = lang.tctBalance
        rewardsTitleLbl.text = lang.rewards
        tctWalletCategoryTitleLbl.text = lang.tctWalletCategory
        transationTypeTitleLbl.text = lang.transationType
        rewardsDownTitleLbl.text = lang.rewards
        feeTitleLbl.text = lang.fee
        totalValTitleLbl.text = lang.totalValue
        assetNewBalTitleLbl.text = lang.assetNewBalance
        topUpBtn.setTitle(lang.TopUp, for: .normal)
        transationTypeLbl.text = topUpDict["transaction_type"].string
        rewardValLbl.text = "\(String(describing: Double(topUpDict["rewards"].stringValue)!)) TCT"
        tctBalValLbl.text = String(describing: Double(topUpDict["balance"].stringValue)!)
        
        totalValueLbl.text = String(describing: Double(topUpDict["total_value"].stringValue)!)
        assetsNewBalLbl.text = String(describing: Double(topUpDict["asset_new_balance"].stringValue)!)
        feeLbl.text = topUpDict["Fee"].string
        
    }
    @IBAction func topUpBtnAction(_ sender: Any) {
        
        callTopUpAssetApi()
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    func callTopUpshowApi(reward:String) {
        
        var currId = String()
        
        for dict in APPDELEGATE.currenctListArry {
            
            if "\(dict["currency_symbol"].stringValue)" == "TCT"
            {
               currId = "\(dict["id"].stringValue)"
            }
        }
        
        let params = ["userid":customer_id,"currency":currId,"rewards":reward]
                                print(params)
        
                                APIManager.shared.TopUpShowWithOutProgress(params: params as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                                    
                                    if response.count > 0 {
                                        self.topUpDict = response
                                        self.setValues()
        
                        
                                    }
                                            else {
                                                Common.checkRemoveCdAlert()

                                                let alert = CDAlertView(title: AppName, message: "TOPUPSHOW", type: .error)
                                                alert.circleFillColor = UIColor.buttonprimary

                                                alert.autoHideTime = 2.0
                                                alert.show()

                                                
                                    }
        }
    }
    
    func callTopUpAssetApi() {
        let params = ["userid":customer_id,"currency":self.currId,"rewards":rewardTxtFld.text!]
                                print(params)
        
                                APIManager.shared.TopUpAsset(params: params as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                                    let status = response["status"].stringValue
                                    let msg = response["msg"].stringValue

                                    if status == "success"
                                    {
                                        
                                        
                                        
                                        
                                         Common.checkRemoveCdAlert()
                                                let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                        alert.circleFillColor = UIColor.buttonprimary
                                        let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                alert.add(action: doneAction)
                                                alert.show() { (alert) in
                                                    NotificationCenter.default.post(name: Notification.Name("topup"), object: nil)
                                                    self.dismiss(animated: false, completion: nil)


                                                }
                                        
//                                        let alert = CDAlertView(title: AppName, message: msg, type: .success)
//                                        alert.circleFillColor = UIColor.buttonprimary
//
//                                        alert.autoHideTime = 1.0
//                                        alert.show { (alert) in
//
//                                        }

//                                        let referral_code = response["referral_code"].dictionary
//                                        self.referalCode = referral_code!["referral_code"]!.stringValue
                        
                                    }
                                            else {
                                               
                                        let msg = response["msg"].stringValue

                                        Common.checkRemoveCdAlert()

                                                let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                                alert.circleFillColor = UIColor.buttonprimary

                                                alert.autoHideTime = 2.0
                                                alert.show()

                                                
                                    }
        }
    }
    
      func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method

      }

      func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
          
        self.callTopUpshowApi(reward: textField.text!)
        
          return true
      }
      
      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          
          let rewardVal = String(describing: textField.text!) + string
          
          self.callTopUpshowApi(reward: rewardVal)
          
          return true
      }

      func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()

          return true
      }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
