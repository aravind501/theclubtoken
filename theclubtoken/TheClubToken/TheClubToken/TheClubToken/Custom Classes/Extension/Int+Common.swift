//
//  Int+Common.swift
//  Trabezah
//
//  Created by UPLOGIC on 03/10/18.
//  Copyright © 2018 UPLOGIC. All rights reserved.
//

import Foundation
extension Int {
    var toConvertCountFormat:String {
        switch abs(self) {
        case 0...999:
            return String(format:"%d",self)
        case 1000...999999:
            return String("1 k")
        case 1000000...999999999:
            return String("1 M")
        default:
            return String("1 b")
        }
    }
    var isSingular : String {
        let singular = self > 1 ? "s" : ""
        return singular
    }
}
