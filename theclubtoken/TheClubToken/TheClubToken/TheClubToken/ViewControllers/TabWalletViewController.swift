//
//  TabWalletViewController.swift
//  TheClubToken
//
//  Created by trioangle on 27/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import ExpandableCell
import CDAlertView
import SwiftyJSON
import DropDown

class TabWalletViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var tableviewOutlet: ExpandableTableView!
    @IBOutlet weak var tableBaseView: UIView!
    @IBOutlet weak var totalBalBaseView: UIView!
    @IBOutlet weak var totalBalValLbl: UILabel!
    @IBOutlet weak var totalBalTilLbl: UILabel!
    
    var swapFromCurr = String()
    var swapToCurr = String()
    var swapReward = String()
    var swapAsset = String()
    var dropDownSelectIndex = Int()
    var expandableSelectIndex = Int()
    var dropdownArry = [JSON]()
    var parentCells: [[String]] =  [[String]]()
    lazy var lang = Language.default.object
      
          var rewardCell: UITableViewCell {
              return tableviewOutlet.dequeueReusableCell(withIdentifier: WalletCell.ID)!
          }
    var totalBal = String()
    var walletMainDict = JSON()
    let dropDown = DropDown()
    override func viewDidLoad() {
        super.viewDidLoad()

        
        totalBalBaseView.layer.cornerRadius = 5
        tableBaseView.layer.cornerRadius = 60
        
        totalBalTilLbl.text = lang.TotalBalance
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        callWalletApi()
        
        self.tableviewOutlet.expandableDelegate = self
        self.tableviewOutlet.animation = .automatic
        
            tableviewOutlet.register(UINib(nibName: "WalletCell", bundle: nil), forCellReuseIdentifier: WalletCell.ID)
            tableviewOutlet.register(UINib(nibName: "WalletExpandableCell", bundle: nil), forCellReuseIdentifier: WalletExpandableCell.ID)
    }
    
    func callWalletApi() {
        let params = ["userid":customer_id]
                                print(params)
        
                                APIManager.shared.Wallet(params: params as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                              
                                    if let walletDict = response["wallet"].dictionary {

                                        self.walletMainDict = response
                                        self.totalBal = "\(String(describing: Double(walletDict["total_usd_balance"]!.stringValue)!)) USD"
                                        self.totalBalValLbl.text = self.totalBal
                                        var cellArr = [String]()
                                        for i in 0..<walletDict.count {
                                            print("for",i)
                                            if let _ = walletDict["\(i)"]?.dictionary {
                                                print("cell",i)
                                                cellArr.append(WalletExpandableCell.ID)
                                            }
                                        }
                                        
                                        if cellArr.count > 0 {
                                            
                                            self.parentCells.append(cellArr)
                                            self.tableviewOutlet.reloadData()
                                        
                                        }
                                        
                                    
                                                }
                                                        else {
                                                            Common.checkRemoveCdAlert()

                                                            let alert = CDAlertView(title: AppName, message: "PACKAGE", type: .error)
                                                            alert.circleFillColor = UIColor.buttonprimary

                                                            alert.autoHideTime = 2.0
                                                            alert.show()

                                                            
                                                }
                                    
        }
    }
    
    func callSwapShowApi(from_currency:String,rewards:String,assets:String,to_currency:String,cell:WalletCell) {
        let params = ["userid":customer_id,"from_currency":from_currency,"rewards":rewards,"assets":assets,"to_currency":to_currency]
                                print(params)
        
                                APIManager.shared.SwapShow(params: params as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                                    if let swapDict = response["swap_show"].dictionary {
                                    
                                        
                                        if let id = swapDict["Amount_after_exchanged"]
                                        {
                                        let keyArr = swapDict.keys
                                        
                                        for strKey in keyArr {
                                           if strKey.contains("1 TCT")
                                           {
                                             cell.exchangeRateLabel.text = "\(strKey) = \(swapDict[strKey]!.stringValue)"
                                            cell.exchangedAmountLabel.text = "\(swapDict["Amount_after_exchanged"]!.stringValue)"
                                            }
                                        }
                                        
                                    }
                                        
                                        else
                                        {
                                            Common.checkRemoveCdAlert()

                                            let msg = swapDict["message"]?.stringValue
                                            let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                            alert.circleFillColor = UIColor.buttonprimary

                                            alert.autoHideTime = 2.0
                                            alert.show()

                                        }
                                    }
                                    else{
                                                Common.checkRemoveCdAlert()

                                                let alert = CDAlertView(title: AppName, message: "SWAPSHOW ERROR", type: .error)
                                                alert.circleFillColor = UIColor.buttonprimary

                                                alert.autoHideTime = 2.0
                                                alert.show()

                                                
                                    }
                                    
                              
        }
    }
    
    func callSwapApi(from_currency:String,rewards:String,assets:String,to_currency:String) {
        let params = ["userid":customer_id,"from_currency":from_currency,"rewards":rewards,"assets":assets,"to_currency":to_currency]
                                print(params)
        
                                APIManager.shared.Swap(params: params as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                                    if let swapDict = response["swap"].dictionary {
                                        
                                        let status = swapDict["status"]!.stringValue
                                        let msg = swapDict["message"]!.stringValue

                                                                        
                                                                        if status == "success"
                                                                        {

                                                                            
                                                                            
                                                                             Common.checkRemoveCdAlert()
                                                                                    let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                                                            alert.circleFillColor = UIColor.buttonprimary
                                                                            let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                                                    alert.add(action: doneAction)
                                                                                    alert.show() { (alert) in

                                                                                        
                                                                                                                                                                    let vc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainTabbarViewController") as? MainTabbarViewController
                                                                                        let appDelegate: AppDelegate = (UIApplication.shared.delegate as? AppDelegate)!
                                                                                        appDelegate.window?.rootViewController = vc

                                                                                    }
                                                                            
                                                                            
                                                                            
                                                                           // self.callWalletApi()
                                                                            
                                                                            

                                                            
                                                                        }
                                                                                else {
                                                                                    
                                                                            Common.checkRemoveCdAlert()

                                                                                    let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                                                                    alert.circleFillColor = UIColor.buttonprimary

                                                                                    alert.autoHideTime = 2.0
                                                                                    alert.show()

                                                                                    
                                                                        }
                                    }
                                    else{
                                        Common.checkRemoveCdAlert()

                                                
                                                let alert = CDAlertView(title: AppName, message: "SWAP", type: .error)
                                                alert.circleFillColor = UIColor.buttonprimary

                                                alert.autoHideTime = 2.0
                                                alert.show()

                                                
                                    }
                                    
                              
        }
    }
    func callGetQrApi(currency_symbol:String) {
        let params = ["userid":customer_id,"currency_symbol":currency_symbol]
                                print(params)
        
                                APIManager.shared.GetQrCode(params: params as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    
                              
                                    if let dict = response["qr_code"].dictionary {

                                    let qrVc = UIStoryboard.init(name: "TabbarStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "QRViewController") as? QRViewController
                                        qrVc!.titleStr = currency_symbol
                                        qrVc?.qrImgStr = (dict["qr_code"]?.stringValue)!
                                        qrVc?.modalPresentationStyle = .overCurrentContext
                                        self.navigationController?.present(qrVc!, animated: false, completion: nil)
                                                                  
                                                              
                                                                          }
                                                                                  else {
                                                                                      Common.checkRemoveCdAlert()

                                                                                      let alert = CDAlertView(title: AppName, message: "GET QR", type: .error)
                                                                                      alert.circleFillColor = UIColor.buttonprimary

                                                                                      alert.autoHideTime = 2.0
                                                                                      alert.show()

                                                                                      
                                                                          }        }
    }
    
    @IBAction func openAllButtonClicked() {
         tableviewOutlet.openAll()
     }
     
     @IBAction func expandMultiButtonClicked(_ sender: Any) {
         tableviewOutlet.expansionStyle = .multi
     }
     
     @IBAction func expandSingleButtonClicked(_ sender: Any) {
         tableviewOutlet.expansionStyle = .single
         tableviewOutlet.closeAll()
     }
     
     @IBAction func closeAllButtonClicked() {
         tableviewOutlet.closeAll()
     }
     
     @IBAction func SelectionDisplayOn(_ sender: UIButton) {
         tableviewOutlet.autoRemoveSelection = !tableviewOutlet.autoRemoveSelection
         let isOn = tableviewOutlet.autoRemoveSelection ? "Off" : "On"
         sender.setTitle("Selection Stays \(isOn)", for: .normal)
     }
    
    @objc func copyBtnPressed(sender: UIButton!) {
    
        if let walletDict = self.walletMainDict["wallet"].dictionary {

            if let dict = walletDict["\(sender.tag)"]?.dictionary {
                UIPasteboard.general.string = dict["currency_address"]?.stringValue
                
            }
        }
    }
    
    @objc func qrBtnPressed(sender: UIButton!) {
    
        
        if let walletDict = self.walletMainDict["wallet"].dictionary {

            if let dict = walletDict["\(sender.tag)"]?.dictionary {
                let curr_symbol = dict["currency_symbol"]?.stringValue
                let title = dict["currency_symbol"]?.stringValue
                self.callGetQrApi(currency_symbol: curr_symbol!)
            }
        }
    }
    
    @objc func dropDownPressed(sender: UIButton!) {
        
        let button = sender
        let cell = button?.superview?.superview?.superview?.superview as? WalletCell
        let indexPath = sender.tag
        print(indexPath)
        
        var dropDownList = [String]()
        
        
        self.dropdownArry.removeAll()
        
        for dict in APPDELEGATE.currenctListArry {
            if "\(dict["currency_symbol"].stringValue)" != "TCT" {
                dropdownArry.append(dict)
            }
            
        }
        
        for dict in self.dropdownArry {
            if "\(dict["currency_symbol"].stringValue)" != "TCT" {
                dropDownList.append("\(dict["currency_symbol"].stringValue)")
            }
            
        }
        
        self.dropDown.dataSource = dropDownList
            self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                
                print("DRopdownarray \(self.dropdownArry)")
                
                print("dropDownList \(dropDownList)")

                
                self.dropDownSelectIndex = index
                let dict = self.dropdownArry[index]
                
                cell?.dropdownLbl.text = "\(dict["currency_symbol"].stringValue)"
                cell?.rewardSwapLbl.text = "Any rewards swap will incur fee \(dict["transaction_fee"].stringValue)"
                cell?.exchangeRateLabel.text = "1 \(dict["currency_symbol"].stringValue) = \(dict["rate"].stringValue)"
                
                
               // let fromdict = APPDELEGATE.currenctListArry[self.expandableSelectIndex]
                
                let fromdict = APPDELEGATE.currenctListArry[indexPath]

                print("SWAPfrom","\(fromdict["currency_symbol"].stringValue)")
                print("SWAPto","\(dict["currency_symbol"].stringValue)")
                
                self.swapFromCurr = "\(fromdict["id"].stringValue)"
                self.swapToCurr = "\(dict["id"].stringValue)"
                self.swapReward = (cell?.rewardTxtFld.text)!
                self.swapAsset = (cell?.assetsTxtFld.text)!
                  

                if self.swapAsset == "" && self.swapReward != "" {
                  
                    self.callSwapShowApi(from_currency: self.swapFromCurr, rewards: self.swapReward, assets: "0", to_currency: self.swapToCurr, cell: cell!)
                         
                      }
                
                
                if self.swapReward == "" &&  self.swapAsset != ""{
                        self.callSwapShowApi(from_currency: self.swapFromCurr, rewards: "0", assets: self.swapAsset, to_currency: self.swapToCurr, cell: cell!)
                         
                      }
                      
                if self.swapReward != "" && self.swapAsset != ""{
                        
                        self.callSwapShowApi(from_currency: self.swapFromCurr, rewards: self.swapReward, assets: self.swapAsset, to_currency: self.swapToCurr, cell: cell!)
                          

                      }
                

                self.dropDown.hide()

            }
            
        dropDown.anchorView = cell?.dropDownBaseView
            dropDown.borderColor = UIColor.black
            dropDown.backgroundColor = UIColor.white
            dropDown.borderLineWidth = 1.0
            
            dropDown.show()
        
    }
    @objc func SwapPressed(sender: UIButton!) {
        
        let button = sender
        let cell = button?.superview?.superview?.superview?.superview as? WalletCell
        let indexPath = sender.tag
        print(indexPath)
        
        
        var dicts = [JSON]()
        
        for dict in APPDELEGATE.currenctListArry {
            if "\(dict["currency_symbol"].stringValue)" != "TCT" {
                dicts.append(dict)
            }
            
        }
        
        
        
        let dict = APPDELEGATE.currenctListArry[indexPath]
     
       // let selectedDict = APPDELEGATE.currenctListArry[self.dropDownSelectIndex]
        
        let selectedDict = dicts[self.dropDownSelectIndex]

        print("SWAPfrom","\(dict["currency_symbol"].stringValue)")
        print("SWAPto","\(selectedDict["currency_symbol"].stringValue)")
        
        swapFromCurr = "\(dict["id"].stringValue)"
        swapToCurr = "\(selectedDict["id"].stringValue)"
        swapReward = (cell?.rewardTxtFld.text)!
        swapAsset = (cell?.assetsTxtFld.text)!
        
        if swapReward.count > 0 || swapAsset.count > 0{
            
            if swapReward == "" {
                swapReward = "0"
            }
            if swapAsset == "" {
                swapAsset = "0"
            }
            callSwapApi(from_currency: swapFromCurr, rewards: swapReward, assets: swapAsset, to_currency: swapToCurr)
        }

    }
    
      func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method

      }

      func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        
        let cell = textField.superview?.superview?.superview as? WalletCell
        let indexPath = textField.tag
        print(indexPath)
        
        
        var dicts = [JSON]()
        
        for dict in APPDELEGATE.currenctListArry {
            if "\(dict["currency_symbol"].stringValue)" != "TCT" {
                dicts.append(dict)
            }
            
        }
        
        let selectedDict = dicts[self.dropDownSelectIndex]


        
        let dict = APPDELEGATE.currenctListArry[indexPath]
        
       // let selectedDict = APPDELEGATE.currenctListArry[self.dropDownSelectIndex]
        
        print("SWAPfrom","\(dict["currency_symbol"].stringValue)")
        print("SWAPto","\(selectedDict["currency_symbol"].stringValue)")
        
        swapFromCurr = "\(dict["id"].stringValue)"
        swapToCurr = "\(selectedDict["id"].stringValue)"
        swapReward = (cell?.rewardTxtFld.text)!
        swapAsset = (cell?.assetsTxtFld.text)!
          

             if swapAsset == "" && swapReward != "" {
                  callSwapShowApi(from_currency: swapFromCurr, rewards: swapReward, assets: "0", to_currency: swapToCurr, cell: cell!)
                 
              }
              if swapReward == "" &&  swapAsset != ""{
                 callSwapShowApi(from_currency: swapFromCurr, rewards: "0", assets: swapAsset, to_currency: swapToCurr, cell: cell!)
                 
              }
              
              if swapReward != "" && swapAsset != ""{
                
                callSwapShowApi(from_currency: swapFromCurr, rewards: swapReward, assets: swapAsset, to_currency: swapToCurr, cell: cell!)
                  

              }
    

          return true
      }
      
      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          
        let cell = textField.superview?.superview?.superview as? WalletCell
          let indexPath = textField.tag
          print(indexPath)
          
        var dicts = [JSON]()
        
        for dict in APPDELEGATE.currenctListArry {
            if "\(dict["currency_symbol"].stringValue)" != "TCT" {
                dicts.append(dict)
            }
            
        }
        
        let selectedDict = dicts[self.dropDownSelectIndex]


          let dict = APPDELEGATE.currenctListArry[indexPath]
         // let selectedDict = APPDELEGATE.currenctListArry[self.dropDownSelectIndex]
          
          print("SWAPfrom","\(dict["currency_symbol"].stringValue)")
          print("SWAPto","\(selectedDict["currency_symbol"].stringValue)")
          
          swapFromCurr = "\(dict["id"].stringValue)"
          swapToCurr = "\(selectedDict["id"].stringValue)"
        
        if textField == cell?.rewardTxtFld {
            
            swapReward = (cell?.rewardTxtFld.text)! + string
            swapAsset = (cell?.assetsTxtFld.text)!
        }
        else if textField == cell?.assetsTxtFld {
            
            swapReward = (cell?.rewardTxtFld.text)!
            swapAsset = (cell?.assetsTxtFld.text)! + string
        }
        
          
            
               if swapAsset == "" && swapReward != "" {
                    callSwapShowApi(from_currency: swapFromCurr, rewards: swapReward, assets: "0", to_currency: swapToCurr, cell: cell!)
                   
                }
                if swapReward == "" &&  swapAsset != ""{
                   callSwapShowApi(from_currency: swapFromCurr, rewards: "0", assets: swapAsset, to_currency: swapToCurr, cell: cell!)
                   
                }
                
                if swapReward != "" && swapAsset != ""{
                  
                  callSwapShowApi(from_currency: swapFromCurr, rewards: swapReward, assets: swapAsset, to_currency: swapToCurr, cell: cell!)
                    

                }
          
          return true
      }

      func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()

          return true
      }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
        extension TabWalletViewController: ExpandableDelegate {
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCellsForRowAt indexPath: IndexPath) -> [UITableViewCell]? {

                        let walletCell = tableviewOutlet.dequeueReusableCell(withIdentifier: WalletCell.ID) as! WalletCell
                       
                        walletCell.rewardTxtFld.layer.cornerRadius = 5
                        walletCell.rewardTxtFld.layer.borderWidth = 1
                        walletCell.rewardTxtFld.layer.borderColor = UIColor.borderGold.cgColor
                
                        walletCell.assetsTxtFld.layer.cornerRadius = 5
                        walletCell.assetsTxtFld.layer.borderWidth = 1
                        walletCell.assetsTxtFld.layer.borderColor = UIColor.borderGold.cgColor
                        
                        walletCell.assetsTxtFld.layer.borderWidth = 1
                        walletCell.assetsTxtFld.layer.borderColor = UIColor.borderGold.cgColor

                        walletCell.dropDownBaseView.layer.cornerRadius = 5
                        
                        walletCell.swapBtnBaseView.layer.cornerRadius = walletCell.swapBtnBaseView.frame.height / 2
                
                walletCell.dropDownBtn.tag = indexPath.row
                walletCell.swapBtn.tag = indexPath.row
                walletCell.dropDownBtn.addTarget(self, action: #selector(dropDownPressed(sender:)), for: .touchUpInside)
                walletCell.swapBtn.addTarget(self, action: #selector(SwapPressed(sender:)), for: .touchUpInside)
                
                let dict = APPDELEGATE.currenctListArry.first
                walletCell.dropdownLbl.text = "\(dict!["currency_symbol"].stringValue)"
                
                walletCell.rewardTxtFld.delegate = self
                walletCell.assetsTxtFld.delegate = self
                
                walletCell.rewardTxtFld.tag = indexPath.row
                walletCell.assetsTxtFld.tag = indexPath.row
                
                walletCell.rewardTxtFld.text = ""
                walletCell.assetsTxtFld.text = ""
                
                
                walletCell.rewardTitlLbl.text = self.lang.rewards
                walletCell.assetTilLbl.text = self.lang.assets
                walletCell.exchangeRateTiLbl.text = self.lang.ExchangeRate
                walletCell.amountAfterExTiLbl.text = self.lang.AmountAfterExchanged
                walletCell.swapBtn.setTitle(lang.Swap, for: .normal)
                
                walletCell.rewardSwapLbl.text = "\(lang.Anyrewardsswapwillincurfee) \(dict!["transaction_fee"].stringValue)"
                walletCell.exchangeRateLabel.text = "1\(dict!["currency_symbol"].stringValue) = \(dict!["rate"].stringValue)"
                
                 return [walletCell]

            }
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, heightsForExpandedRowAt indexPath: IndexPath) -> [CGFloat]? {
 
                let walletDict = self.walletMainDict["wallet"].dictionary
                let dict = walletDict?["\(indexPath.row)"]?.dictionary
                let currSym = dict?["currency_symbol"]?.stringValue
                
                if currSym == "TCT"{
                    return [250]
                }
                else
                {
                    return[0]
                }
                
            }
            
            func numberOfSections(in tableView: ExpandableTableView) -> Int {
                return parentCells.count
            }

            func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int {
                return parentCells[section].count
            }

            func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectRowAt indexPath: IndexPath) {
              //  let cell = expandableTableView.dequeueReusableCell(withIdentifier: parentCells[indexPath.section][indexPath.row]) as! ExpandableCell1

              ///  cell.baseView.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
                self.expandableSelectIndex = indexPath.row
                print("didSelectRow:\(indexPath)")
               // expandableTableView.reloadData()
            }
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectExpandedRowAt indexPath: IndexPath) {
                print("didSelectExpandedRowAt:\(indexPath)")
            }
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCell: UITableViewCell, didSelectExpandedRowAt indexPath: IndexPath) {
//                if let cell = expandedCell as? ExpandedCell {
//                    print("\(cell.titleLabel.text ?? "")")
//                }
            }
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, titleForHeaderInSection section: Int) -> String? {
                return "Section:\(section)"
            }
            func expandableTableView(_ expandableTableView: ExpandableTableView, heightForHeaderInSection section: Int) -> CGFloat {
                return 0
            }
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let cell = expandableTableView.dequeueReusableCell(withIdentifier: parentCells[indexPath.section][indexPath.row]) as! WalletExpandableCell
                
                cell.copyPasteBtn.setImage(UIImage(named: "copy")?.withRenderingMode(.alwaysTemplate),
                                              for: .normal)
                cell.copyPasteBtn.tintColor = UIColor.white
                
                if let walletDict = self.walletMainDict["wallet"].dictionary {

                    if let dict = walletDict["\(indexPath.row)"]?.dictionary {
                        cell.titleLabel.text = dict["currency_symbol"]?.stringValue
                        cell.subTitleLabel.text = "(\(String(describing: dict["currency_name"]!.stringValue)))"
                        cell.titleValLabel.text = dict["currency_balance"]?.stringValue
                        cell.subTitleValLabel.text = dict["currency_usd_balance"]?.stringValue
                        cell.copyValLbl.text = dict["currency_address"]?.stringValue
                        
                        let img = (dict["currency_image"]?.stringValue)!
                        cell.coinImgView.layer.cornerRadius = cell.coinImgView.frame.height / 2
                        cell.coinImgView.sd_setImage(with: URL(string: img), completed: nil)
                    
                        
                        cell.copyPasteBtn.tag = indexPath.row
                        cell.qrBtn.tag = indexPath.row
                        cell.copyPasteBtn.addTarget(self, action: #selector(copyBtnPressed(sender:)), for: .touchUpInside)
                        cell.qrBtn.addTarget(self, action: #selector(qrBtnPressed(sender:)), for: .touchUpInside)
                        
                    }
                
                }

                return cell
            }
            
            func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                
                return 120
            }
            
            @objc(expandableTableView:didCloseRowAt:) func expandableTableView(_ expandableTableView: UITableView, didCloseRowAt indexPath: IndexPath) {
                let cell = expandableTableView.cellForRow(at: indexPath)
                cell?.contentView.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
                cell?.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
            }
            
            func expandableTableView(_ expandableTableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
                return true
            }
            
            func expandableTableView(_ expandableTableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        //        let cell = expandableTableView.cellForRow(at: indexPath)
        //        cell?.contentView.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        //        cell?.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            }
            
        //    func expandableTableView(_ expandableTableView: ExpandableTableView, titleForHeaderInSection section: Int) -> String? {
        //        return "Section \(section)"
        //    }
        //
        //    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForHeaderInSection section: Int) -> CGFloat {
        //        return 33
        //    }
        }
