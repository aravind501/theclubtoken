//
//  RegisterViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 25/04/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CDAlertView
class RegisterViewController: UIViewController {
    @IBOutlet weak var bottomview: UIView!

    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var yourAccTiLbl: UILabel!
    @IBOutlet weak var nameTxtffld: HoshiTextField!
    @IBOutlet weak var middlenameTxtfld: HoshiTextField!
    @IBOutlet weak var lastnameTxtflfd: HoshiTextField!
    @IBOutlet weak var referalTxtfld: HoshiTextField!
    @IBOutlet weak var epinTxtfld: HoshiTextField!
    @IBOutlet weak var privacypoliceimg: UIImageView!
    @IBOutlet weak var termsLbl: UILabel!
    @IBOutlet weak var liveenvironmentImg: UIImageView!
    @IBOutlet weak var freeAccTiLbl: UILabel!
    
    var privaypolicy = false
    var liveenv = false
    lazy var lang = Language.default.object
    var myString = "I accept Privacy Policy and Terms of Conditions"

    
    override func viewDidLoad() {
        super.viewDidLoad()

        bottomview.clipsToBounds = true
        bottomview.layer.cornerRadius = 50
       
        if #available(iOS 11.0, *) {
            bottomview.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }

        setDesigns()
        
        yourAccTiLbl.text = lang.YourAccountDetails
        nameTxtffld.placeholder = lang.name
        middlenameTxtfld.placeholder = lang.MiddleName
        lastnameTxtflfd.placeholder = lang.LastName
        referalTxtfld.placeholder = lang.ReferralCode
        epinTxtfld.placeholder = lang.EPINoptinal
        freeAccTiLbl.text = lang.FreeAccount
        continueBtn.setTitle(lang.Continue, for: .normal)


        // Do any additional setup after loading the view.
    }
    
    @IBAction func privacypolicyselect(_ sender: Any) {
        
        if(privaypolicy == false) {
            
            self.privacypoliceimg.image = #imageLiteral(resourceName: "tick")

        } else {
            
            self.privacypoliceimg.image = #imageLiteral(resourceName: "circle")
        }
        
        privaypolicy = !privaypolicy
    }
    
    @IBAction func liveenvironmentact(_ sender: Any) {
        
        if(liveenv == false) {
            
            self.liveenvironmentImg.image = #imageLiteral(resourceName: "tick")

        } else {
            
            self.liveenvironmentImg.image = #imageLiteral(resourceName: "circle")
        }
        
        liveenv = !liveenv
    }
    
    @IBAction func backact(_ sender: Any) {
        
        self.popLeft()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        nameTxtffld.text = ""
        middlenameTxtfld.text = ""
        lastnameTxtflfd.text = ""
        referalTxtfld.text = ""
        epinTxtfld.text = ""
        self.privacypoliceimg.image = #imageLiteral(resourceName: "circle")
        liveenv = false
    }
    func setDesigns(){
        
        self.nameTxtffld.borderActiveColor = .textcolor
        self.nameTxtffld.borderInactiveColor = .lightGray
        self.nameTxtffld.placeholderColor = .textcolor
        self.nameTxtffld.textColor = .textcolor
        self.nameTxtffld.delegate = self
        
        self.middlenameTxtfld.borderActiveColor = .textcolor
        self.middlenameTxtfld.borderInactiveColor = .lightGray
        self.middlenameTxtfld.placeholderColor = .textcolor
        self.middlenameTxtfld.textColor = .textcolor
        self.middlenameTxtfld.delegate = self
       
        self.lastnameTxtflfd.borderActiveColor = .textcolor
        self.lastnameTxtflfd.borderInactiveColor = .lightGray
        self.lastnameTxtflfd.placeholderColor = .textcolor
        self.lastnameTxtflfd.textColor = .textcolor
        self.lastnameTxtflfd.delegate = self
        
        self.referalTxtfld.borderActiveColor = .textcolor
               self.referalTxtfld.borderInactiveColor = .lightGray
               self.referalTxtfld.placeholderColor = .textcolor
               self.referalTxtfld.textColor = .textcolor
               self.referalTxtfld.delegate = self
        
        self.epinTxtfld.borderActiveColor = .textcolor
               self.epinTxtfld.borderInactiveColor = .lightGray
               self.epinTxtfld.placeholderColor = .textcolor
               self.epinTxtfld.textColor = .textcolor
               self.epinTxtfld.delegate = self

        myString = lang.IacceptPrivacyPolicyandTermsofConditions
        
        let termsRange = (myString as NSString).range(of: lang.TermsandConditions)
        // comment for now
        let privacyRange = (myString as NSString).range(of: lang.PrivacyPolicy)
        
        let myMutableString = NSMutableAttributedString(string: myString, attributes: [NSAttributedString.Key.font :UIFont(name: "Montserrat-Regular", size: 12.0)!])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.buttonprimary, range: privacyRange)
        
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.buttonprimary, range: termsRange)

        termsLbl.isUserInteractionEnabled = true

        termsLbl.attributedText = myMutableString
        termsLbl.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(tapLabel(gesture:))))

        
    }
    
    
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
       let termsRange = (myString as NSString).range(of: lang.TermsandConditions)
       // comment for now
       let privacyRange = (myString as NSString).range(of: lang.PrivacyPolicy)

       if gesture.didTapAttributedTextInLabel(label: termsLbl, inRange: privacyRange) {
        
        self.push(id: "PrivacyPolicyViewController", animation: true, fromSB: Profile)

        
     //   self.view.make(toast: "Privacy Policy")

       }
             else if gesture.didTapAttributedTextInLabel(label: termsLbl, inRange: termsRange)
              {
                
                self.push(id: "TermsandConditionsViewController", animation: true, fromSB: Profile)

        
               // self.view.make(toast: "Terms of Conditions")

        }
       
       else {
           print("Tapped none")
       }
    }
    
    override func viewDidLayoutSubviews() {
        if #available(iOS 11.0, *){
            
        }else{
           let rectShape = CAShapeLayer()
           rectShape.bounds = view.frame
           rectShape.position = view.center
           rectShape.path = UIBezierPath(roundedRect: view.bounds,    byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 50 , height: 50)).cgPath
           bottomview.layer.mask = rectShape
    }
    
    }
    
    
    @IBAction func continueact(_ sender: Any) {

        
        if nameTxtffld.text == ""
        {
            
            self.view.make(toast: lang.Please_Enter_Name)
        }
        
        
        else if lastnameTxtflfd.text == ""
        {
            self.view.make(toast: lang.Please_Enter_Last_Name)
        }
            
            else if referalTxtfld.text == ""
            {
                self.view.make(toast: lang.Please_Enter_Referral_Code)
            }

            
        
        
        else if privaypolicy == false
        {
            self.view.make(toast: lang.Please_Accept_Privacy_Policy_and_Terms_and_Conditions)

        }
            
        else
        {

            

            var epin = ""
            let ref = referalTxtfld.text!
            var mname = ""
            if epinTxtfld.text == ""
            {
                epin = ""
                
            }
            else
            {
                epin = epinTxtfld.text!
            }
            
//            if referalTxtfld.text == ""
//            {
//             ref = ""
//            }
//            else
//            {
//                ref = referalTxtfld.text!
//            }
            
            
            
             if middlenameTxtfld.text == ""
            {
mname = ""
            }
            else
             {
                mname = middlenameTxtfld.text!
                
            }
            
            

            let params1 = ["firstname":nameTxtffld.text!,
                           "middlename":mname,
                           "lastname":lastnameTxtflfd.text!,
                           "referral_code":ref,
                           "epin":epin,
                           "free_account":"yes",
                           "environment":"test",
                           "username":nameTxtffld.text!,
                           "device_id":device_token]
            
                        print(params1)
                        APIManager.shared.Register(params: params1 as [String : AnyObject]) { (response) in
                            print("response params1 \(response)")
                            print("CHeckparams")
                            

                                let msg = response["message"].stringValue
                                let status = response["status"].stringValue

                                    
                                    if status == "success" {
                                        
                                            let userid = response["userid"].stringValue
customer_id = userid
                    
                                        Common.checkRemoveCdAlert()
                                               let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                       alert.circleFillColor = UIColor.buttonprimary
                                       let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                               alert.add(action: doneAction)
                                               alert.show() { (alert) in
                                      // self.push(id: "MobileVerificationViewController", animation: true, fromSB: Login)
                                                
                                                self.push(id: "EmailVerificationViewController", animation: true, fromSB: Login)

                                               }

                                       
                                    }
                                        
                                    else {
                                        
                                        Common.checkRemoveCdAlert()

                                        let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                        alert.circleFillColor = UIColor.buttonprimary

                                        alert.autoHideTime = 2.0
                                        alert.show()

                                        
                            }
                                    
                                
                                    


                            
                            
                        
                            
                        
                                    
             
                                }        }

       // self.push(id: "MobileVerificationViewController", animation: true, fromSB: Login)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension RegisterViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        

        if textField == nameTxtffld
        {
        nameTxtffld.placeholder = lang.name
        }
        
        else if textField == middlenameTxtfld
        {
        middlenameTxtfld.placeholder = lang.MiddleName
        }
        else if textField == lastnameTxtflfd
        {
        lastnameTxtflfd.placeholder = lang.LastName
        }
        else if textField == referalTxtfld
        {
        referalTxtfld.placeholder = lang.ReferralCode
        }
        else if textField == epinTxtfld
        {
        epinTxtfld.placeholder = lang.EPINoptinal
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.count == 0 {
            
            
            if textField == nameTxtffld
                   {
                   nameTxtffld.placeholder = lang.name
                    self.nameTxtffld.borderInactiveColor = .lightGray

                   }
                   
                   else if textField == middlenameTxtfld
                   {
                   middlenameTxtfld.placeholder = lang.MiddleName
                    self.middlenameTxtfld.borderInactiveColor = .lightGray

                   }
                   else if textField == lastnameTxtflfd
                   {
                   lastnameTxtflfd.placeholder = lang.LastName
                    self.lastnameTxtflfd.borderInactiveColor = .lightGray

                   }
                   else if textField == referalTxtfld
                   {
                   referalTxtfld.placeholder = lang.ReferralCode
                    self.referalTxtfld.borderInactiveColor = .lightGray

                   }
            
            else if textField == epinTxtfld
            {
            epinTxtfld.placeholder = lang.EPINoptinal
             self.epinTxtfld.borderInactiveColor = .lightGray

            }

        }
    }
    
}
