//
//  EditProfileAuthenticationViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 05/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class EditProfileAuthenticationViewController: UIViewController {

    @IBOutlet weak var accountline: UIView!
    @IBOutlet weak var accountview: UIView!
    @IBOutlet weak var authenticationview: UIView!
    @IBOutlet weak var authenticationline: UIView!
    @IBOutlet weak var helpview: UIView!
    @IBOutlet weak var helpline: UIView!
    @IBOutlet weak var bioauthenticationswitch: UISwitch!
    @IBOutlet weak var enablepinTransferswitch: UISwitch!
   
    @IBOutlet weak var accountbtn: UIButton!
    
    @IBOutlet weak var authbtn: UIButton!
    @IBOutlet weak var helpbtn: UIButton!

    @IBOutlet weak var backbtn: UIButton!
    
    @IBOutlet weak var timerview: UIView!
    
    @IBOutlet weak var oneminBtn: UIButton!
    @IBOutlet weak var tenminBtn: UIButton!
    
    @IBOutlet weak var twentyminBtn: UIButton!
    @IBOutlet weak var thirtyminBtn: UIButton!
    
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var accountTiLbl: UILabel!
    @IBOutlet weak var authedicationTiLbl: UILabel!
    @IBOutlet weak var helpTiLbl: UILabel!
    @IBOutlet weak var chnagePinTiLbl: UILabel!
    @IBOutlet weak var changeSecondyPinTiLbl: UILabel!
    @IBOutlet weak var autoSetTimTiLbl: UILabel!
    @IBOutlet weak var bioAutheTiLbl: UILabel!
    @IBOutlet weak var enablePinTransTiLbl: UILabel!
    
    lazy var lang = Language.default.object
    
    override func viewDidLoad() {
        super.viewDidLoad()

        switchreSize()
        
        titleLbl.text = lang.Authentication
        accountTiLbl.text = lang.Account
        authedicationTiLbl.text = lang.Authentication
        helpTiLbl.text = lang.Help
        
        chnagePinTiLbl.text = lang.ChangePIN
        changeSecondyPinTiLbl.text = lang.ChangeSecondaryPIN
        autoSetTimTiLbl.text = lang.AutolockSetTimer
        bioAutheTiLbl.text = lang.BioAuthentication
        enablePinTransTiLbl.text = lang.EnablePINforTransfer
        
        if let timer = UserDefaults.standard.string(forKey: "timer")
        {
            if timer == "onemin"
            {
                selecBtn(btn1: oneminBtn, btn2: tenminBtn, btn3: twentyminBtn, btn4: thirtyminBtn)

            }
            else if timer == "tenmin"
            {
                selecBtn(btn1: tenminBtn, btn2: oneminBtn, btn3: twentyminBtn, btn4: thirtyminBtn)

            }
            else if timer == "twentymin"
            {
                selecBtn(btn1: twentyminBtn, btn2: oneminBtn, btn3: tenminBtn, btn4: thirtyminBtn)

            }
            
            else if timer == "thirtymin"
            {
                selecBtn(btn1: thirtyminBtn, btn2: oneminBtn, btn3: tenminBtn, btn4: twentyminBtn)

            }
        }
        
        else
        {
          //  UserDefaults.standard.set("onemin", forKey: "timer")

        }
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func oneminBtn(_ sender: Any) {
      APPDELEGATE.screenlock = 60

        NotificationCenter.default.post(name: Notification.Name("timerchange"), object: nil)

        UserDefaults.standard.set("onemin", forKey: "timer")

        selecBtn(btn1: oneminBtn, btn2: tenminBtn, btn3: twentyminBtn, btn4: thirtyminBtn)
    }
    
    @IBAction func tenminBtn(_ sender: Any) {
        APPDELEGATE.screenlock = 600
        
        NotificationCenter.default.post(name: Notification.Name("timerchange"), object: nil)

        UserDefaults.standard.set("tenmin", forKey: "timer")

        selecBtn(btn1: tenminBtn, btn2: oneminBtn, btn3: twentyminBtn, btn4: thirtyminBtn)

    }
    
    @IBAction func twentymin(_ sender: Any) {
       
        APPDELEGATE.screenlock = 1200

        NotificationCenter.default.post(name: Notification.Name("timerchange"), object: nil)

        UserDefaults.standard.set("twentymin", forKey: "timer")

        
        selecBtn(btn1: twentyminBtn, btn2: oneminBtn, btn3: tenminBtn, btn4: thirtyminBtn)

    }
    
    @IBAction func thirtyminBtn(_ sender: Any) {
        
        APPDELEGATE.screenlock = 1800

        NotificationCenter.default.post(name: Notification.Name("timerchange"), object: nil)

        UserDefaults.standard.set("thirtymin", forKey: "timer")

        selecBtn(btn1: thirtyminBtn, btn2: oneminBtn, btn3: tenminBtn, btn4: twentyminBtn)

    }
    
    func selecBtn(btn1:UIButton,btn2:UIButton,btn3:UIButton,btn4:UIButton)
    {
        
        let image = UIImage(named: "radio-on-button")
        let image1 = UIImage(named: "empty")


        btn1.setImage(image, for: .normal)
        btn2.setImage(image1, for: .normal)
        btn3.setImage(image1, for: .normal)
        btn4.setImage(image1, for: .normal)


    }
    
    
    @IBAction func closeact(_ sender: Any) {
        
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {

            self.backbtn.isHidden = true
            self.timerview.isHidden = true
        })
    }
    func switchreSize()
        
    {
        bioauthenticationswitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        enablepinTransferswitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        
         let bio = UserDefaults.standard.bool(forKey: "bio")
        
        let pin = UserDefaults.standard.bool(forKey: "pintransfer")

            if bio
            {
            bioauthenticationswitch.isOn = true
            bioauthenticationswitch.setOn(true, animated: true)
            }
            else
            {
                bioauthenticationswitch.isOn = false
                bioauthenticationswitch.setOn(false, animated: false)

            }
            
        if pin
        {
        enablepinTransferswitch.isOn = true
        enablepinTransferswitch.setOn(true, animated: true)
        }
        else
        {
            enablepinTransferswitch.isOn = false
            enablepinTransferswitch.setOn(false, animated: false)

        }

        
        
        
        
        
        
    }
    
    @IBAction func backbtn(_ sender: Any) {

        
        let mainViewControllerVC = self.navigationController?.viewControllers.first(where: { (viewcontroller) -> Bool in
            return viewcontroller is UserProfileListViewController
        })
        if let mainViewControllerVC = mainViewControllerVC {
            navigationController?.popToViewController(mainViewControllerVC, animated: true)
        }

    }
       @IBAction func accountact(_ sender: Any) {
        
        self.push(id: "EditProfileViewController", animation: false, fromSB: Profile)
    }
    @IBAction func authenticationact(_ sender: Any) {
    }
    
    @IBAction func helpact(_ sender: Any) {
        
        self.push(id: "ProfileHelpViewController", animation: false, fromSB: Profile)
    }


    @IBAction func changepinact(_ sender: Any) {
        
        pinfrom = "menu"

        
        self.push(id: "ProfilePinCodeViewController", animation: true, fromSB: Profile)

    }
     @IBAction func changesecondarypin(_ sender: Any) {
     }
     
    @IBAction func autolockact(_ sender: Any) {
        
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {

            self.backbtn.isHidden = false
            self.timerview.isHidden = false
        })
        
    }
    
    @IBAction func bioauthenticationact(_ sender: UISwitch) {
        
        
        if sender.isOn
        {
            UserDefaults.standard.set(true, forKey: "bio")

            self.view.make(toast: "Bio Authentication Enabled")

        }
        else
        {
            UserDefaults.standard.set(false, forKey: "bio")

            self.view.make(toast: "Bio Authentication Disabled")

        }
    }
    
    @IBAction func enablepintransferact(_ sender: UISwitch) {
        
        if sender.isOn
        {
            UserDefaults.standard.set(true, forKey: "pintransfer")

            self.view.make(toast: "Pin Enabled for Transfer")


        }
        else
        {
            UserDefaults.standard.set(false, forKey: "pintransfer")

            self.view.make(toast: "Pin Disabled for Transfer")


        }
    }
    /*
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
