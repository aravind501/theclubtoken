//
//  ApiList.swift
//  Centros_Camprios
//
//  Created by imac on 12/18/17.
//  Copyright © 2017 Appoets. All rights reserved.
//

import Foundation

//Http Method Types

enum HttpType : String{
    
    case GET = "GET"
    case POST = "POST"
    case PATCH = "PATCH"
    case PUT = "PUT"
    case DELETE = "DELETE"
    
}

// Status Code
struct BASEAPI {
    
    static let AUTHORIZEKEY = ""
    
    static let APIKEY = ""
    
    
    static let DEMOURL = "https://www.marquislux.com/"
    static let DEMOIMGURL = "https://www.marquislux.com/"
    
    static let LIVEURL = ""
    static let LIVEIMGURL = ""

    
    static let URL = DEMOURL
    static let IMGURL = DEMOIMGURL
    
    
}



enum StatusCode : Int {
    
    case notreachable = 0
    case success = 200
    case multipleResponse = 300
    case unAuthorized = 401
    case notFound = 404
    case ServerError = 500
    
}



enum Base : String{
    
    case  home = "home"
    case  userRegister = "signup"
    case  email_otp_signup = "email_otp_signup"
    case  verify_email = "verify_email"
    case  email_otp_signin = "email_otp_signin"
    case  login_email = "login_email"
    case  phone_otp_signup = "phone_otp_signup"
    case  verify_phone = "verify_phone"
    case  phone_otp_signin = "phone_otp_signin"
    case  login_phone = "login_phone"
    case  update_profile = "update_profile"
    case  view_profile = "view_profile"
    case profile_img_update = "profile_img_update"
    case  kyc_step1 = "kyc_step1"
    case  kyc_step2 = "kyc_step2"
    case  kyc_step3 = "kyc_step3"
    
    case  swap = "swap"
    case  swap_show = "swap_show"
    case  wallet = "wallet"
    case  get_qrcode = "get_qrcode"
    
    case  topup_show = "topup_show"
    case  topup_asset = "topup_asset"
    case  currency_list = "currency_list"
    
    case  transfer_show = "transfer_show"
    case  transfer_pay = "transfer_pay"
    
    case  upgrade_package_epinshow = "upgrade_package_epinshow"
    case  upgrade_package_epin = "upgrade_package_epin"
    
    case  exchange_chipshow = "exchange_chipshow"
    case  exchange_chips = "exchange_chips"

    case  support_category = "support_category"
    case  support = "support"
    case  feedback = "feedback"

    case  promotions = "promotions"
    case  announcements = "announcements"
    case  site_settings = "site_settings"
    case  tokens = "tokens"
    case  packages = "packages"

    case  get_currency_balance = "get_currency_balance"

    case  upgrade_package_show = "upgrade_package_show"
    case  create_epin_show = "create_epin_show"
    case  upgrade_package_cryptoshow = "upgrade_package_cryptoshow"
    case  upgrade_package = "upgrade_package"
    case  upgrade_package_crypto = "upgrade_package_crypto"


    case  cms = "cms"
    case  forgot_pwd_email = "forgot_pwd_email"
    case  reset_pwd_email = "reset_pwd_email"
    case  forgot_pwd_phone = "forgot_pwd_phone"
    case  reset_pwd_phone = "reset_pwd_phone"
    case  countries = "countries"
    case  states = "states"
    case  cities = "cities"
    case  notifications = "notifications_user"
    case  membership = "membership"
    case  create_sub_account = "create_sub_account"
    case  subaccount_list = "subaccount_list"
    case  transfer_subaccount_email = "transfer_subaccount_email"
    case  transfer_subaccount_phone = "transfer_subaccount_phone"
    case  genealogy_teamview = "genealogy_teamview"
    case  genealogy_treeview = "genealogy_treeview"
    case  package_info = "package_info"
    case  get_referralcode = "get_referralcode"
    case  transaction_filter = "transaction_filter"
    case  transaction_filters = "transaction_filters"
    case  transactions = "transactions"

    
    case  user_epins = "user_epins"
    case  generate_epinbarcode = "generate_epinbarcode"
    case  epin_gift = "epin_gift"
    case  create_epin = "create_epin"

    case  referral = "referral"

    case  transfer_chips = "transfer_chips"
    case  get_refercode = "get_refercode"
    case  digital_chips_websites = "games"
    case  payment = "payment_api"

    case payment_packages = "payment_packages"
    init(fromRawValue: String){
        self = Base(rawValue: fromRawValue) ?? .userRegister
    }
    
    static func valueFor(Key : String?)->Base{
        
        guard let key = Key else {
            return Base.userRegister
        }
        
        
        if let base = Base(rawValue: key) {
            return base
        }
        
        return Base.userRegister
        
    }
    
}
