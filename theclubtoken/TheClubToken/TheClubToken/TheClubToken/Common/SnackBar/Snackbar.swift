//
//  Snackbar.swift
//  snackbar
//
//  Created by Нейкович Сергей on 30.11.16.
//  Copyright © 2016 CubInCup. All rights reserved.
//

import UIKit

enum SBAnimationLength {
    case shot
    case long
}

class Snackbars: NSObject {
    
    // settings snackbar
    var snackbarHeight: CGFloat     = 65
    var backgroundColor: UIColor    = UIColor.buttonprimary
    var textColor: UIColor          = UIColor.textcolor
    var buttonColor:UIColor         = .cyan
    var buttonColorPressed:UIColor  = .gray
    var sbLenght: SBAnimationLength = .shot
    var txty = 0
    
    //private variables
    private let window = UIApplication.shared.keyWindow!
    private let snackbarView = UIView(frame: .zero)
    
    private let txt: UILabel = UILabel()
    private let btn: UIButton = UIButton()
    
    private var action: (() -> Void)? = nil
    
    var timer = Timer()
    
    override init(){
        super.init()
        
        let height = UIApplication.shared.statusBarFrame.height
        snackbarHeight = snackbarHeight + height - 20
        
        txty = Int(height - 10)
        
       // NotificationCenter.default.addObserver(self, selector: #selector(rotate), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    /// Show simple text notification
    open func createWithText(_ text: String) {
        
        setupSnackbarView()
        
        txt.text = text
        txt.textColor = textColor
        txt.frame = CGRect(x: window.frame.width * 5/100, y: CGFloat(txty), width: window.frame.width * 95/100, height: snackbarHeight)
        snackbarView.addSubview(txt)
        
        show()
    }
    
    
    /// Show snackbar with text and button
    open func createWithAction( text: String, actionTitle: String, action: @escaping () -> Void){
        self.action = action
        
        setupSnackbarView()
        
        txt.text = text
        txt.textColor = textColor
        txt.frame = CGRect(x: window.frame.width * 5/100, y: 0, width: window.frame.width * 75/100, height: snackbarHeight)
        snackbarView.addSubview(txt)
        
        btn.setTitleColor(buttonColor,  for: .normal)
        btn.setTitleColor(.gray, for: .highlighted)
        btn.setTitle(actionTitle, for: .normal)
        btn.addTarget(self, action: #selector(actionButtonPress), for: .touchUpInside)
        btn.frame = CGRect(x: window.frame.width * 73/100, y: 0, width: window.frame.width * 25/100, height: snackbarHeight)
        snackbarView.addSubview(btn)
        
        show()
    }
    
    
    
    open func show(){
        
        timer.invalidate()
        snackbarView.frame = CGRect(x: 0, y: -snackbarHeight, width: window.frame.width, height: snackbarHeight)
        
        switch sbLenght {
        case .shot:
            animateBar(2)
            
        case .long:
            animateBar(3)
            
        }
    }
    
    
    private func setupSnackbarView(){
        window.addSubview(snackbarView)
        
        snackbarView.frame = CGRect(x: 0, y: -snackbarHeight, width: window.frame.width, height: snackbarHeight)
        snackbarView.backgroundColor = self.backgroundColor
    }
    
    var statusbarhide = false
    
    fileprivate func animateBar(_ timerLength: Float){
        
        // UIApplication.shared.isStatusBarHidden = true
        
        statusbarhide = true
        
        UIView.animate(withDuration: 0.4, animations: {
            
            // if UIApplication.shared.isStatusBarHidden == false {
            
            if self.statusbarhide == false {
                
                self.snackbarView.frame = CGRect(x: 0, y: -self.snackbarHeight, width: self.window.frame.width, height: self.snackbarHeight)
                
            }
            else {
                
                self.snackbarView.frame = CGRect(x: 0, y: 0, width: self.window.frame.width, height: self.snackbarHeight)
                
            }
            
            
            self.timer = Timer.scheduledTimer(timeInterval: TimeInterval(timerLength), target: self, selector: #selector(self.hide), userInfo: nil, repeats: false)
        })
        
        
    }
    
    
    // MARK: Selectors
    
    @objc private func actionButtonPress(){
        action!()
        hide()
    }
    
    @objc private func hide(){
        
        // UIApplication.shared.isStatusBarHidden = false
        
        statusbarhide = false
        
        UIView.animate(withDuration: 0.4, animations: {
            self.snackbarView.frame = CGRect(x: 0, y: -self.snackbarHeight, width: self.window.frame.width, height: self.snackbarHeight)
        }, completion: { (check) in
            
            self.timer.invalidate()
            self.snackbarView.frame = CGRect(x: 0, y: -self.snackbarHeight, width: self.window.frame.width, height: self.snackbarHeight)
            
        })
    }
    
    @objc private func rotate(){
        self.snackbarView.frame = CGRect(x: 0, y: 0, width: self.window.frame.width, height: self.snackbarHeight)
        btn.frame = CGRect(x: window.frame.width * 73/100, y: 0, width: window.frame.width * 25/100, height: snackbarHeight)
    }
    
}


