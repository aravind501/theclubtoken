//
//  EditProfileViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 05/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CDAlertView
class EditProfileViewController: UIViewController {

    @IBOutlet weak var accountline: UIView!
    @IBOutlet weak var accountview: UIView!
    @IBOutlet weak var authenticationview: UIView!
    @IBOutlet weak var authenticationline: UIView!
    @IBOutlet weak var helpview: UIView!
    @IBOutlet weak var helpline: UIView!
    @IBOutlet weak var firstnameTxtFld: HoshiTextField!
    @IBOutlet weak var middlenameTxtFld: HoshiTextField!
    @IBOutlet weak var lastnameTxtFld: HoshiTextField!
    @IBOutlet weak var phonenumberTxtFld: HoshiTextField!
    @IBOutlet weak var emailTxtFld: HoshiTextField!
    @IBOutlet weak var accountbtn: UIButton!
    
    @IBOutlet weak var authbtn: UIButton!
    @IBOutlet weak var helpbtn: UIButton!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var accountTiLbl: UILabel!
    @IBOutlet weak var AuthedicationTiLbl: UILabel!
    @IBOutlet weak var helpTiLbl: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    
    lazy var lang = Language.default.object
    
    override func viewDidLoad() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("popleft"), object: nil)

viewprofile()
       setDesigns()
        
        
        titleLbl.text = lang.EditProfile
        accountTiLbl.text = lang.Account
        AuthedicationTiLbl.text = lang.Authentication
        helpTiLbl.text = lang.Help
        editBtn.setTitle(lang.Edit, for: .normal)
        
        firstnameTxtFld.placeholder = lang.FirstName
         middlenameTxtFld.placeholder = lang.MiddleName
        lastnameTxtFld.placeholder = lang.LastName
        phonenumberTxtFld.placeholder = lang.PhoneNumber
        emailTxtFld.placeholder = lang.Email
        
    }

   
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        
        self.popLeft()
    }

    
    func viewprofile()
    {
                    let params1 = ["userid":customer_id]
                               
        
        print(params1)
        
                                APIManager.shared.view_profile(params: params1 as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    

                                    self.firstnameTxtFld.text = response["firstname"].stringValue
                                  self.middlenameTxtFld.text = response["middlename"].stringValue
                                    self.lastnameTxtFld.text = response["lastname"].stringValue
                                    self.phonenumberTxtFld.text = response["phone"].stringValue

                                    self.emailTxtFld.text = response["email"].stringValue


        }
    }
    func setDesigns(){
        
        self.firstnameTxtFld.borderActiveColor = .textcolor
        self.firstnameTxtFld.borderInactiveColor = .buttonsecondary
        self.firstnameTxtFld.placeholderColor = .textcolor
        self.firstnameTxtFld.textColor = .textcolor
        self.firstnameTxtFld.delegate = self
        
        self.middlenameTxtFld.borderActiveColor = .textcolor
        self.middlenameTxtFld.borderInactiveColor = .buttonsecondary
        self.middlenameTxtFld.placeholderColor = .textcolor
        self.middlenameTxtFld.textColor = .textcolor
        self.middlenameTxtFld.delegate = self
       
        self.lastnameTxtFld.borderActiveColor = .textcolor
        self.lastnameTxtFld.borderInactiveColor = .buttonsecondary
        self.lastnameTxtFld.placeholderColor = .textcolor
        self.lastnameTxtFld.textColor = .textcolor
        self.lastnameTxtFld.delegate = self
        
        self.phonenumberTxtFld.borderActiveColor = .textcolor
        self.phonenumberTxtFld.borderInactiveColor = .buttonsecondary
        self.phonenumberTxtFld.placeholderColor = .textcolor
        self.phonenumberTxtFld.textColor = .textcolor
        self.phonenumberTxtFld.delegate = self

        self.emailTxtFld.borderActiveColor = .textcolor
        self.emailTxtFld.borderInactiveColor = .buttonsecondary
        self.emailTxtFld.placeholderColor = .textcolor
        self.emailTxtFld.textColor = .textcolor
        self.emailTxtFld.delegate = self
        
        
    }
    @IBAction func backbtn(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
      //  self.navigationController?.popToViewController(UserProfileListViewController, animated: true)
       // self.pushwithbackanimation(id: "UserProfileListViewController", animation: false, fromSB: Profile)

    }
    
    @IBAction func accountact(_ sender: Any) {
        
    }
    @IBAction func authenticationact(_ sender: Any) {
        
        self.push(id: "EditProfileAuthenticationViewController", animation: false, fromSB: Profile)
    }
    
    @IBAction func helpact(_ sender: Any) {
       

        self.push(id: "ProfileHelpViewController", animation: false, fromSB: Profile)
    }
    
    
    @IBAction func editact(_ sender: Any) {
        
        
        if firstnameTxtFld.text == ""
        {
            self.view.make(toast: lang.Please_Enter_First_Name)
        }
        
        
        else if lastnameTxtFld.text == ""
        {
            self.view.make(toast: lang.Please_Enter_Last_Name)
        }
        
        else if phonenumberTxtFld.text == ""
        {
            self.view.make(toast: lang.Please_Enter_Phone_Number)
        }
        else if emailTxtFld.text == ""
        {
            self.view.make(toast: lang.Please_Enter_Email_Id)
        }

            else if emailTxtFld.text?.isEmail  == false
               
            {
                self.view.make(toast: lang.Please_Enter_Valid_Email_Id)

                }

        
        else
        {
            let params1 = ["userid":customer_id,
                           "firstname":firstnameTxtFld.text!,
                           "middlename":middlenameTxtFld.text!,
                           "lastname":lastnameTxtFld.text!,
                "phone":phonenumberTxtFld.text!,
                "email":emailTxtFld.text!]
                                   
            
            print(params1)
            
                                    APIManager.shared.update_profile(params: params1 as [String : AnyObject]) { (response) in
                                        print("response params1 \(response)")
                                        print("CHeckparams")
                                        
                                        let status = response["status"].stringValue
                                        let msg = response["msg"].stringValue
                                        
                                        if status == "success"
                                        {
                                             Common.checkRemoveCdAlert()
                                                    let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                            alert.circleFillColor = UIColor.buttonprimary
                                            let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                    alert.add(action: doneAction)
                                                    alert.show() { (alert) in


                                                    }
                                        }
                                        
                                        else
                                        {
                                            let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                            alert.circleFillColor = UIColor.buttonprimary

                                            alert.autoHideTime = 2.0
                                            alert.show()

                                        }

            }
        }
        
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EditProfileViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == firstnameTxtFld
        {
            firstnameTxtFld.placeholder = lang.FirstName
        }
        
        else if textField == lastnameTxtFld
        {
            lastnameTxtFld.placeholder = lang.LastName
        }
        else if textField == middlenameTxtFld
        {
            middlenameTxtFld.placeholder = lang.MiddleName
        }
        else if textField == phonenumberTxtFld
        {
            phonenumberTxtFld.placeholder = lang.PhoneNumber
        }
        else if textField == emailTxtFld
        {
            emailTxtFld.placeholder = lang.Email
        }
        


    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        if textField.text?.count == 0 {
            
            
            
            if textField == firstnameTxtFld
            {
                firstnameTxtFld.placeholder = lang.FirstName
                self.firstnameTxtFld.borderInactiveColor = .buttonsecondary

            }
            
            else if textField == lastnameTxtFld
            {
                lastnameTxtFld.placeholder = lang.LastName
                self.lastnameTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == middlenameTxtFld
            {
                middlenameTxtFld.placeholder = lang.MiddleName
                self.middlenameTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == phonenumberTxtFld
            {
                phonenumberTxtFld.placeholder = lang.PhoneNumber
                self.phonenumberTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == emailTxtFld
            {
                emailTxtFld.placeholder = lang.Email
                self.emailTxtFld.borderInactiveColor = .buttonsecondary

            }
            
            
            
            
          
            
        }
    }
    
}
