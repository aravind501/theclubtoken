//
//  NewSubAccountViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 25/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CDAlertView
class NewSubAccountViewController: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var usernameTxtFld: HoshiTextField!
    @IBOutlet weak var referralCodeTxtFld: HoshiTextField!
    @IBOutlet weak var epinTxtFld: HoshiTextField!
    @IBOutlet weak var tickImg: UIImageView!
    @IBOutlet weak var freeAccountTiLbl: UIButton!
    
    @IBOutlet weak var registerBtn: UIButton!
    var freeaccount = true
    lazy var lang = Language.default.object
    override func viewDidLoad() {
        super.viewDidLoad()
getDesign()
        
        titleLbl.text = lang.NewSubAccount
        usernameTxtFld.placeholder = lang.UserName
        referralCodeTxtFld.placeholder = lang.ReferralCode
        epinTxtFld.placeholder = lang.EPINoptinal
        freeAccountTiLbl.setTitle(lang.FreeAccount, for: .normal)
        registerBtn.setTitle(lang.Register, for: .normal)
        // Do any additional setup after loading the view.
    }
    
    
    func getDesign()
    {
        self.usernameTxtFld.borderActiveColor = .textcolor
        self.usernameTxtFld.borderInactiveColor = .buttonsecondary
        self.usernameTxtFld.placeholderColor = .textcolor
        self.usernameTxtFld.textColor = .textcolor
        self.usernameTxtFld.delegate = self

        self.referralCodeTxtFld.borderActiveColor = .textcolor
        self.referralCodeTxtFld.borderInactiveColor = .buttonsecondary
        self.referralCodeTxtFld.placeholderColor = .textcolor
        self.referralCodeTxtFld.textColor = .textcolor
        self.referralCodeTxtFld.delegate = self
       
        
        self.epinTxtFld.borderActiveColor = .textcolor
        self.epinTxtFld.borderInactiveColor = .buttonsecondary
        self.epinTxtFld.placeholderColor = .textcolor
        self.epinTxtFld.textColor = .textcolor
        self.epinTxtFld.delegate = self

        
    }

    @IBAction func freeaccount(_ sender: Any) {
        
        if freeaccount ==  true
        {
            self.tickImg.isHidden = true
        }
        else
        {
            self.tickImg.isHidden = false

        }
        freeaccount = !freeaccount
        
    }
    @IBAction func backact(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
   
    
    @IBAction func registerAct(_ sender: Any) {
        
        if usernameTxtFld.text == ""
        {
            self.view.make(toast: lang.Please_Enter_User_Name)

        }

        else if referralCodeTxtFld.text == ""
        {
            self.view.make(toast: lang.Please_Enter_Referral_Code)

        }
        
            
        else
        {
            
            
            
            var epin = ""
            var free_account = ""
            
            
            if epinTxtFld.text != ""
            {
                epin = epinTxtFld.text!
            }
            else
            {
                epin = ""
            }
            
            
            if freeaccount == true
            {
                free_account = "yes"
            }
            else
            
            {
                free_account = "no"

            }
            
            let params1 = ["userid":customer_id,
                           "username":usernameTxtFld.text!,
                           "referral_code":referralCodeTxtFld.text!,
                           "epin":epin,
                "free_account":free_account]
                                   
            
            print(params1)
            
                                    APIManager.shared.create_sub_account(params: params1 as [String : AnyObject]) { (response) in
                                        print("response params1 \(response)")
                                        print("CHeckparams")
                                        
                                        
                                        let status = response["status"].stringValue
                                        let msg = response["message"].stringValue
                                        
                                        if status == "success"
                                        {
                                             Common.checkRemoveCdAlert()
                                                    let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                            alert.circleFillColor = UIColor.buttonprimary
                                            let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                    alert.add(action: doneAction)
                                                    alert.show() { (alert) in


                                                    }
                                        }
                                        
                                        else
                                        {
                                            let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                            alert.circleFillColor = UIColor.buttonprimary

                                            alert.autoHideTime = 2.0
                                            alert.show()

                                        }

            }
        }
    }
    


    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension NewSubAccountViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == usernameTxtFld
        {
            usernameTxtFld.placeholder = lang.UserName
        }
        
        else if textField == referralCodeTxtFld
        {
            referralCodeTxtFld.placeholder = lang.ReferralCode
        }
        else if textField == epinTxtFld
        {
            epinTxtFld.placeholder = lang.EPINoptinal
        }

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        if textField.text?.count == 0 {
            
            
            
            
            if textField == usernameTxtFld
            {
                usernameTxtFld.placeholder = lang.UserName
                self.usernameTxtFld.borderInactiveColor = .buttonsecondary

                
            }
            
            else if textField == referralCodeTxtFld
            {
                referralCodeTxtFld.placeholder = lang.ReferralCode
                self.referralCodeTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == epinTxtFld
            {
                epinTxtFld.placeholder = lang.EPINoptinal
                self.epinTxtFld.borderInactiveColor = .buttonsecondary

            }
            
        }
    }
    
}
