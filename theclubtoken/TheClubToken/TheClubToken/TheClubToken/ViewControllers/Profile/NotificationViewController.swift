//
//  NotificationViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 25/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import SwiftyJSON
class NotificationViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var notificationtitleLbl: UILabel!
    @IBOutlet weak var notificationTblView: UITableView!
    @IBOutlet weak var norecordLbl: UILabel!
    lazy var lang = Language.default.object
    var searchDatas = [JSON]()
    override func viewDidLoad() {
        super.viewDidLoad()
viewprofile()
        
        
        notificationTblView.delegate = self
        notificationTblView.dataSource = self
        self.notificationTblView.tableFooterView = UIView()

        
        notificationTblView.rowHeight = UITableView.automaticDimension
        notificationTblView.estimatedRowHeight = 133.0

        notificationtitleLbl.text = lang.Notifications
        norecordLbl.text = lang.NoRecordsFound

        
        // Do any additional setup after loading the view.
    }
    
    
    
    func viewprofile()
    {
                    let params1 = ["userid":customer_id]
                               
        
                  print(params1)
        
                                APIManager.shared.view_notification(params: params1 as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    
                                    self.searchDatas = response["notifications"].arrayValue
                                    
                                    if self.searchDatas.count > 0
                                    {
                                        self.notificationTblView.isHidden = false
                                        self.norecordLbl.isHidden = true
                                        self.notificationTblView.reloadData()

                                    }
                                    else{
                                        self.notificationTblView.isHidden = true
                                        self.norecordLbl.isHidden = false

                                    }
                                    
        }
    }
      
    
    @IBAction func backbtn(_ sender: Any) {
            
            self.navigationController?.popViewController(animated: true)

        }
        

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchDatas.count
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }



    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
               
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! notificationCell
        
        let dict = searchDatas[indexPath.row].dictionaryValue
        
        
        let title = dict["title"]?.stringValue
        let descri = dict["description"]?.stringValue
        
//        if title == "Login Success"
//        {
//            cell.ttileLbl.text = "Successful login from new IP"
//
//        }
//        else
//        {
            cell.ttileLbl.text = title
            cell.contentLbl.text = descri

        //}
//        let ip = dict["ip_address"]?.stringValue
//
//        if ip != ""
//        {
//            let ip1 = "IP address:" + ip!
//            let content = "The system has detected that your account is logged in from an unused" + "\n" + ip1
//            cell.contentLbl.text = content
//        }
        
        cell.datelbl.text = dict["date"]?.stringValue
        return cell

    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class notificationCell : UITableViewCell
{
    @IBOutlet weak var ttileLbl: UILabel!
    
    @IBOutlet weak var datelbl: UILabel!
    @IBOutlet weak var contentLbl: UILabel!
}
