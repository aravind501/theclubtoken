//
//  UIFont+LCAdditions.swift
//  Trabezah
//
//  Created by UPLOGIC on 03/10/18.
//  Copyright © 2018 UPLOGIC. All rights reserved.
//

import UIKit

extension UIFont {

    class func englishFontSet(fontSize: CGFloat) -> UIFont? {
        return UIFont(name: "OpenSans-Bold", size: fontSize)
    }
    
}
