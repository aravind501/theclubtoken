//
//  PackageViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 29/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//


import UIKit
import SwiftyJSON
import CDAlertView

class PackageViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var amountLblusd: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var amountlblbtc: UILabel!
    @IBOutlet weak var tctwalletTitle: UILabel!
    @IBOutlet weak var rewardTitleLbl: UILabel!
    @IBOutlet weak var rewardsAmountLbl: UILabel!
    @IBOutlet weak var assetTitleLbl: UILabel!
    @IBOutlet weak var assetAmntLbl: UILabel!
    @IBOutlet weak var packageBtn: UIButton!
    @IBOutlet weak var currentPacLKitlebl: UILabel!
    
    @IBOutlet weak var upgradeBtn: UIButton!
    @IBOutlet weak var paybyTiLbl: UILabel!
    @IBOutlet weak var packageTypeTitleLbl: UILabel!
    @IBOutlet weak var viewheight: NSLayoutConstraint!
    @IBOutlet weak var currencyBtn: UIButton!
    
    @IBOutlet weak var backbtnblack: UIButton!
    
    @IBOutlet weak var locationview: UIView!
    
    @IBOutlet weak var yourbalanceheight: NSLayoutConstraint!
    @IBOutlet weak var tblviewTitleLbl: UILabel!
    @IBOutlet weak var locationtableview: UITableView!

    @IBOutlet weak var yourbalanceTitle: UILabel!
    @IBOutlet weak var yourbalancerewardsTitle: UILabel!
    @IBOutlet weak var yourbalanceamntLbl: UILabel!
    @IBOutlet weak var yourbalanceassetTitle: UILabel!
    @IBOutlet weak var yourbalanceassestAmount: UILabel!
    @IBOutlet weak var balanceview: UIView!
    
    
    var packagesDatas = [JSON]()
    var currencyDatas = [JSON]()

    var packagesList = [String]()
    var currencyList = [String]()
    
    var selectedList = "packages"
    
    var packageId = ""
    var packageRewards = ""
    var currencyId = ""
    lazy var lang = Language.default.object
    var assests = ""
    
    var rewards = ""
    @IBOutlet weak var assetview: UIView!
    
    @IBOutlet weak var cryptoLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

    assetview.isHidden = true
    balanceview.isHidden = true
         
        print("current_package in Upgrade package \(current_package)")
        
        
//        if current_package == "FREE"
//        {
//
//
//            yourbalanceheight.constant = 100
//            yourbalanceassetTitle.isHidden = true
//            yourbalanceassestAmount.isHidden = true
//            assetTitleLbl.isHidden = true
//            assetAmntLbl.isHidden = true
//
//
//        }
//
//        else
//        {
//            yourbalanceheight.constant = 142
//            yourbalanceassetTitle.isHidden = false
//            yourbalanceassestAmount.isHidden = false
//            assetTitleLbl.isHidden = false
//            assetAmntLbl.isHidden = false
//
//
//        }
        
        
        locationtableview.delegate = self
        locationtableview.dataSource = self
        
        locationtableview.tableFooterView = UIView()
        locationtableview.estimatedRowHeight = 60.0
        locationtableview.rowHeight = UITableView.automaticDimension

        getpackages()

        getcurrencylist()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("createpin"), object: nil)
        
        titleLbl.text = lang.upgradePackage
        currentPacLKitlebl.text = lang.currentPackage
        packageTypeTitleLbl.text = lang.PackageType
        paybyTiLbl.text = lang.Payby
        
        yourbalanceTitle.text = lang.YourBalance
        tctwalletTitle.text = lang.PaymentAmount
        yourbalanceassetTitle.text = lang.assets
        assetTitleLbl.text = lang.assets
        packageBtn.setTitle(lang.PackageTit, for: .normal)
        currencyBtn.setTitle(lang.Currency, for: .normal)
        upgradeBtn.setTitle(lang.upgradePackage, for: .normal)
        // Do any additional setup after loading the view.
    }
    
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        self.navigationController?.popViewController(animated: true)
    }

    
    func getpackages()
    {
        
        let params1 = [":":":"]
        
        print(params1)
        
                                APIManager.shared.packages(params: params1 as [String : AnyObject]) { (response) in
                                    print("response getpackages \(response)")
                                    
                                    self.packagesDatas = response["packages"].arrayValue
                                    self.locationtableview.reloadData()

                                    

        }
    }
    
    
    func getcurrencylist()
    {
        
        let params1 = [":":":"]
        
        print(params1)
        
                                APIManager.shared.currency_list1(params: params1 as [String : AnyObject]) { (response) in
                                    print("response getcurrencylist \(response)")
                                    
                                 //   self.currencyDatas = response["currency_list"].arrayValue
                                    
                                    let dict = response["currency_list"].arrayValue
                                    
                                    if dict.count > 0
                                        
                                    {
                                    for i in 0...dict.count - 1
                                    {
                                        let dicts = dict[i].dictionaryValue
                                        let name = dicts["currency_symbol"]?.stringValue
                                        
                                                if current_package == "FREE"
                                                {
                                                    
                                                    if name != "TCT"
                                                    {
                                                        let dicts = dict[i]

                                                        self.currencyDatas.append(dicts)
                                                    }
                                                    
                                        }
                                        else
                                                {
                                                
                                                    self.currencyDatas = response["currency_list"].arrayValue

//                                                    if name == "TCT"
//                                                    {
//                                                        let dicts = dict[i]
//
//                                                        self.currencyDatas.append(dicts)
//                                                    }
                                        }
                                    }
                                    }

                                    self.locationtableview.reloadData()


        }
    }
    
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! locationcells
         
        
                if selectedList == "packages"
                {
                    let dict = packagesDatas[indexPath.row].dictionaryValue
                    
                    cell.name.text = dict["package_name"]?.stringValue

        }
        
        else
                {
                    let dict = currencyDatas[indexPath.row].dictionaryValue

                    cell.name.text = dict["currency_symbol"]!.stringValue + " " + "(" + dict["currency_name"]!.stringValue + ")"

        }
         

        // cell.name.text = stringarray[indexPath.row]
         return cell
     }
     
     
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return UITableView.automaticDimension
     }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectedList == "packages"
        {
            return packagesDatas.count

        }
        else
        {
            return currencyDatas.count

        }
     }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if selectedList == "packages"
        {
            let dict = packagesDatas[indexPath.row].dictionaryValue
            
            let name = dict["package_name"]?.stringValue
            packageId = dict["package_id"]!.stringValue
            packageRewards = dict["personal_rewards"]!.stringValue

            self.packageBtn.setTitle(name, for: .normal)
        }
        else
        {
            let dict = currencyDatas[indexPath.row].dictionaryValue
            let name = dict["currency_symbol"]?.stringValue
            self.currencyBtn.setTitle(name, for: .normal)
            currencyId = dict["id"]!.stringValue

        }
        
        
        
        
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            
            self.locationview.isHidden = true
            
            self.backbtnblack.isHidden = true
        })
        
        
        let package = self.packageBtn.currentTitle
        let currencyValue = self.currencyBtn.currentTitle
        
        if package! != lang.PackageTit && currencyValue! != lang.Currency
        {
            create_epin_show(fromtype: currencyValue!)
        }
        
    }
    
    @IBAction func backact(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func viewcloseact(_ sender: Any) {
        
        
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            
            self.locationview.isHidden = true
            
            self.backbtnblack.isHidden = true
            
        })
        
        
    }
    
    @IBAction func selectamountBtn(_ sender: Any) {
        
selectedList = "packages"
        tblviewTitleLbl.text = "Select \(lang.PackageTit)"
        self.viewheight.constant = 380

        self.locationtableview.reloadData()
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            
            self.locationview.isHidden = false
            
            self.backbtnblack.isHidden = false
            
        })
        
    }
    
    @IBAction func selectCurrencyBtn(_ sender: Any) {
        selectedList = "currency"
        tblviewTitleLbl.text = "Select \(lang.Currency)"

        if current_package == "FREE"
        {
        self.viewheight.constant = 250
        }
        else
        {
           // self.viewheight.constant = 130
            self.viewheight.constant = 270


        }
        
        self.locationtableview.reloadData()
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            
            self.locationview.isHidden = false
            
            self.backbtnblack.isHidden = false
            
        })
        
    }
  
    
    func create_epin_show(fromtype : String)
    {
        
        
        
        var pack = ""
     
//        if fromtype == "TCT"
//
//        {
//            pack = "TCT"
//        }
//        else
//        {
//            pack = "BTC"
//
//        }
//
        
        if current_package == "FREE"
        {
            pack = "BTC"
        }
        else
        {
            pack = "TCT"

        }
        
        
        let params1 = ["userid":customer_id,
                       "package":packageId,
                       "currency": currencyId,
                       "rewards":packageRewards]
        
        print(params1)
        
        APIManager.shared.upgrade_package_show(params: params1 as [String : AnyObject], from: pack) { (response) in
                                    print("response upgrade_package_show \(response)")
                                    
            
            var dict = [String:JSON]()

            if current_package == "FREE"

            {
                dict = response["upgrade_package_cryptoshow"].dictionaryValue



            }
            else
            {
                dict = response["upgrade_package_show"].dictionaryValue

            }

            
                                    let cur = self.currencyBtn.currentTitle
                                    self.amountLblusd.text = "\(self.lang.AmountinUSD)" + "\n" + dict["Amount_in_USD"]!.stringValue

            self.assetview.isHidden = false
            self.balanceview.isHidden = false
                                  
            if let id = dict["Amount_in_BTC"]
                                    {
                                        self.amountlblbtc.text = "\(self.lang.AmountrequiredBTC)" + " " + cur! + "\n" + dict["Amount_in_BTC"]!.stringValue
                                        
                                        self.yourbalancerewardsTitle.text = "BTC"
                                        
                                      if let id  = dict["Your_balance_assets"]
                                      {

                                        self.yourbalanceamntLbl.text = dict["Your_balance_assets"]!.stringValue
                                        }
                                        
                                        self.rewardTitleLbl.text = "BTC"
                                        
                                        if let id  = dict["Payment_amount"]
                                        {
                                            self.rewardsAmountLbl.text = dict["Payment_amount"]!.stringValue
                                            self.assests = dict["Payment_amount"]!.stringValue


                                        }
                                        
                                        if let id = dict["Payment_Amount_assets"]
                                        {
                                            self.rewardsAmountLbl.text = dict["Payment_Amount_assets"]!.stringValue
                                            self.assests = dict["Payment_Amount_assets"]!.stringValue


                                        }

                                        //self.cryptoLbl.text = dict["Amount_in_BTC"]!.stringValue
                                        self.yourbalanceheight.constant = 100
                                        self.yourbalanceassetTitle.isHidden = true
                                        self.yourbalanceassestAmount.isHidden = true
                                        self.assetTitleLbl.isHidden = true
                                        self.assetAmntLbl.isHidden = true


                                        
                                    }
                                    
                                    if let id = dict["Amount_in_ETH"]
                                    {
                                        self.amountlblbtc.text = "\(self.lang.AmountrequiredBTC)" + " " + cur! + "\n" + dict["Amount_in_ETH"]!.stringValue
                                       
                                        self.yourbalancerewardsTitle.text = "ETH"
                                        if let id  = dict["Your_balance_assets"]
                                        {

                                          self.yourbalanceamntLbl.text = dict["Your_balance_assets"]!.stringValue
                                          }


                                        self.rewardTitleLbl.text = "ETH"
                                        
                                        if let id  = dict["Payment_amount"]
                                        {
                                            self.rewardsAmountLbl.text = dict["Payment_amount"]!.stringValue
                                            self.assests = dict["Payment_amount"]!.stringValue


                                        }
                                        
                                        if let id = dict["Payment_Amount_assets"]
                                        {
                                            self.rewardsAmountLbl.text = dict["Payment_Amount_assets"]!.stringValue
                                            self.assests = dict["Payment_Amount_assets"]!.stringValue


                                        }
                                        self.yourbalanceheight.constant = 100
                                        self.yourbalanceassetTitle.isHidden = true
                                        self.yourbalanceassestAmount.isHidden = true
                                        self.assetTitleLbl.isHidden = true
                                        self.assetAmntLbl.isHidden = true


                                    }
                                    
                                    if let id = dict["Amount_in_TCT"]
                                    {
                                        self.amountlblbtc.text = "\(self.lang.AmountrequiredBTC)" + " " + cur! + "\n" + dict["Amount_in_TCT"]!.stringValue
                                       
                                        self.rewardTitleLbl.text = self.lang.rewards
                                        self.assests = dict["Payment_Amount_assets"]!.stringValue
                                        self.rewards = dict["Payment_Amount_rewards"]!.stringValue

                                        self.yourbalancerewardsTitle.text = self.lang.rewards
                                        self.yourbalanceamntLbl.text = dict["Your_balance_rewards"]!.stringValue

                                        self.yourbalanceassestAmount.text = dict["Your_balance_assets"]!.stringValue
                                        
                                        
                                        self.rewardsAmountLbl.text = dict["Payment_Amount_rewards"]?.stringValue
                                        self.assetAmntLbl.text = dict["Payment_Amount_assets"]?.stringValue
                                    
                                        self.yourbalanceheight.constant = 142
                                        self.yourbalanceassetTitle.isHidden = false
                                        self.yourbalanceassestAmount.isHidden = false
                                        self.assetTitleLbl.isHidden = false
                                        self.assetAmntLbl.isHidden = false

                                        

                                    }

                                    if let id = dict["Amount_in_USDT"]
                                    {
                                        self.amountlblbtc.text = "\(self.lang.AmountrequiredBTC)" + " " + cur! + "\n" + dict["Amount_in_USDT"]!.stringValue
                                       
                                        self.yourbalancerewardsTitle.text = "USDT"
                                       
                                        if let id  = dict["Your_balance_assets"]
                                        {

                                          self.yourbalanceamntLbl.text = dict["Your_balance_assets"]!.stringValue
                                          }
                                        
                                        

                                        self.rewardTitleLbl.text = "USDT"
                                        if let id  = dict["Payment_amount"]
                                        {
                                            self.rewardsAmountLbl.text = dict["Payment_amount"]!.stringValue
                                            self.assests = dict["Payment_amount"]!.stringValue

                                        }
                                        
                                        if let id = dict["Payment_Amount_assets"]
                                        {
                                            self.rewardsAmountLbl.text = dict["Payment_Amount_assets"]!.stringValue
                                            self.assests = dict["Payment_Amount_assets"]!.stringValue


                                        }
                                        self.yourbalanceheight.constant = 100
                                        self.yourbalanceassetTitle.isHidden = true
                                        self.yourbalanceassestAmount.isHidden = true
                                        self.assetTitleLbl.isHidden = true
                                        self.assetAmntLbl.isHidden = true

                                        

                                    }



        }
        
    }
    
    func hideshow(lb1:Bool,lb2:Bool)
    {
        self.assetview.isHidden = false

        self.cryptoLbl.isHidden = lb1
        self.rewardTitleLbl.isHidden = lb2
        self.rewardsAmountLbl.isHidden = lb2
        self.assetAmntLbl.isHidden = lb2
        self.assetTitleLbl.isHidden = lb2

    }
    
    
    @IBAction func upgradepackageact(_ sender: Any) {
        
        
        if packageBtn.currentTitle == lang.PackageTit
        {
            self.view.make(toast: "Please Select Package")
        }
        else if currencyBtn.currentTitle == lang.Currency
        {
            self.view.make(toast: "Please Select Currency")

        }
       
        else
        {
            
            
            let package = self.currencyBtn.currentTitle

            var pack = ""
         
            var params1 = ["userid":customer_id,
                           "package":packageId,
                           "currency": currencyId,
                           "assets":assests]

            if package == "TCT"

            {
                params1["rewards"] = rewards
            }
            else
            {

            }
            

            
            if current_package == "FREE"
            {
                pack = "BTC"
            }
            else
            {
                pack = "TCT"


            }

            

            

        print(params1)
        
            APIManager.shared.upgrade_package(params: params1 as [String : AnyObject], from: pack) { (response) in
                                  
                
                print("response upgrade_package \(response)")
                                  


                var msg = ""
                
                var status = ""
                
                if current_package == "FREE"

                {
                    status = response["upgrade_package_crypto"]["status"].stringValue
                    msg = response["upgrade_package_crypto"]["message"].stringValue


                }
                else
                {
                    status = response["upgrade_package"]["status"].stringValue
                    msg = response["upgrade_package"]["message"].stringValue


                }



                                    
                                    if status == "success"
                                    {
                                        
                                        
                                         Common.checkRemoveCdAlert()
                                                let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                        alert.circleFillColor = UIColor.buttonprimary
                                        let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                alert.add(action: doneAction)
                                                alert.show() { (alert) in


                                                    self.navigationController?.popViewController(animated: true)

                                                }
                                    }
                                    
                                    else
                                    {
                                        let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                        alert.circleFillColor = UIColor.buttonprimary

                                        alert.autoHideTime = 2.0
                                        alert.show()

                                    }

        }
        }

    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
