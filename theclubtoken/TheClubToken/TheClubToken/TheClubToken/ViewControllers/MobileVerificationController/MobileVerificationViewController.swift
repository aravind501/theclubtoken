//
//  MobileVerificationViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 26/04/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import CountryPickerView
import libPhoneNumber_iOS
import CDAlertView

class MobileVerificationViewController: UIViewController {

    @IBOutlet weak var verifyBtn: UIButton!
    @IBOutlet weak var sendVerificationCodeBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var bottomview: UIView!
    @IBOutlet weak var mobileTxtffld: UITextField!
    @IBOutlet weak var verificationTxtfld: UITextField!

    @IBOutlet weak var countryImg: UIImageView!
    @IBOutlet weak var countryCodeLbl: UILabel!
    
    let countryPickerView = CountryPickerView()

    var verificationCode = ""
    lazy var lang = Language.default.object
    override func viewDidLoad() {
        super.viewDidLoad()

        mobileTxtffld.placeholder = lang.MobileNumber
        verificationTxtfld.placeholder = lang.EnterVerificationCode
        bottomview.clipsToBounds = true
        bottomview.layer.cornerRadius = 50

        if #available(iOS 11.0, *) {
            bottomview.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        setDesigns()

        titleLbl.text = lang.PleaseEntermobile
        sendVerificationCodeBtn.setTitle(lang.SendVerificationcode, for: .normal)
        verifyBtn.setTitle(lang.Verify, for: .normal)
        
        countryPickerView.delegate = self
        countryPickerView.dataSource = self

        countryImg.image = #imageLiteral(resourceName: "india")

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        
        if #available(iOS 11.0, *){
            
        }else{
           let rectShape = CAShapeLayer()
           rectShape.bounds = view.frame
           rectShape.position = view.center
           rectShape.path = UIBezierPath(roundedRect: view.bounds,    byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 50 , height: 50)).cgPath
           bottomview.layer.mask = rectShape
    }
    
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        mobileTxtffld.text = ""
        verificationCode = ""
        verificationTxtfld.text = ""
    }
    func setDesigns(){
        
        
//        self.mobileTxtffld.borderActiveColor = .clear
//        self.mobileTxtffld.borderInactiveColor = .clear
//        self.mobileTxtffld.placeholderColor = .textcolor
//        self.mobileTxtffld.textColor = .textcolor
//        self.mobileTxtffld.delegate = self
//
//        self.verificationTxtfld.borderActiveColor = .clear
//        self.verificationTxtfld.borderInactiveColor = .clear
//        self.verificationTxtfld.placeholderColor = .textcolor
//        self.verificationTxtfld.textColor = .textcolor
//        self.verificationTxtfld.delegate = self

       
    }
   
    @IBAction func backact(_ sender: Any) {
        
        self.popLeft()
    }
    
    
    @IBAction func countryBtnAct(_ sender: Any) {
        
        countryPickerView.showCountriesList(from: self)

    }
    @IBAction func sendverificationcodeact(_ sender: Any) {
        
        let mob = self.mobileTxtffld.text!
        let country = self.countryCodeLbl.text!

        var isValidNumber: Bool! = false

        let phoneUtil = NBPhoneNumberUtil.sharedInstance()
        
        print("countrycountry \(country)")
        
        
        if country.contains("+") {
            
            let newcode = country.replacingOccurrences(of: "+", with: "")
            
            let ioscode = phoneUtil?.getRegionCode(forCountryCode: NSNumber(value:Int(newcode)!))
            
            var phoneNumber: NBPhoneNumber!
            
            do {
                phoneNumber = try phoneUtil!.parse(mob, defaultRegion: ioscode)
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            
            isValidNumber = phoneUtil?.isValidNumber(phoneNumber)
            
            print("code \(newcode) \(phoneNumber)")
            
        }
        
        print("isValidNumber \(isValidNumber)")
        

        
        
        if mobileTxtffld.text == ""
        {
            self.view.make(toast: lang.Please_Enter_Mobile_Number)
        }
       
        else if !isValidNumber {
            self.view.make(toast: lang.Please_Enter_Valid_Mobile_Number)

        }

        else
        {
            
                        let params1 = ["userid":customer_id,
                                       "country_code":countryCodeLbl.text,
                                       "phone":mobileTxtffld.text!]
                        
                                    print(params1)
                                    APIManager.shared.phone_otp_signup(params: params1 as [String : AnyObject]) { (response) in
                                        print("response params1 \(response)")
                                        print("CHeckparams")
                                        

                                            let msg = response["message"].stringValue
                                            let status = response["status"].stringValue

                                                
                                                if status == "success" {
                                                    
                                                    self.verificationCode = response["otpcode"].stringValue
                                                    Common.checkRemoveCdAlert()
                                                           let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                                   alert.circleFillColor = UIColor.buttonprimary
                                                   let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                           alert.add(action: doneAction)
                                                           alert.show() { (alert) in

                                                           }

                                                   
                                                }
                                                    
                                                else {
                                                    self.verificationCode = ""
                                                    let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                                    alert.circleFillColor = UIColor.buttonprimary

                                                    alert.autoHideTime = 2.0
                                                    alert.show()

                                                    
                                        }
                                                
            }

        }
        
       }
      
    @IBAction func verifycodeact(_ sender: Any) {
        
        
        
        if verificationTxtfld.text == ""
        {
            self.view.make(toast: lang.Please_Enter_Verification_Code)
        }
        
        else if verificationTxtfld.text != verificationCode
        {
            self.view.make(toast: lang.Verification_Code_Mismatch)

        }
        
        else
        {
                        let params1 = ["userid":customer_id,
                                       "country_code":countryCodeLbl.text!,
                                       "phone":mobileTxtffld.text!,
                                       "verifycode":verificationTxtfld.text!]
                        
                                    print(params1)
            
                                    APIManager.shared.verify_phone(params: params1 as [String : AnyObject]) { (response) in
                                        print("response params1 \(response)")
                                        print("CHeckparams")
                                        

                                            let msg = response["message"].stringValue
                                            let status = response["status"].stringValue

                                                
                                                if status == "success" {
                                                    
                                
                                                    Common.checkRemoveCdAlert()
                                                           let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                                   alert.circleFillColor = UIColor.buttonprimary
                                                   let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                           alert.add(action: doneAction)
                                                           alert.show() { (alert) in

                                                            self.push(id: "EmailVerificationViewController", animation: true, fromSB: Login)

                                                           }

                                                   
                                                }
                                                    
                                                else {
                                                    
                                                    let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                                    alert.circleFillColor = UIColor.buttonprimary

                                                    alert.autoHideTime = 2.0
                                                    alert.show()

                                                    
                                        }
                                                
            }
        }
        
        

        
       }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//extension MobileVerificationViewController : UITextFieldDelegate {
//
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//
//        return textField.resignFirstResponder()
//    }
//
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//
//        if textField == mobileTxtffld
//        {
//        mobileTxtffld.placeholder = "Enter Mobile Number"
//        }
//
//        else if textField == verificationTxtfld
//        {
//        verificationTxtfld.placeholder = "Enter Verification Code"
//        }
//
//    }
//
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField.text?.count == 0 {
//
//
//            if textField == mobileTxtffld
//                   {
//                   mobileTxtffld.placeholder = "Enter Mobile Number"
//                    self.mobileTxtffld.borderInactiveColor = .clear
//
//                   }
//
//                   else if textField == verificationTxtfld
//                   {
//                   verificationTxtfld.placeholder = "Enter Verification Code"
//                    self.verificationTxtfld.borderInactiveColor = .clear
//
//                   }
//
//
//        }
//    }
//
//}

extension MobileVerificationViewController: CountryPickerViewDelegate, CountryPickerViewDataSource {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        // Only countryPickerInternal has it's delegate set
        let message = "Name: \(country.name) \nCode: \(country.code) \nPhone: \(country.phoneCode)"
        
        print("Selected Country \(message)")
        self.countryCodeLbl.text = country.phoneCode
       self.countryImg.image = country.flag
    
    }
}
