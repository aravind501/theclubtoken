//
//  EpinBarCodeViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 28/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import SDWebImage
class EpinBarCodeViewController: UIViewController {

    @IBOutlet weak var packageLbl: UILabel!
    @IBOutlet weak var codeLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var qrImg: UIImageView!
    
    var package = ""
    var status = "Available"
    var code = ""
    var barcode = ""
    
    var from = ""
    
    @IBOutlet weak var copyBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        packageLbl.text = "Package : " + package
        codeLbl.text = code
        statusLbl.text = status
        
        copyBtn.setImage(UIImage(named: "copy")?.withRenderingMode(.alwaysTemplate),
                                      for: .normal)
        copyBtn.tintColor = UIColor.white


        print("barcodeString \(barcode)")
        self.qrImg.sd_setImage(with: URL(string: barcode)) { (image, error, cache, urls) in
            if (error != nil) {
            } else {
                self.qrImg.image = image
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeact(_ sender: Any) {
        
        if from == "createpin"
        {
            NotificationCenter.default.post(name: Notification.Name("createpin"), object: nil)
            self.dismiss(animated: false, completion: nil)

        }
        else
        {
            self.dismiss(animated: false, completion: nil)

        }
    }
    @IBAction func copyact(_ sender: UIButton) {
   
        sender.blink()
        self.view.make(toast: "Epin Code Copied")

        UIPasteboard.general.string = codeLbl.text

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
