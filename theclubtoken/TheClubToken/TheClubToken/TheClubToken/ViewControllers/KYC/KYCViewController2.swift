//
//  KYCViewController2.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 03/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import DropDown
import CDAlertView
import RSSelectionMenu
class KYCViewController2: UIViewController {

    @IBOutlet weak var nextLbl: UILabel!
    @IBOutlet weak var previousLbl: UILabel!
    @IBOutlet weak var emptyTxtFld: HoshiTextField!
    @IBOutlet weak var residentialAddrLbl: UILabel!
    @IBOutlet weak var addressTxtView: UITextView!
    @IBOutlet weak var citytownTxtfFld: HoshiTextField!
    @IBOutlet weak var postalcodeTxtFld: HoshiTextField!
    @IBOutlet weak var downarrow1: UIImageView!
    @IBOutlet weak var downarrow2: UIImageView!
    @IBOutlet weak var nationalityTxtFld: HoshiTextField!
    @IBOutlet weak var countryTxtFld: HoshiTextField!
    @IBOutlet weak var downarrow3: UIImageView!
    @IBOutlet weak var regionstateTxtFld: HoshiTextField!
    @IBOutlet weak var downarrow4: UIImageView!
    @IBOutlet weak var bottomview: UIView!
    @IBOutlet weak var bottomviewheight: NSLayoutConstraint!
    lazy var lang = Language.default.object
    var selectedCountry = ""
    var selectedState = ""
    var countryArray = [String]()
    var statesArray = [String]()
    var selectedCountryNames: [String] = []
    var selectedStatesNames: [String] = []
    var selectedNationality: [String] = []

    let dropDown = DropDown()
    let dropDown1 = DropDown()

    override func viewDidLoad() {
        super.viewDidLoad()

        setDesigns()
        
        previousLbl.text = lang.Previous
        nextLbl.text = lang.Next
        
        addressTxtView.text = lang.ResidentialAddress
        citytownTxtfFld.placeholder = lang.CityTown
        postalcodeTxtFld.placeholder = lang.PostalCode
        nationalityTxtFld.placeholder = lang.Nationality
        countryTxtFld.placeholder = lang.Country
        regionstateTxtFld.placeholder = lang.RegionState
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func setDesigns(){
        
        self.emptyTxtFld.borderActiveColor = .clear
        self.emptyTxtFld.borderInactiveColor = .clear
        self.emptyTxtFld.placeholderColor = .clear
        self.emptyTxtFld.textColor = .clear
        self.emptyTxtFld.delegate = self
        
        self.citytownTxtfFld.borderActiveColor = .textcolor
        self.citytownTxtfFld.borderInactiveColor = .buttonsecondary
        self.citytownTxtfFld.placeholderColor = .textcolor
        self.citytownTxtfFld.textColor = .textcolor
        self.citytownTxtfFld.delegate = self
       
        self.postalcodeTxtFld.borderActiveColor = .textcolor
        self.postalcodeTxtFld.borderInactiveColor = .buttonsecondary
        self.postalcodeTxtFld.placeholderColor = .textcolor
        self.postalcodeTxtFld.textColor = .textcolor
        self.postalcodeTxtFld.delegate = self
        
        self.nationalityTxtFld.borderActiveColor = .textcolor
        self.nationalityTxtFld.borderInactiveColor = .buttonsecondary
        self.nationalityTxtFld.placeholderColor = .textcolor
        self.nationalityTxtFld.textColor = .textcolor
        self.nationalityTxtFld.delegate = self

        self.countryTxtFld.borderActiveColor = .textcolor
        self.countryTxtFld.borderInactiveColor = .buttonsecondary
        self.countryTxtFld.placeholderColor = .textcolor
        self.countryTxtFld.textColor = .textcolor
        self.countryTxtFld.delegate = self
        
        self.regionstateTxtFld.borderActiveColor = .textcolor
        self.regionstateTxtFld.borderInactiveColor = .buttonsecondary
        self.regionstateTxtFld.placeholderColor = .textcolor
        self.regionstateTxtFld.textColor = .textcolor
        self.regionstateTxtFld.delegate = self
        
       
        downarrow1.image = downarrow1.image?.withRenderingMode(.alwaysTemplate)
        downarrow1.tintColor = UIColor.buttonsecondary

        downarrow2.image = downarrow2.image?.withRenderingMode(.alwaysTemplate)
        downarrow2.tintColor = UIColor.buttonsecondary

        downarrow3.image = downarrow3.image?.withRenderingMode(.alwaysTemplate)
        downarrow3.tintColor = UIColor.buttonsecondary

        downarrow4.image = downarrow4.image?.withRenderingMode(.alwaysTemplate)
        downarrow4.tintColor = UIColor.buttonsecondary

        
        addressTxtView.text = lang.ResidentialAddress
        residentialAddrLbl.isHidden = true
        residentialAddrLbl.text = lang.ResidentialAddress
        addressTxtView.font = UIFont(name: "Montserrat-Regular", size: 12)
        addressTxtView.delegate = self

        bottomview.backgroundColor = UIColor.buttonsecondary
        bottomviewheight.constant = 1
        
        countryList()
        
    }
    
    
    
    
    func countryList()
    {
                    let params1 = ["":""]
                               
        
        print(params1)
        
                                APIManager.shared.countriesList(params: params1 as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    

                                    let dicts = response["countries_list"].arrayValue
                                    
                                    for dict in dicts
                                    {
                                        let name = dict["name"].stringValue
                                        self.countryArray.append(name)
                                    }

        }
    }
   
    
    func stateList()
    {
                    let params1 = ["country_name":selectedCountry]
                               
        
        print(params1)
        
                                APIManager.shared.statesList(params: params1 as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    print("CHeckparams")
                                    

                                    let dicts = response["states_list"].arrayValue
                                    
                                    for dict in dicts
                                    {
                                        let name = dict["name"].stringValue
                                        self.statesArray.append(name)
                                    }

        }
    }
    
    @IBAction func backact(_ sender: Any) {
       // self.popLeftWithoutAnimation()
        
        let mainViewControllerVC = self.navigationController?.viewControllers.first(where: { (viewcontroller) -> Bool in
            return viewcontroller is UserProfileListViewController
        })
        if let mainViewControllerVC = mainViewControllerVC {
            navigationController?.popToViewController(mainViewControllerVC, animated: true)
        }
    }
    
    @IBAction func postalcodeact(_ sender: Any) {
    }
    @IBAction func nationalityact(_ sender: Any) {
        
        
           let selectionMenu = RSSelectionMenu(selectionStyle: .single, dataSource:countryArray) { (cell, name, indexPath) in

               cell.textLabel?.text = name
           }
           
           selectionMenu.showSearchBar { [weak self] (searchText) -> ([String]) in

           // return filtered array based on any condition
           // here let's return array where name starts with specified search text

           return self?.countryArray.filter({ $0.lowercased().hasPrefix(searchText.lowercased()) }) ?? []
           }
           
           
         
           
           selectionMenu.rightBarButtonTitle = "OK"

           selectionMenu.cellSelectionStyle = .tickmark
          
        selectionMenu.title = lang.SelectNationality
        
          // selectionMenu.show(style: .popover(sourceView: sender, size: nil), from: self)

           selectionMenu.show(style: .present, from: self)

        selectionMenu.setSelectedItems(items: selectedNationality) { (name, index, selected, selectedItems) in
               
               print("Selected index \(index)")
              print("selected or deslected \(selected)")
              
                        
               self.selectedNationality = selectedItems

            self.nationalityTxtFld.text = name
                      
           
           }

    }
    @IBAction func countryact(_ sender: Any) {
    
        
        
           
           let selectionMenu = RSSelectionMenu(selectionStyle: .single, dataSource:countryArray) { (cell, name, indexPath) in

               cell.textLabel?.text = name
           }
           
           selectionMenu.showSearchBar { [weak self] (searchText) -> ([String]) in

           // return filtered array based on any condition
           // here let's return array where name starts with specified search text

           return self?.countryArray.filter({ $0.lowercased().hasPrefix(searchText.lowercased()) }) ?? []
           }
           
           
         
           
           selectionMenu.rightBarButtonTitle = "OK"

           selectionMenu.cellSelectionStyle = .tickmark
        selectionMenu.title = lang.SelectCountry
          // selectionMenu.show(style: .popover(sourceView: sender, size: nil), from: self)

           selectionMenu.show(style: .present, from: self)

        selectionMenu.setSelectedItems(items: selectedCountryNames) { (name, index, selected, selectedItems) in
               
               print("Selected index \(index)")
              print("selected or deslected \(selected)")
              
                        
               self.selectedCountryNames = selectedItems

            self.selectedCountry = name!
            self.countryTxtFld.text = name
                      
            self.stateList()
           
           }
        
    }
    
    @IBAction func regionstateact(_ sender: Any) {
        
        
        if statesArray.count > 0
        
        {
           let selectionMenu = RSSelectionMenu(selectionStyle: .single, dataSource:statesArray) { (cell, name, indexPath) in

               cell.textLabel?.text = name
           }
           
           selectionMenu.showSearchBar { [weak self] (searchText) -> ([String]) in

           // return filtered array based on any condition
           // here let's return array where name starts with specified search text

           return self?.countryArray.filter({ $0.lowercased().hasPrefix(searchText.lowercased()) }) ?? []
           }
           
           
         
           
           selectionMenu.rightBarButtonTitle = "OK"

           selectionMenu.cellSelectionStyle = .tickmark
            selectionMenu.title = lang.SelectState

           selectionMenu.show(style: .present, from: self)

        selectionMenu.setSelectedItems(items: selectedStatesNames) { (name, index, selected, selectedItems) in
               
               print("Selected index \(index)")
              print("selected or deslected \(selected)")
              
                        
               self.selectedStatesNames = selectedItems

            self.selectedState = name!
            
            self.regionstateTxtFld.text = name
                         
           
           }
        }
    }
    
    @IBAction func previousact(_ sender: Any) {
      //  self.popLeftWithoutAnimation()
        
        let mainViewControllerVC = self.navigationController?.viewControllers.first(where: { (viewcontroller) -> Bool in
            return viewcontroller is KYCViewController1
        })
        if let mainViewControllerVC = mainViewControllerVC {
            navigationController?.popToViewController(mainViewControllerVC, animated: false)
        }
        
    }
    @IBAction func nextact(_ sender: Any) {
        

        if addressTxtView.text == lang.ResidentialAddress
        {
            self.view.make(toast: lang.PleaseEnterResidentialAddress)
        }
        else if citytownTxtfFld.text == ""
        {
            self.view.make(toast: lang.PleaseEnterCity)

        }
        
        else if postalcodeTxtFld.text == ""
        {
            self.view.make(toast: lang.PleaseEnterPostalCode)

        }

        
        else if nationalityTxtFld.text == ""
        {
            self.view.make(toast: lang.PleaseSelectNationality)

        }
        
        else if countryTxtFld.text == ""
        {
            self.view.make(toast: lang.PleaseSelectCountry)

        }
        else if regionstateTxtFld.text == ""
        {
            self.view.make(toast: lang.PleaseSelectState)

        }

else
        
        {
            
            let params1 = ["userid":customer_id,
                           "address":addressTxtView.text!,
                           "city":citytownTxtfFld.text!,
                           "postal_code":postalcodeTxtFld.text!,
                "nationality":nationalityTxtFld.text!,
                "country":countryTxtFld.text!,
                "state":regionstateTxtFld.text!]
            
            print(params1)
            
                                    APIManager.shared.kyc_step2(params: params1 as [String : AnyObject]) { (response) in
                                        print("response params1 \(response)")
                                        print("CHeckparams")
                                        
                                        let status = response["status"].stringValue
                                        let msg = response["msg"].stringValue
                                        
                                        if status == "success"
                                        {
                                             Common.checkRemoveCdAlert()
                                                    let alert = CDAlertView(title: AppName, message: msg, type: .success)
                                            alert.circleFillColor = UIColor.buttonprimary
                                            let doneAction = CDAlertViewAction(title: "OK", font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
                                                    alert.add(action: doneAction)
                                                    alert.show() { (alert) in

                                                        self.push(id: "KYCViewController3", animation: false, fromSB: Profile)

                                                    }
                                        }
                                        
                                        else
                                        {
                                            let alert = CDAlertView(title: AppName, message: msg, type: .error)
                                            alert.circleFillColor = UIColor.buttonprimary

                                            alert.autoHideTime = 2.0
                                            alert.show()

                                        }

            }
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension KYCViewController2 : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == citytownTxtfFld
        {
        citytownTxtfFld.placeholder = lang.CityTown
        }

        else if textField == postalcodeTxtFld
        {
        postalcodeTxtFld.placeholder = lang.PostalCode
        }
        else if textField == nationalityTxtFld
        {
        nationalityTxtFld.placeholder = lang.Nationality
        }
        else if textField == countryTxtFld
        {
        countryTxtFld.placeholder = lang.Country
        }
        else if textField == regionstateTxtFld
        {
            regionstateTxtFld.placeholder = lang.RegionState
        }
        


    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        if textField.text?.count == 0 {
            
            
//            if textField == addre
//            {
//            citytownTxtfFld.placeholder = "City/Town"
//                self.citytownTxtfFld.borderInactiveColor = .buttonsecondary
//
//            }
            
            if textField == citytownTxtfFld
            {
                citytownTxtfFld.placeholder = lang.CityTown
                self.citytownTxtfFld.borderInactiveColor = .buttonsecondary

            }
            
            else if textField == postalcodeTxtFld
            {
            postalcodeTxtFld.placeholder = lang.PostalCode
                self.postalcodeTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == nationalityTxtFld
            {
            nationalityTxtFld.placeholder = lang.Nationality
                self.nationalityTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == countryTxtFld
            {
            countryTxtFld.placeholder = lang.Country
                self.countryTxtFld.borderInactiveColor = .buttonsecondary

            }
            else if textField == regionstateTxtFld
            {
            regionstateTxtFld.placeholder = lang.RegionState
                self.regionstateTxtFld.borderInactiveColor = .buttonsecondary

            }
            
            
          
            
        }
    }
    
}

extension KYCViewController2 : UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == lang.ResidentialAddress {
            textView.text = nil
            UIView.transition(with: self.view, duration: 0.325, options: .curveEaseInOut, animations: {

                self.bottomview.backgroundColor = UIColor.white
                self.residentialAddrLbl.isHidden = false
                self.bottomviewheight.constant = 2

            }) { finished in

                // completion

            }
            textView.font = UIFont(name: "Montserrat-Regular", size: 17)

        }
    }
   
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = lang.ResidentialAddress
            UIView.transition(with: self.view, duration: 0.325, options: .curveLinear, animations: {

                self.bottomview.backgroundColor = UIColor.buttonsecondary
                self.residentialAddrLbl.isHidden = true
                self.bottomviewheight.constant = 1

            }) { finished in

                // completion

            }

            textView.font = UIFont(name: "Montserrat-Regular", size: 12)


        }
    }

}
