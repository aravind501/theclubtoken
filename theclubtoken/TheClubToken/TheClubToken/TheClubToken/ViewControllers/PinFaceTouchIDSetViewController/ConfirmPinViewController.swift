//
//  ConfirmPinViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 07/05/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import  CDAlertView
class ConfirmPinViewController: UIViewController {

    @IBOutlet weak var setbtnoutlet: UIButton!
    
    @IBOutlet var pinselectionview: [UIView]!
    @IBOutlet var pintypebuttons: [UIButton]!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    @IBOutlet weak var lbl6: UILabel!
    @IBOutlet weak var activeinactivebtn: UIButton!
    @IBOutlet weak var passcodeview: UIView!
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var backbtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    
    var pinentered = ""
    lazy var lang = Language.default.object
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        titleLbl.text = lang.confirmSixdigitPIN
        setbtnoutlet.setTitle(lang.set, for: .normal)
        //create PasswordContainerView
        
    }
    
    func colorupdate(from:String)
    {
        if from == "delete"
        {
            if lbl6.text == ""
            {
                lbl6.backgroundColor = UIColor.clear
            }
            
              if lbl5.text == ""
            {
                lbl5.backgroundColor = UIColor.clear

            }
              if lbl4.text == ""
            {
                lbl4.backgroundColor = UIColor.clear

            }
              if lbbl3.text == ""
            {
                lbbl3.backgroundColor = UIColor.clear

            }
              if lbl2.text == ""
            {
                lbl2.backgroundColor = UIColor.clear

            }
              if lbl1.text == ""
            {
                lbl1.backgroundColor = UIColor.clear

            }
            buttoncolor()

            
        }
        else
        {
           
            if lbl1.text != ""
            {
                lbl1.backgroundColor = UIColor.buttonprimary
            }
            
            if lbl2.text != ""
            {
                lbl2.backgroundColor = UIColor.buttonprimary

            }
             if lbbl3.text != ""
            {
                lbbl3.backgroundColor = UIColor.buttonprimary

            }
             if lbl4.text != ""
            {
                lbl4.backgroundColor = UIColor.buttonprimary

            }
             if lbl5.text != ""
            {
                lbl5.backgroundColor = UIColor.buttonprimary

            }
             if lbl6.text != ""
            {
                lbl6.backgroundColor = UIColor.buttonprimary

            }
            buttoncolor()

        }
    }
    
    
    
    @IBAction func numberbtnact(_ sender: UIButton) {
        
        UIView .animate(withDuration: 0.1, animations: {
            sender.backgroundColor = UIColor.buttonprimary
        }, completion: { completed in
            if completed {
                sender.backgroundColor = UIColor.buttonprimary.withAlphaComponent(0.2)
            }
        })
        
        if lbl1.text == ""
        {

            lbl1.text = "\(sender.tag)"
            colorupdate(from: "add")
            
        }
        
        else if lbl2.text == ""
        {
            lbl2.text = "\(sender.tag)"
            colorupdate(from: "add")

        }
        else if lbbl3.text == ""
        {
            lbbl3.text = "\(sender.tag)"
            colorupdate(from: "add")

        }
        else if lbl4.text == ""
        {
            lbl4.text = "\(sender.tag)"
            colorupdate(from: "add")

        }
        else if lbl5.text == ""
        {
            lbl5.text = "\(sender.tag)"
            colorupdate(from: "add")

        }
        else if lbl6.text == ""
        {
            lbl6.text = "\(sender.tag)"
            colorupdate(from: "add")

        }
        
        
    }
    
    @IBAction func acivatebtnblink(_ sender: Any) {
        
        passcodeview.shake()

    }
    @IBAction func clearbtnact(_ sender: UIButton) {
     
        UIView .animate(withDuration: 0.1, animations: {
            sender.backgroundColor = UIColor.buttonprimary
        }, completion: { completed in
            if completed {
                sender.backgroundColor = UIColor.clear
                
            }
        })
        
        
        if lbl6.text != ""
        {
            lbl6.text = ""
        }
        
         else if lbl5.text != ""
        {
            lbl5.text = ""

        }
         else if lbl4.text != ""
        {
            lbl4.text = ""

        }
        else if lbbl3.text != ""
        {
            lbbl3.text = ""

        }
         else if lbl2.text != ""
        {
            lbl2.text = ""

        }
         else if lbl1.text != ""
        {
            lbl1.text = ""

        }
        
        colorupdate(from: "delete")
    }
    
    
    func buttoncolor()
    {
        
        if lbl1.text == "" || lbl2.text == "" || lbbl3.text == "" || lbl4.text == "" || lbl5.text == "" || lbl6.text == ""
        {
            activeinactivebtn.isHidden = false
        }
        else
        {
            activeinactivebtn.isHidden = true
        }
        
        
    }
    
    @IBAction func setact(_ sender: Any) {
     
        
        let c = lbl1.text! + lbl2.text! + lbbl3.text!
        let c1 = c + lbl4.text! + lbl5.text! + lbl6.text!

        print("Selected Pin \(c1)")


   if enteredpin == c1
   {
    
    UserDefaults.standard.set(c1, forKey: "pin")
    UserDefaults.standard.set(customer_id, forKey: "user_id")

    
    
     Common.checkRemoveCdAlert()
    let alert = CDAlertView(title: AppName, message: lang.Pin_Successfully_Set, type: .success)
    alert.circleFillColor = UIColor.buttonprimary
    let doneAction = CDAlertViewAction(title: lang.OK, font: nil, textColor: UIColor.white, backgroundColor: UIColor.buttonprimary, handler: nil)
            alert.add(action: doneAction)
            alert.show() { (alert) in
                
                if pinfrom == "menu"
                {
                    self.push(id: "EditProfileAuthenticationViewController", animation: false, fromSB: Profile)

                }
                else
                {
                    self.push(id: "TouchIDSetViewController", animation: true, fromSB: Login)

                }

            }
    
    

    }
        
  else
    
   {
    
    passcodeview.shake()

    
  }
        

        
    }
    
    @IBAction func backact(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        //self.popLeft()
    }
    
    
}
