//
//  PrivacyPolicyViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 27/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit

class PrivacyPolicyViewController: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var termsLbl: UILabel!
    @IBOutlet weak var maniview: UIView!
    lazy var lang = Language.default.object
    override func viewDidLoad() {
        super.viewDidLoad()

        maniview.clipsToBounds = true
        maniview.layer.cornerRadius = 50
        if #available(iOS 11.0, *) {
            maniview.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        getterms()
        titleLbl.text = lang.TermsandConditions
        // Do any additional setup after loading the view.
    }
    
    
    
    func getterms()
    {
        
        let params1 = ["link_name":"privacy_policy"]
        
        print(params1)
        
                                APIManager.shared.termsandconditions(params: params1 as [String : AnyObject]) { (response) in
                                    print("response termsandconditions \(response)")
                                    

                                    let dict = response["cms_detail"].arrayValue
                                    
                                    let dicts = dict[0].dictionaryValue
                                    
                                    self.termsLbl.text = dicts["content"]?.stringValue
                                    

        }
    }
    
    override func viewDidLayoutSubviews() {
        
        if #available(iOS 11.0, *){
            
        }else{
           let rectShape = CAShapeLayer()
           rectShape.bounds = view.frame
           rectShape.position = view.center
           rectShape.path = UIBezierPath(roundedRect: view.bounds,    byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 50 , height: 50)).cgPath
           maniview.layer.mask = rectShape
    }
    
    }
    
    @IBAction func backact(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
