//
//  MembershipSwipeViewController.swift
//  TheClubToken
//
//  Created by Uplogic Tech on 25/06/20.
//  Copyright © 2020 The Club Token. All rights reserved.
//

import UIKit
import SwipeMenuViewController

class MembershipSwipeViewController: SwipeMenuViewController {
    lazy var lang = Language.default.object
 private var datas: [String] = [""]
    var options = SwipeMenuViewOptions()
    var dataCount: Int = 0
    

    override func viewDidLoad() {
        


        viewmemberShip()

        
         super.viewDidLoad()
        
      
        // Do any additional setup after loading the view.
    }
    
    
    func viewmemberShip()
    {
                    let params1 = ["userid":customer_id]
                               
        
        print(params1)
        
                                APIManager.shared.membership(params: params1 as [String : AnyObject]) { (response) in
                                    print("response params1 \(response)")
                                    
                                    let result = response["membership"].arrayValue
                                    
                                    membershipJSON = response["membership"].arrayValue
                                    self.datas.removeAll()
                                    
                                    self.dataCount = result.count
fromIndexValue = 0
                                    for i in 0...result.count - 1
                                    {
                                        let dicts = result[i].dictionaryValue
                                        let dict = dicts["ranking_name"]!.stringValue
                                        self.datas.append(dict)

                                       // fromIndexValue = i
                                        
                                        let vc1 = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "MemberShipViewController") as? MemberShipViewController
                                        vc1!.title = self.datas[i]
                                        
                                        self.addChild(vc1!)

                                        
                                        if i == result.count - 1
                                        {
                                            
                                            self.options.tabView.itemView.textColor = UIColor.white
                                            self.options.tabView.itemView.font = UIFont(name:"Montserrat-Medium",size:15)!

                                            self.options.tabView.height = 50.0
                                            self.options.tabView.needsAdjustItemViewWidth = false
                                            self.options.tabView.needsConvertTextColorRatio = false
                                            let Color = UIColor(red: 69.0/255.0, green: 83.0/255.0, blue: 96.0/255.0, alpha: 1.0)
                                            // let myColor = UIColor(red:0.69, green:0.83, blue:0.96, alpha:1.0)
                                            self.options.tabView.itemView.width = self.view.frame.width / 4
                                            self.options.tabView.backgroundColor = UIColor.buttonsecondary
                                            self.options.tabView.addition = .underline
                                            self.options.tabView.itemView.selectedTextColor = UIColor.white
                                            self.options.tabView.additionView.backgroundColor = UIColor.white
                                            
                                            self.swipeMenuView.reloadData(options: self.options)


                                        }
                                        
                                        
                                    }
                                    
        }
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewWillSetupAt currentIndex: Int) {
        super.swipeMenuView(swipeMenuView, viewWillSetupAt: currentIndex)
        print("will setup SwipeMenuView")
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewDidSetupAt currentIndex: Int) {
       
        super.swipeMenuView(swipeMenuView, viewDidSetupAt: currentIndex)

        
        
        print("did setup SwipeMenuView")
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, willChangeIndexFrom fromIndex: Int, to toIndex: Int) {
        super.swipeMenuView(swipeMenuView, willChangeIndexFrom: fromIndex, to: toIndex)
        fromIndexValue = toIndex
        NotificationCenter.default.post(name: Notification.Name("membership"), object: nil)

        print("will change from section\(fromIndex + 1) to section\(toIndex + 1)")
        
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, didChangeIndexFrom fromIndex: Int, to toIndex: Int) {
        super.swipeMenuView(swipeMenuView, didChangeIndexFrom: fromIndex, to: toIndex)
        print("did change from section\(fromIndex + 1) to section\(toIndex + 1)")
    }
    
  

    // MARK - SwipeMenuViewDataSource
    
    override func numberOfPages(in swipeMenuView: SwipeMenuView) -> Int {
        return dataCount
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, titleForPageAt index: Int) -> String {
        return children[index].title ?? ""
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewControllerForPageAt index: Int) -> UIViewController {
        let vc = children[index]
        vc.didMove(toParent: self)
        return vc
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

